Sect. 3

This is interesting, and since at least I am not familiar with these
details and always
had enormous respect of Bernreuther abstract formulae, important. I
therefore think
a little bit more care should be used.

E.g. what is completely missing is some motivation beyond, ‚I just want
to measure this‘.
What is the importance: can you identify P/CP violation? Are there
detailed BSM models
predicting deviations etc.? E.g. can you derive from all the
measurements statements
about the P and CP state (of the tt system)?
--->  There are different scenarios which can alter the spin correlation or the polarisation, but no specific model is used in the analysis. The observable set by Bernreuther is constructed in a way that the observables are sensitive to new physics with a defined behaviour under P- or CP-transformations. That's the only statement which can be made if there is a significant deviation between the measurement and the SM. Any measurement which is in agreement with the SM expectations of the observables, confirms the p- and CP-conserving nature of ttbar production in the SM. But for example a difference in the polarisation of top and antitop quark would refer to a CP-violating production mechanism. In (arXiv:1508.05271), Bernreuther uses an effective field theory approach in order to define new physics contributions in terms of their behaviour under the mentioned transformations, e.g. chromo-magnetic and chromo-electric dipole moments.


Furthermore some of the quantities are not properly defined (what do the
indices 1, 2
refer to?), Eq.2 has an index on the lhs, none on the rhs. How is \alpha
related to
\alpha_1, 2 (last term of Eq.1 would only drop out, if c_1 und c_2 are
uncorrelated …
lead to the question where is A).
--->  The indices 1,2 refer to the top and antitop quark, respectively, which is only mentioned for Eqn.3 and needs to be added earlier. The index for Eqn.2 was removed. \alpha depends on the final state particle which is chosen to construct the cos\theta variable. As only one final state particle goes into the calculation of cos\theta in Eqn.2, the indices 1,2 were dropped in this case. In Eqn.1 the indices are necessary, because one can take different types of top decay particles for the construction of cos\theta for the top and antitop quarks. As I take charged leptons for both of them, \alpha_1 = \alpha_2 = \alpha ~ 1 in this case. In the case of no correlation, A is 0 and the last term drops completely, this is correct. This leads to a completely flat double differential cross section distribution, if one plots cos\theta_1 vs. cos\theta_2.
How are the formulae and arguments of 3.1 related to the intro into the
observables?
---> Eqn.1 originates from Eqn.3, but just takes into account the two spin quantisation axes involved in the calculation of cos\theta for the top and antitop quarks. The relations in the intro of the observables are helpful to measure the observables, which are introduced in section 3.1.

Eq. 3 is this meant as sum _i,j  C_ij, s….?? Would be good to spell out.
---> As part of the matrix element, it is a sum over i and j, this will be added in a new version of the note.

People may als not be that familiar with the helicity axis, in a 170 pg.
document it does
not harm to add a sentence.
---> will be added

Sect. 4.2

Before discussing the reason for stable-particle level you might first
say (in general terms)
what this is
---> In the introduction of the section I refer to stable particles as particles with a lifetime of \tau > 0.3*10^(-10)s. In the subsection I go into more detail about the stable particles that are used.

Sect. 6.1

Can you say something about a potential bias due to the selection of low
mass tt?
---> The effect of the low mass tt was only checked w.r.t. to systematic uncertainties where a onesided behaviour is seen for some of the observables. In this case, the low mass tt selection didn't change the effect, but the net effect on the observables themselves has not been checked.

Sect. 6.2

In Figs. 12ff there is an obvious trend to have more data than expected
and also the
shape is different., e.g. high eta etc. (also Fig. 75, scattering angle
with fairly
significant excess). Where do you comment on these? Could you say, in
how far these
quantities are correlated with the spin observables?

---> After selection and reconstruction we observe more data than we expect with a difference of ~5%, this is true. The biggest difference is visible for the ee-channel and it has been checked by now, that there is no problem related to the modelling of the ee channel. The shape difference of e.g. the scattering angle has not been commented on yet, but is under investigation. All variables, where a slope between data and expectation can be observed, are related to the top pT. The effect on the observables due to the top pT shape has been evaluated via reweighting the distributions and is found to be small. The same technique will also be tested with one of the kinematic variables which shows a similar trend like the top pT.

Sect. 7.3.1

Do not understand the plot (y - axis in %?) Does this mean you have some
80 - 90%
outside the fiducial volume? The x - axis is also rather obscure. You may
easily assist the reader who meets this analysis the first time.

---> Only around 8% of the events are outside the fiducial volume after the reconstruction. The plot was a bit misleading and has been changed to the fraction which is applied later in the analysis, i.e. (nonfiducial)/(nonfiducial+fiducial).

Sect. 9.1

l.838 two sigma difference is certainly not significant.
---> Bad phrasing, this part has been removed.

On the other hand: how strongly should P+^hel and P-^hel be the same in
the SM.
Since both are on the neagtive side does the difference to the SM become
more telling?
---> In the SM, P+^hel and P-^hel are the same. In terms of new physics this is a different story as a P-violating production mechanism would lead to top- and anti-top quarks of oppositve polarization. Due to some recent changes in the analysis, the helicity polarisations are closer to the SM now.

BTW: I do not see in Fig.21 the same notation as in Tab.2 - are these
different quantities,
I may have missed the introduction of the Fig.21ff varibales.
---> In Tab.2 all the variables which are used for the construction of the observable distributions are listed. In order to obtain the polarisation and spin correlation, these distributions are used in combination with the relations defined in section 3. The slope of the cos\theta distribution is the polarisation for the specific spin quantisation axis and the mean of all other distributions multiplied with a factor of '-9' gives the spin correlations. Those numbers are quoted in the summary figures. As the polarisation and spin correlation are not properly defined at stable-particle level, the x-axis notation for their summary figures changed to the mean of the distributions along with the corresponding pre-factors.

Another BTW: there are MANY distributions in App. C, you would help the
reader if
you give which Figs. one should compare (l.843).
---> Will be added in a new version of the note.

The green spots in the Figs 21ff are hardly visible.
---> They have been increased in the current version.
Sect. 9.2

l.849 You never mentioned in the previous subsection the agreement you
observe at
the parton level. It would be good to get some more precise numbers  on
before/after
unfolding and how many events you expect to fall out/come into the
acceptance.
In some sense the agreement at the stable particle level is worrisome
and indeed should
be understood. It may point to a problem in the procedure. Did you make
a closure
test (maybe I have missed it).
---> The numbers at selection/reconstruction/unfolded level will be added in the new version of the note. At the unfolded level, the ratio of unfolded data/expectation is around 1.06.
     Closure tests are performed by running ensemble tests on the default MC sample and comparing it to the expectation.


It is good that you state the issue clearly, because as far as I
understand in principle only
normalised distributions, i.e. the shape are relevant. True?
---> This is true. The actual normalization is not of any interest, but only the shape. In the end, only normalized distributions will be presented in the publication.

Sect. 10 Conclusions

As mentioned before, some more clear statement on what we learn from
this, in how
far we can constrain quantum numbers or BSM model would put some more
juice into the analysis
---> The goal in the beginning was to set limits on the parameters of the chromo-magnetic couplings (like CMS did for their spin correlation ), but this has been dropped. However, most of the observables have not been measured before and are thus a good of the SM. Depending on the new results, it might be possible to observe transverse correlation for pair-produced top quarks. I agree it would have been nice to have a specifiv model to compare to, but one of the advantages of the unfolded stable-particle distributions is the possibility for theorists to compare their models to the data, once the data has been added to Rivet.
