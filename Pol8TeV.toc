\select@language {UKenglish}
\contentsline {section}{\numberline {1}Introduction}{6}{section.1}
\contentsline {section}{\numberline {2}Observables}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}Spin density matrix and the set of observables}{7}{subsection.2.1}
\contentsline {section}{\numberline {3}Monte Carlo}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Parton level}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Stable-particle level}{11}{subsection.3.2}
\contentsline {section}{\numberline {4}Selection}{19}{section.4}
\contentsline {subsection}{\numberline {4.1}Object Selection}{19}{subsection.4.1}
\contentsline {paragraph}{Jets}{19}{section*.13}
\contentsline {paragraph}{Electrons}{19}{section*.14}
\contentsline {paragraph}{Muons}{19}{section*.15}
\contentsline {paragraph}{Missing Transverse Momentum}{19}{section*.16}
\contentsline {paragraph}{Trigger Requirements}{20}{section*.17}
\contentsline {paragraph}{Event Cleaning}{20}{section*.18}
\contentsline {subsection}{\numberline {4.2}Dilepton selection}{20}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}$Z+$jets\xspace \ Background Estimation}{21}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Fake Background Estimation}{23}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Comparison of Data and Simulation}{28}{subsubsection.4.2.3}
\contentsline {section}{\numberline {5}Reconstruction}{32}{section.5}
\contentsline {subsection}{\numberline {5.1}The neutrino weighting method}{33}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Comparison of Data and Simulation}{33}{subsection.5.2}
\contentsline {section}{\numberline {6}Unfolding}{36}{section.6}
\contentsline {subsection}{\numberline {6.1}FBU}{36}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}FBU Ingredients}{36}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Prior Choice}{36}{subsubsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.3}Marginalization}{37}{subsubsection.6.1.3}
\contentsline {subsection}{\numberline {6.2}Parton level unfolding}{37}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Binning choice}{39}{subsubsection.6.2.1}
\contentsline {subsection}{\numberline {6.3}Stable-particle level unfolding}{40}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}Non-fiducial background}{42}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.2}Binning choice}{45}{subsubsection.6.3.2}
\contentsline {section}{\numberline {7}Systematics}{46}{section.7}
\contentsline {subsection}{\numberline {7.1}Procedure used to assess systematic uncertainties}{46}{subsection.7.1}
\contentsline {subsubsection}{\numberline {7.1.1}Signal Modelling Uncertainties}{46}{subsubsection.7.1.1}
\contentsline {subsubsection}{\numberline {7.1.2}Detector Modelling Uncertainties}{46}{subsubsection.7.1.2}
\contentsline {subsubsection}{\numberline {7.1.3}Other systematics}{48}{subsubsection.7.1.3}
\contentsline {subsection}{\numberline {7.2}List of systematics considered}{49}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}\ensuremath {t\bar {t}}\xspace \ modelling systematics}{49}{subsubsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.2}Background uncertainties}{50}{subsubsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.3}Detector modelling systematics}{50}{subsubsection.7.2.3}
\contentsline {section}{\numberline {8}Results}{54}{section.8}
\contentsline {subsection}{\numberline {8.1}Parton measurement}{54}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Stable-particle measurement}{56}{subsection.8.2}
\contentsline {section}{\numberline {9}Conclusion}{62}{section.9}
\contentsline {section}{\numberline {A}Monte Carlo samples}{63}{appendix.A}
\contentsline {section}{\numberline {B}Further plots and tables}{66}{appendix.B}
\contentsline {subsection}{\numberline {B.1}Resolutions}{66}{subsection.B.1}
\contentsline {subsubsection}{\numberline {B.1.1}Partonic}{66}{subsubsection.B.1.1}
\contentsline {subsubsection}{\numberline {B.1.2}Stable particle}{66}{subsubsection.B.1.2}
\contentsline {subsection}{\numberline {B.2}Binnings}{66}{subsection.B.2}
\contentsline {subsection}{\numberline {B.3}Response matrices}{90}{subsection.B.3}
\contentsline {subsubsection}{\numberline {B.3.1}Partonic}{90}{subsubsection.B.3.1}
\contentsline {subsubsection}{\numberline {B.3.2}Stable-Particle}{97}{subsubsection.B.3.2}
\contentsline {subsection}{\numberline {B.4}Acceptance efficiencies}{104}{subsection.B.4}
\contentsline {subsubsection}{\numberline {B.4.1}Partonic}{104}{subsubsection.B.4.1}
\contentsline {subsubsection}{\numberline {B.4.2}Stable-Particle}{104}{subsubsection.B.4.2}
\contentsline {subsection}{\numberline {B.5}Reconstructed distributions}{117}{subsection.B.5}
\contentsline {subsection}{\numberline {B.6}Nuisance parameters}{123}{subsection.B.6}
\contentsline {subsubsection}{\numberline {B.6.1}Partonic}{123}{subsubsection.B.6.1}
\contentsline {subsubsection}{\numberline {B.6.2}Stable-particle}{123}{subsubsection.B.6.2}
\contentsline {subsection}{\numberline {B.7}Unfolded distributions}{154}{subsection.B.7}
\contentsline {subsubsection}{\numberline {B.7.1}Partonic}{154}{subsubsection.B.7.1}
\contentsline {section}{\numberline {C}Fakes estimation}{169}{appendix.C}
\contentsline {section}{\numberline {D}Kinematic \ensuremath {t\bar {t}}\xspace reconstruction and its uncertainty}{180}{appendix.D}
\contentsline {subsection}{\numberline {D.1}The kinematic reconstruction method}{180}{subsection.D.1}
\contentsline {subsection}{\numberline {D.2}Comparison of Data and Simulation}{180}{subsection.D.2}
\contentsline {subsection}{\numberline {D.3}Reconstruction uncertainty}{181}{subsection.D.3}
\contentsline {section}{\numberline {E}Systematic Tables}{184}{appendix.E}
\contentsline {section}{\numberline {F}Calibration curves for the stable-particle measurement}{196}{appendix.F}
\contentsline {section}{\numberline {G}Mean calibration}{198}{appendix.G}
\contentsline {section}{\numberline {H}EE channel}{199}{appendix.H}
\contentsline {section}{\numberline {I}Ensemble Tests}{206}{appendix.I}
\contentsline {subsection}{\numberline {I.1}Partonic}{206}{subsection.I.1}
\contentsline {subsection}{\numberline {I.2}Stable-particle}{206}{subsection.I.2}
\contentsline {section}{\numberline {J}Observable correlations}{211}{appendix.J}
\contentsline {subsection}{\numberline {J.1}Reco level: KIN method}{211}{subsection.J.1}
\contentsline {subsection}{\numberline {J.2}Unfolded level: NW method}{211}{subsection.J.2}
\contentsline {section}{\numberline {K}Top quark mass uncertainty}{214}{appendix.K}
\contentsline {section}{\numberline {L}Background shapes}{222}{appendix.L}
\contentsline {subsection}{\numberline {L.1}Single-top}{222}{subsection.L.1}
\contentsline {subsection}{\numberline {L.2}Drell-Yan}{222}{subsection.L.2}
\contentsline {section}{\numberline {M}Top $p_{\text {T}}$}{225}{appendix.M}
