2014-11-20 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 01-04-01.
	* Small fix so atlascover recognises better when atlasdoc is used.
	* Started documenting ATLAS preprint style in atlas_latex.
2014-11-19 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 01-04-00.
	* First implementation of the ATLAS preprint style.
	* Fonts still have to be checked on other platforms.
	* Major rewrite of both atlasdoc.cls and atlascover.sty, which removes spurious offsets.
	* Removed need for title skip parameter and horizontal and vertical offsets on cover pages.
	* Need coverpage=true in atlascover.sty if you want a draft cover and are using
	  atlascover.sty standalone.
	* No longer need atlasdoc as option to atlascover,
	  as package checks if atlasdoc is being used itself.
	* make cleanpdf command added (or being added) to standard Makefiles.
	* Adding of extra blank pages after cover page mostly removed.
	  Have to see if it is necessary somewhere.
	* Options added to atlasdoc for different fonts.
	* showframe option added for debugging.
2014-10-20 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 01-03-06.
	* Require hyperref in atlascover to avoid problems with supporting documentation.
2014-10-16 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 01-03-05.
	* Rename documentation directories and files to use _ instead of -.
2014-10-09 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 01-03-04.
	* Small fix to auxiliary material.
	* Replace some \\ with \par in atlascover for consistency.
2014-10-09 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 01-03-03.
	* Fix to CONF and PUB note draft cover pages.
2014-09-22 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 01-03-02.
	* Instructions for Fedora added.
	* Do not use lineno with revtex - use class option linenumbers instead.
	* Add option to atlaspackage to turn off lineno.
	* Use \linewidth instead of \textwidth in PrintAtlasContribute so it works in 
	  two column format.
	* Print out web page on auxiliary material cover.
	* Fix typo that put line numbers on cover page.
	* Move date and page number from header to footer in draft mode.
	* List of contributors was too wide.
	* Small fix to BibTeX style files and bring them back in sync.
2014-09-15 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 01-02-00
    * Bug fixed for CentOS/Fedora, as scrdate/scrtime did not work properly.
2014-09-09 Ian Brock <Ian.Brock@cern.ch>
	* Add texmf swtich to atlasdoc (needed for cover).
	* full option did not work in atlaspackage.
	* Use kvoptions in atlascontribute.
	* Change option from noauthblk to authblk=false.
	* Internally quite a lot of variable name changes.
	* Prepare atlasdoc for moving to kvoptions.
	* Switched from tabs to spaces in many places.
2014-09-08 Ian Brock <Ian.Brock@cern.ch>
	* Small fix to atlaspackage so texlive switch works.
2014-09-06 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 01-01-00
	* Add template for auxiliary material (atlas-auxmat).
	* Add switch so that TeX Live 2009 also works.
	* Do not include atlascontribute in metadata file.
	* Add preprintcover to Makefile.
	* Use fancyhdr to make header in draft mode (and also for auxiliary material).
	* Use scrdate and scrtime for current date and time.
	* Replace color with xcolor.
	* Switch to kvoptions in atlaspackage.
	* Add option to atlaspackage to turn off including siunitx.
	* Added atlasjournal and atlasxref to atlasphysics.
	* Further improvements to documentation, especially to atlas-latex.
2014-09-04 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 01-00-00
	* Add some examples of paper drafts with journal formatting.
	* More style files for journal names and cross references.
	* Use kvoptions for setting options, as then offsets can be given.
	* Move logos from figures to logos directory.
	* Include JHEP BibTeX style file in bibtex/bst.
	* Updates to documentation, especially BibTeX and LaTeX guide.
2014-08-29 Ian Brock <Ian.Brock@cern.ch>
	* Added a new style file atlascontribute.sty. It can be used to create both the
	  list of authors and the contributions they made to the analysis.
	* atlas-document-contribute is no longer needed. This information should be
	  included in atlas-document-metadata.tex instead.
2014-08-28 Ian Brock <Ian.Brock@cern.ch>
	* Add lineno as required package, so that atlasdoc does not need atlaspackage.
	* Rounding note added to documentation.
2014-08-27 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 00-97-00.
	* atlas-physics document list of definitions created automatically.
	* CONF and PUB are now options for CONF and PUB notes.
	* Cover pages change a bit depending on these options.
	* EdBoard replaced by Readers for a PUB note.
	* More documentation on options in atlasdoc and atlascover added to atlas-latex
2014-08-27 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 00-96-00.
	* Merge biblatex setup into atlaspackage with an option.
	* Add option texmf to atlasphysics if package in installed centrally
	* Add note number back into title page
	* Switch back from \AtlasTitle to \title
	* Further improve documentation
2014-08-22 Ian Brock <Ian.Brock@cern.ch>
	* Tag as version 00-95-00.
	* First alpha release of atlaslatex.
	* atlasphysics.sty split into smaller files.
	* Options to steer atlasphysics.sty.
	* Documentation reorganised. Split into paper style,
	  for which PubCom is responsible and atlaslatex guide.
	* Templates all in templates directory.
	* Makefile can make a new document.
	* Better use of geometry to set page size everywhere.
	* Add inputenc and fontenc to atlasphysics.sty.
	* In fact, these (and txfonts) should probably move to atlasdoc.cls.
	* Use tocloft for table of contents.
	* Titles should invoke boldmath automatically.
2014-08-25 Ian Brock <Ian.Brock@cern.ch>
	* Merge biblatex into atlaspackage and add biblatex option.
	* Add koma option to atlasdoc.cls.
