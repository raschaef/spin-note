\section{Background shapes}
\label{app:bkg_shapes}

All different background sources have to be well-modelled as they need
to be taken into account when running the unfolding procedure on the
observed data. Any mis-modelled background will result in an opposite
effect on the unfolded \ttbar signal. Therefore, all backgrounds have
an associated nuisance parameter, where the width of the Gaussian
distribution corresponds to the normalisation uncertainty of the
corresponding background process. E.g. the fakes background was
estimated to have a very large background uncertainty of the order of
100\% depending on its channel. However, the shape of the estimated
background can also be mis-modelled. In order to check the modelled
backgrounds and possible effect on the results, they are compared to
different generators. This is evaluated for the two largest
backgrounds, which are single-top and Drell-Yan production.

\subsection{Single-top}
Two different schemes exist for the generation of associated $Wt$
production in order to take into account its interference at NLO with
LO \ttbar production, these are the diagram-removal (DR) and the
diagram-subtraction (DS) schemes~\cite{}. While the former one is the
nominal sample used for analyses, the latter one is used to estimate
the systematic uncertainty on the $Wt$ background.

The nominal DR samples is replaced with the DS sample and the
unfolding procedure repeated for each observable. The resulting
unceratinty is evaluated by taking the difference to the unfolding
with the nominal sample. The uncertainty for each observable is listed
in Tab.~\ref{tab:single_top_unc} and is about on order of magnitude smaller than single detector-related
uncertainties. Therefore, the uncertainty coming from the shape of the
single-top background is considered negligible.

\begin{table}
  \begin{center}
    \begin{tabular}{c|c|c}
      Observable & $\Delta$ single top \\
      \hline
      \helpol & 0.0001\\
      \transpol & 0.0004  \\
      \rpol  &  0.0004 \\
       \helcorr & 0.0000 \\
       \transcorr & 0.0002\\
       \rcorr & 0.0016\\
       \transhelsum & 0.0007 \\
       \transheldiff & ----\\
       \transrsum & 0.0008\\
       \transrdiff & 0.0021\\
       \rhelsum & 0.0024\\
       \rheldiff & 0.0001\\
    \end{tabular}
    \caption{Uncertainties coming from the single top background by
      comparing DR and DS generation schemes.}
    \label{tab:single_top_unc}
  \end{center}
\end{table}


\subsection{Drell-Yan}

The Drell-Yan background for the measurement is generated with
Alpgen+Pythia. The simulation of the process is cross-checked with
Sherpa as MC generator. Especially the extrapolation of events outside
the $Z$-mass window is of interest. The scale factors computed for the
Sherpa samples are $\text{SF}_{ee} = 0.97 \pm 0.03$, $\text{SF}_{\mu
  \mu} = 0.94 \pm 0.03$ and $\text{SF}_{\text{HF}} = 1.08 \pm 0.11$.

Figures~\ref{fig:app_zjets_comp_ee} and~\ref{fig:app_zjets_comp_mm} show invariant mass distributions of the
leptons for the signal region including the $Z$-mass window for the
\eeme and \mumume channels for both Alpgen+Pythia and Sherpa
samples. As can be seen, their shapes are very similar and do not have
an effect on the total expectation/data agreement. The disagreement
below $m(\ell \ell) = 40$ GeV for the Sherpa samples comes from the
lack of dedicated low-mass samples used in this study. The
extrapolation below and and above the $Z$-mass window can be seen in
tab.~\ref{tab:zjets_gen_fractions}. It gives the fraction of events below, within and
above the $Z$-mass window, where the number of events within the
window is approximately the same for both Alpgen+Pythia and Sherpa due
to the calculation of the scale factors in this control region. The
Sherpa samples tend to have a slightly lower normalisation compared to
Alpgen+Pythia below the $Z$-mass window, while it predicts more events
above the window. Fig.~\ref{fig:app_zjetsbkg_comp} show the shapes of both generators for
both \eeme and \mumume channels.

\begin{table}
  \begin{center}
    \begin{tabular}{c|c|c|c|c}
      Sample (channel) &45 GeV $<m(\ell \ell)<$ 81 GeV & 81 GeV $<=m(\ell
      \ell)<$ 101 GeV &$m(\ell \ell)<=$ 101 GeV  \\
      \hline
      Alpgen+Pythia (\eeme) & 0.066 & 0.892 & 0.042\\
      Sherpa (\eeme) & 0.061 & 0.889 & 0.050\\
      Alpgen+Pythia (\mumume) & 0.062 & 0.890 & 0.048  \\
      Sherpa (\mumume)  & 0.061 & 0.885 & 0.054 \\
    \end{tabular}
    \caption{Fraction of event below, within and above the $Z$-mass
      window for Alpgen+Pythia and Sherpa for both \eeme and \mumume
      channels. (Uncertainties will follow)}
    \label{tab:zjets_gen_fractions}
  \end{center}
\end{table}


\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.48\textwidth]{figures/app/zjets/ee_invmass_v_log_btag_noz_central.pdf} 
   \includegraphics[width=0.48\textwidth]{figures/app/zjets/sherpa/ee_invmass_v_log_btag_noz_central.pdf} 
   \caption{Data/expectation comparison in the signal region including
   the $Z$-mass window in the \eeme-channel for both Alpgen+Pythia
   (left) and Sherpa (right) samples for the Drell-Yan background.}
   \label{fig:app_zjets_comp_ee}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.48\textwidth]{figures/app/zjets/mumu_invmass_v_log_btag_noz_central.pdf} 
   \includegraphics[width=0.48\textwidth]{figures/app/zjets/sherpa/mumu_invmass_v_log_btag_noz_central.pdf} 
   \caption{Data/expectation comparison in the signal region including
   the $Z$-mass window in the \mumume-channel for both Alpgen+Pythia
   (left) and Sherpa (right) samples for the Drell-Yan background.}
   \label{fig:app_zjets_comp_mm}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.48\textwidth]{figures/app/zjets/mll_ee_btag_noz.pdf} 
   \includegraphics[width=0.48\textwidth]{figures/app/zjets/mll_mumu_btag_noz.pdf} 
   \caption{Shape comparison of the invariant dilepton mass for the Drell-Yan background generated
     with the Alpgen+Pythia (nominal) and Sherpa for \eeme (left) and
     \mumume (right) channels.}
   \label{fig:app_zjetsbkg_comp}
 \end{center}
\end{figure}