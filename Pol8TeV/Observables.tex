\section{Observables}
\label{sec:obs}

The top quark spin polarization and correlation are observable in
angular distributions as can be directly seen from the
double-differential cross-section for \ttbar production;
\begin{equation}
\frac{1}{\sigma} \frac{d^{2} \sigma}{d \cos \theta_{1} d \cos
  \theta_{2}} = \frac{1}{4} (1 +\alpha_{1} P_{1} \cos \theta_{1} +
\alpha_{2} P_{2} \cos \theta_{2} -  \alpha_{1} \alpha_{2} A \cos
\theta_{1} \cos \theta_{2}),
\label{eqn:double-diff}
\end{equation}
where $P$ and $A$ are the polarization and spin correlation for a
certain spin quantization axis and $\theta$ the angle between the momentum
direction of a lepton
in its parent top quark rest frame and the quantization axis. The factor
$\alpha$ is
the spin analysing power of the lepton and describes how much of the
spin information is passed to the lepton. This factor is calculated to
be 0.998 for leptons at NLO~\cite{spin_anapow_lep}. By integrating out one of the
angles in equation~\ref{eqn:double-diff}, the differential
cross-section, which only depends linearly on the polarization of the top
quark, is obtained:
\begin{equation}
\frac{1}{\sigma} \frac{d \sigma}{d \cos \theta } = \frac{1}{2} (1 +\alpha P \cos \theta ).
\label{eqn:single_diff}
\end{equation}
For that reason, \costheta is often used as observable in analyses
involving polarization or spin correlation.
%The polarization $P$ can be extracted from the \costheta distribution,
%while the spin correlation strength $C$ can be obtained from the
%expectation value of the \coscos distribution:
%\begin{equation}
% C = \alpha_{1} \alpha_{2} A = - 9 <\cos\theta_{1}\cos \theta_{2}>.
%\label{eqn:spincorr}
%\end{equation}
%While there exist other angular distributions which are sensitive to the
%spin correlation, like the azimuthal angle between the final state
%leptons in the lab frame, a direct measurement of the spin correlation
%is only possible via equation~\ref{eqn:spincorr}. A simultaneous
%measurement of the top quark polarization and spin correlation from
%the two-dimensional \costheta-distribution is also possible, but not
%preferred from the technical point of view, as this requires a larger
%amount of statistics than currently available. Also, a fit to the two-dimensional
%distribution would be required to extract the polarization and spin correlation.

\subsection{Spin density matrix and the set of observables}
The main mechanisms for \ttbar production at the LHC are gluon-gluon
fusion (\gluglu) and
quark-antiquark annihilation (\qqbar), with contributions of approximately
80\% and 20\%, at leading order for $\sqrt{s} = 8 \TeV$, respectively. Their squared matrix elements
can be decomposed in the following way:
\begin{equation}
|M|^{2} \propto \tilde{A} + \mathbf{B}^{+} \cdot \mathbf{s}_{1} +
\mathbf{B}^{-} \cdot \mathbf{s}_{2} + C_{ij}s_{1i}s_{2j}.
\label{eqn:obs_me}
\end{equation}
The coefficient $\tilde{A}$ is completely spin independent and fixes the cross-section of \ttbar production. $\mathbf{B}^{\pm}$ define
the polarization vectors for the top and anti-top quark and $C_{ij}$ is
the spin correlation matrix. The vectors $\mathbf{s}_{1/2}$ denote the
spin vectors of top/anti-top quark. By choosing a specific basis $\{\hat{k},\hat{n},\hat{r}\}$, the
coefficients from equation~(\ref{eqn:obs_me}) can be further decomposed
into;
\begin{eqnarray}
\tilde{B}^{\pm}_{i} &=& b_{1}^{\pm}\hat{k}_{i} + b_{2}^{\pm}\hat{n}_{i}
+ b_{3}^{\pm}\hat{r}_{i}, \\
\tilde{C}_{ij} &=& c_{nn}\hat{n}_{i}\hat{n}_{j} +
c_{rr}\hat{r}_{i}\hat{r}_{j} + c_{kk}\hat{k}_{i}\hat{k}_{j}  \nonumber
\\ &+&  c_{rk}(
\hat{r}_{i}\hat{k}_{j} + \hat{k}_{i}\hat{r}_{j}) +  c_{kn}(
\hat{k}_{i}\hat{n}_{j} + \hat{n}_{i}\hat{k}_{j}) +  c_{rn}(
\hat{r}_{i}\hat{n}_{j} + \hat{n}_{i}\hat{r}_{j}) \nonumber \\ &+& \epsilon_{ijl}(
c_{r}^{\text{CP}}\hat{r}_l 
+  c_{k}^{\text{CP}}\hat{k}_l +
c_{n}^{\text{CP}}\hat{n}_l), 
\end{eqnarray}
where $b_{l} (l = 1,2,3)$ are the polarization coefficients with respect to
its corresponding axis and $c_{lm}$ are the spin correlation
coefficients. The superscripts $\pm$ indicate the top and anti-top
quark.

The goal of this analysis is to measure as many
coefficients of the spin density matrix as possible. In order to do
this, a suitable set of spin-quantization axes must first be defined. The helicity axis ($\hat{\mathbf{k}}$) is well-defined and has already
been used in several analyses~\cite{atlas_pol,atlas_spincorr,cms_spinpol}.
A quantization axis transverse to the
production plane ($\hat{\mathbf{n}}$), span by the top quarks'
direction in the lab frame, has been
motivated in recent publications~\cite{bernreuther_8tev,baumgart_transpol}. To have an orthonormal basis, the last
quantization axis ($\hat{\mathbf{r}}$) is chosen to be orthogonal to the other
two. They are defined as:
\begin{eqnarray}
\hat{\mathbf{n}} = \frac{1}{r} \left( \hat{\mathbf{p}} \times \hat{\mathbf{k}} \right), \quad
\hat{\mathbf{r}} = \frac{1}{r} \left( \hat{\mathbf{p}} - y\hat{\mathbf{k}} \right), \\
y = \hat{\mathbf{k}} \cdot \hat{\mathbf{p}}, \quad r = \sqrt{1 - y^{2}}.
\label{eqn:axis_defs}
\end{eqnarray}
Axis $\hat{\mathbf{p}}$ defines the direction of one of the proton beams, with
$\hat{\mathbf{p}} = (0,0,1)$ being the convention taken for this measurement.
To measure the different coefficients of the spin density matrix, it is
preferable to have one observable which is directly sensitive to just one
coefficient. These observables are usually obtained from angular distributions and are defined by the
double-differential cross-section for \ttbar production (see equation~\ref{eqn:double-diff}).
The cosine $\theta$ defined in this equation has
several advantages for the measurement of the spin density
parameters. As can be seen from Eqn.~\ref{eqn:single_diff}, the
polarization is linearly dependent on the \costheta distribution. In
that way, the polarization can be easily extracted from the slope of
the distribution. The product of two different cosines for
the positive and negative lepton results in a distribution, for which its
mean is directly connected to the spin correlation $C$, i.e. $C = \alpha_{1} \alpha_{2} A = - 9 <\cos\theta_{1}\cos \theta_{2}>$. The observables to be
measured therefore consist out of single and double \costheta
distributions and are summarized in table~\ref{tab:obs}. It describes
the names used for the different observables, their definition, a
shorter notation as well as their sensitivity to possible new
physics. Within the short notation, $C(x,y)$ is defined as the mean of
the corresponding \coscos distribution, i.e. $C(x,y) = - 9
<\cos\theta_{x}\cos \theta_{y}>$. The  %kann ich vielleicht auch auf
% ein paar zeilen weiter oben verweisen.
observables consisting out of sums and differences of two \coscos terms
will also be referred to as cross correlations. 

% The \costheta terms are calculated by taking the dot product for
% the leptons and the different axes, i.e. $\hat{k} \cdot \ell^{*}_{+}$
% for the \costheta value in the helicity basis with respect to the
% positive lepton in the top quark rest frame, indicated by the asterisk. But all axes defined in eqns.~\ref{eqn:axis_defs} need in fact to be
% multiplied with a factor in order to write down
% Equation~\ref{eqn:double-diff} with the same sign for both
% polarizations. Table~\ref{tab:new_axes_defs} gives the actual axes,
% which are taken for the \costheta calculation for positive and
% negative leptons indicate by the + and - sign.
All \costheta variables are calculated by taking the dot product of
the positive/negative lepton momentum direction, indicated by a subscript " $+/-$", and one of the
spin quantization axes, indicated by a superscript for the angle
$\theta$. For example, $\hat{\mathbf{k}} \cdot \mathbf{\ell}_{+} = cos
\theta^{k}_{+}$ describes the \costheta variable of the positive
lepton and the helicity axis. Table~\ref{tab:new_axes_defs} describes
the proper corrections to the axes, that are used for the \costheta
calculation. 
\begin{table}
  \begin{center}
    \begin{tabular}{|c|c|c|}
      \hline
      axis name & pos. lepton & neg. lepton \\
      \hline 
      Helicity & $\hat{\mathbf{k}}$& $-\hat{\mathbf{k}}$ \\
      Transverse &  sign$(y)\,\hat{\mathbf{n}}$ & $-$sign$(y)\,\hat{\mathbf{n}}$\\
      R &  sign$(y)\,\hat{\mathbf{r}}$ & $-$sign$(y)\,\hat{\mathbf{r}}$\\
      \hline
      \end{tabular}
    \end{center}
    \caption{Axes taken for the \costheta calculation by taking the
      dot product with the momentum direction of the positive (+) or negative (-) lepton.
     The fact of having two symmetric beams is considered by
     multiplying with the sign of the 
      scattering angle $y$.}
    \label{tab:new_axes_defs}
\end{table}
For the calculation of \costheta with negative leptons, each of the
axes has to be multiplied by a minus sign. The need for this additional
factor is intuitive in the helicity axis, as $-\hat{\mathbf{k}}$ is the direction of the anti-top
quark in the \ttbar rest frame. The multiplication with the sign of
the scattering angle $y$ for the transverse and r axes is necessary as
the initial state is symmetric due to the two proton beams, but in the
definition of the axes a fixed beam direction has been used. Without
applying this factor, the corresponding \costheta distributions would
always be a flat line.

In the previous measurements of \atlas~\cite{atlas_pol,atlas_spincorr} and \cms~\cite{cms_spinpol}, only a subset of
these coefficients have been measured. The polarization was measured
with respect to the helicity axis (momentum direction of the top quark in the
\ttbar rest frame), which provides one coefficient $b_{i}$. The spin correlation measurements were sensitive to various linear combinations of
different CP-even correlation coefficients. The set of observables to
be measured in this analysis avoids having any linear combinations. All
previous results were in agreement with the SM. Out of all the
observables considered for the 8 TeV measurement, only polarizations
and the spin correlation in the helicity axis have been measured at 7
TeV, i.e. \helpolplus, \helpolminus and \helcorr.

\begin{table}
  \begin{center}
    \begin{tabular}{|r|c|c|c|}
      \hline
      Name & short notation & Observable & sensitive to \\
      \hline
      Helicity polarization & \helpol & $\cos{\theta^{k}_{\pm}}$ & P-odd, CP-even/odd\\
      Transverse polarization & \transpol & $\cos \theta^{n}_{\pm}$ & P-even,
      CP-even/odd \\
      R polarization & \rpol & $\cos \theta^{r}_{\pm}$ & P-odd, CP-even/odd \\
      Helicity correlation & \helcorr & $\cos \theta^{k}_{+} \cos \theta^{k}_{-}$
      & P-, CP-even \\
      Transverse correlation & \transcorr & $\cos \theta^{n}_{+} \cos
      \theta^{n}_{-}$ & P-, CP-even \\
      R correlation & \rcorr & $\cos \theta^{r}_{+} \cos \theta^{r}_{-}$ & P-,
      CP-even \\
     R-Hel sum & \rhelsum & $\cos \theta^{r}_{+} \cos \theta^{k}_{-} + \cos
     \theta^{k}_{+} \cos \theta^{r}_{-}$ & P-, CP-even \\
     R-Hel diff & \rheldiff & $\cos \theta^{r}_{+} \cos \theta^{k}_{-} - \cos
     \theta^{k}_{+} \cos \theta^{r}_{-}$ & P-even, CP-odd
     \\
     Trans-Hel sum & \transhelsum & $\cos \theta^{n}_{+} \cos \theta^{k}_{-} + \cos 
     \theta^{k}_{+} \cos \theta^{n}_{-}$ & P-odd, CP-even \\
     Trans-Hel diff & \transheldiff & $\cos \theta^{n}_{+} \cos \theta^{k}_{-} - \cos 
     \theta^{k}_{+} \cos \theta^{n}_{-}$ & P-odd, CP-odd \\
     Trans-R sum & \transrsum & $\cos \theta^{n}_{+} \cos \theta^{r}_{-} + \cos 
     \theta^{r}_{+} \cos \theta^{n}_{-}$ & P-odd, CP-even \\
     Trans-R diff & \transrdiff & $\cos \theta^{n}_{+} \cos \theta^{r}_{-} - \cos 
     \theta^{k}_{+} \cos \theta^{r}_{-}$ & P-odd, CP-odd \\
      \hline
      \end{tabular}
    \end{center}
    \caption{The top spin observables, their definitions and their
  sensitivity to new physics are listed in this table.}
    \label{tab:obs}
\end{table}
