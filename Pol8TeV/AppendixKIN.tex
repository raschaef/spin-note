\section{Kinematic \ttbar reconstruction and its uncertainty}
\label{sec:app_kin}

\subsection{The kinematic reconstruction method}

The KIN  method was used  in the past for top mass reconstruction and
other studies at CDF~\cite{Abulencia:2006js,Aaltonen:2012tk} . A brief
description of this method is given in the following.  

Assuming a value for the top quark mass ($m_t = 172.5 $ GeV), the system 
is constrained and can be solved. In order to find the solution of the
kinematic equations the numerical Newton-Raphson
method~\cite{NumerRecip} is used. 
 
In case there are more than two reconstructed jets in a given event,
the two jets  with the highest value of b-tagging weight (as determined by the
MV1 $b$-tagging algorithm~\cite{ATLAS:2011qia}) are used. This improves
the efficiency of choosing the correct jets, in comparison to just
choosing the two jets with the highest transverse energy ($E_{T})$,
from about 54\% to about 69\%. 

The experimental uncertainties on the measured objects are taken into
account by sampling the phase-space of the measured jets and $\MET$
($N_{smear}$ trials) according to their resolution. For each point in
this space the procedure described below is applied to find a solution
 for $t$ and $\bar{t}$ four-vectors.  
The resolution functions are determined in a similar way as the
transfer functions used in KL fitter~\cite{Erdmann:2013rxa}, i.e. the 
resolution functions as a function of the jet $p_T$ (for jets) and the
total transverse energy in the event (for $\MET$) are produced. 

From the four kinematically possible solutions, the KIN method takes the
solution which leads to the lowest effective mass of the $t\bar{t}$
system.   
The reason for this is that the partonic $t\bar{t}$ cross-section  is a
decreasing function of the partonic energy $\hat{s}\simeq
m(t\bar{t})$ (except very close to the $m(t\bar{t})$ boundary),  therefore,
it is more likely to have events with smaller $m_{t\bar{t}}$.    
The numerical algorithm to find the solution actually stops after
finding two solutions. This is mostly due to efficiency reasons, since
it is known that most of the time there are two solutions and the probability
that there exist four real solutions is very small (a few percent). In
consequence, we would gain very little and the effectiveness of the
method would be decreased (longer processing time). 

The twofold ambiguity in the lepton and $b$-jet assignment is resolved
in the following way. 
For a given lepton-jet pairing (lep1-jet1, lep2-jet2), the
 kinematic solution with the lowest $m(t\bar{t})$ is taken, as
 described above.  The smearing is performed and the reconstruction of
 the $t\bar{t}$ kinematics  for a given pairing is tried $N_{\text{smear}}$
 times. There could be no solution  found given the ``smeared'' jets
 and $\MET$. Therefore, for a given event, it is possible to end up
 with  less reconstructed trials ($N_{\text{smear}}^{\text{reco}}$) than what was
 originally tried  ($N_{\text{smear}}$). For the second lepton-jet pairing
 possibility (lep1-jet2, lep2-jet1), the previously described
 procedure is repeated and a different number of reconstructed trials
 is obtained. The correct pairing is chosen to be the one that has
 more reconstructed trials, i.e. the one that maximizes
 $N_{\text{smear}}^{\text{reco}}/N_{\text{smear}}$. Once the pairing
 is chosen, the \ttbar system is reconstructed according to the
 described procedure. If the initial set of particles, this means the
 unsmeared set, does not lead to
 a solvable system of equations, the first smeared set that leads to a
 solution is taken for the measurement.


\subsection{Comparison of Data and Simulation}
We compare the data and expectations after the full dilepton selection and
$t\bar{t}$ kinematic reconstruction performed by the KIN method.

Figures~\ref{fig:dataMC1reco} to~\ref{fig:dataMC3reco} show the
data/prediction comparisons after the final selection. The shaded area shows the
uncertainty on the signal and background normalization as for the
selection plots.
 Figure~\ref{fig:dataMC1reco} shows the top/antitop $p_T$ and $\eta$,
 figure~\ref{fig:dataMC2reco} the $t\bar{t}$ pair
 $p_T$ and $m_{t\bar{t}}$ and
 figure~\ref{fig:dataMC3reco} transverse-/r-axis $\phi$ and $\eta$
 distributions for all three individual
 dilepton channels.  
The overall agreement between Monte Carlo prediction and data is
good. 

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_top_pt_ratio.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_antitop_pt_ratio.pdf} \\
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_top_eta_ratio.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_antitop_eta_ratio.pdf}
%    \includegraphics[scale=0.25]{figures/reco/partonic_tbar_pt_reco.eps}
%    \includegraphics[scale=0.25]{figures/reco/partonic_tbar_pt_reco.eps}
    \caption{Comparison of the top/antitop $p_{\text{T}}$ and $\eta$ distributions between data and predictions
      after the kinematic reconstruction in the inclusive dilepton channel. The data/expectation ratio is also
      shown. The shaded area shows the uncertainty on signal and
      background coming from statistics, normalization and detector systematics. } 
    \label{fig:dataMC1reco}
  \end{center}
\end{figure}

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_ttbar_pt_ratio.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_ttbar_m_ratio.pdf}
    \caption{Comparison of the \ttbar $p_{\text{T}}$ and mass distributions between data and predictions
      after the kinematic reconstruction in the inclusive dilepton channel. The data/expectation ratio is also
      shown. The shaded area shows the uncertainty on signal and
      background coming from statistics, normalization and detector systematics. } 
    \label{fig:dataMC2reco}
  \end{center}
\end{figure}

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_trans_phi_ratio.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_r_phi_ratio.pdf} \\
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_trans_eta_ratio.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_r_eta_ratio.pdf}
    \caption{Comparison of the transverse-/r-axis $\phi$ and $\eta$ distributions between data and predictions
      after the kinematic reconstruction in the inclusive dilepton channel. The data/expectation ratio is also
      shown. The shaded area shows the uncertainty on signal and
      background coming from statistics, normalization and detector systematics. } 
    \label{fig:dataMC3reco}
  \end{center}
\end{figure}

\subsection{Reconstruction uncertainty}

If the initial kinematic conditions of an event don't yield a solution
for the reconstruction of the \ttbar system, the kinematics get
smeared until a solution is found with a maximum of 500 smears. The
smearing of the kinematics uses a random generator with a fixed
seed. As this seed determines what kinematics will be probed next, the
solution of the \ttbar reconstruction of an event depends on that
seed. This dependence implies an uncertainty of the reconstruction
method coming from the choice of the random seed. In order to evaluate
the size of this uncertainty for the measurement of the spin
observables, a subset of the nominal \ttbar sample is taken with the
same statistics as measured on data. These events are reconstructed
using 400 different seeds for the smearing, resulting in 400 different
distributions for all observables. The bins' event numbers are plotted
for each bin and result in Gaussian distributions with a width
$\sigma_{\text{reco}}$ (see Figs.~\ref{}). Finally, this width is compared to
the expected statistical uncertainty $\sigma_{\text{stat}}$ of this bin in the reconstructed distribution to have a feeling on
how big the uncertainty on the final unfolded observable is. This
approach is justified by the pull distribution, for which the bin of the reconstructed
distributions are varied within their Poisson
uncertainty. Figure~\ref{fig:app_kin_unc} shows the ratio of
$\sigma_{\text{reco}}/\sigma_{\text{stat}}$ for the bins of all
obsvervables. As can be seen, the fraction is at least 50\% for all
bins with an average of about 70-80\%. Translated to the final
observable, one would have to add an uncertainty on the reconstructed
method which is about 70-80\% the size of the statistical uncertainty,
which is not acceptable for the reconstruction method.

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{figures/reco/KIN_unc/reconstruction_polarization.pdf}
    \caption{Comparison of the reconstruction and statistical
      uncertainty for the individual bins of all reconstructed
      observable distributions. The reconstruction uncertainty is
      obtained by varying the random seed for the smearing of the
      kinematics 400 times and taking the width of the resulting
      Gaussian distribution for each bin.} 
    \label{fig:app_kin_unc}
  \end{center}
\end{figure}