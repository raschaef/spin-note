\section{Monte Carlo}
In order to study the observables, make predictitions for their
distributions and compare them to the measured data, we use Monte
Carlo (MC) samples to model the signal and background contributions. The
background is composed of the following processes:
\begin{itemize}
\item Single top production in the $W$t channel.
\item Drell-Yan process ($\gamma^{*}/Z$) with additional jets.
\item Diboson production ($WW$, $ZZ$ and $WZ$).
\item Fake background coming from processes with non-prompt leptons as
  will be discussed in sec.~\ref{sec:backgrounds_dilep_dy}.
\end{itemize}
The standard signal and single top background Monte Carlo samples for the measurement are
generated with Powheg (version 1, r2330)~\cite{powhegbox}
 and showered by Pythia6 (version 6.426,, Perugia2011c tune)~\cite{pythia6}. The Drell-Yan background MC
 is generated with Alpgen (version 2.14 )~\cite{alpgen} and also showered with
 Pythia6. The diboson production samples are generated with Alpgen,
 but showered with Herwig (version 6.520.2, AUET2)~\cite{herwig}. The fake background is a
 composition of various processes mimicking the signal topology and
 will be discussed later (see
 sec.~\ref{sec:backgrounds_dilep_dy}). The production of a \ttbar pair
 with an additional vector boson (ttV) is produced with
 Madgraph~\cite{} (version 5.1.4.8 ) and showered with
 Pythia8~\cite{pythia8} (AUET2B tune). More information on the samples used can be
 found in app.~\ref{app:mcsamples}.

\label{sec:truth}
For the further studies, it is necessary to know
the exact definitions that will be used on truth level.
Two different levels of truth information are used and based on them
two different measurements are performed. One of them uses parton level information from the
Monte Carlo generator and the other one takes only stable particles
(stable referring to particles with a mean lifetime $> 0.3 \cdot
10^{-10}\text{s}$).
The main reason is the Monte Carlo
generator dependence for the parton level. Different generators
can have a different definition of what they call parton level. Even
though differences between MC generators should be small, their
definitions are not necessarily the same and do not match what
theorists use in their calculartions. It is therefore preferred to
have a common definition based only on physical particles,
i.e. particles with a lifetime long enough to call them stable and
possibly detect them. Furthermore, the
extrapolation from detector-level to parton-level objects is quite large. At the
detector level, there are only electrons and muons after radiation
and possible \met from the two neutrinos whereas on parton level there
are electrons, muons and taus before radiation, which is
a very different phase space and can be highly generator dependent.  

\subsection{Parton level}
\label{sec:parton_def}
A parton level measurement is desirable as on this level we have a
clear definition for the polarization and spin correlation in the
distributions of the observables, defined in section~\ref{sec:obs}. The
polarization is defined as the slope of the \costheta distribution and
the spin correlation is proportional to the mean of the \coscos
distribution. The same holds for the other observables. Thus, it is necessary
to retrieve the initial top quarks from the \ttbar production and the
leptons directly coming from the $W$ boson decays. Leptons affected by
photon radiation would distort the distributions and make comparison
with theoretical predictions harder. In order to get the proper
particles for the measurement, we rely on the Monte Carlo generator information
and history, which provides the connection between decay products and
their mother
particles and also a status code for each particle. 
The desired top quarks and leptons needed
for the measurement have status code 3, which indicates leptons that
have not subsequently decayed\footnote{The status code refers to Pythia6. In
  the case of Herwig, the top quarks are required to have status code
  155 and the leptons to have status code 123.}. This includes also tau leptons before
having decayed. Taking this definition, the observables can be built and
compared with the theoretical predictions in Table~\ref{tab:numbers_mctheo}.
\begin{table}
  \begin{center}
    \begin{tabular}{r|c|c}
 %     \hline
      Observable & Theory & Monte Carlo \\
      \hline
      $B_{1}(k)$ & $0.0030^{+0.001}_{-0.001}$ & $0.0060\pm0.0007$ \\[0.05cm]
      $B_{2}(k)$ & $0.0034^{+0.001}_{-0.001}$ & $0.0068\pm0.0007$ \\[0.05cm]
      $B_{1}(n)$ & $0.0035\pm0.0004$ & $0.0002\pm0.0007$ \\[0.05cm]
      $B_{2}(n)$ & $0.0035\pm0.0004$ & $-0.0002\pm0.0007$ \\[0.05cm]
      $B_{1}(r)$ & $0.0013^{+0.0010}_{-0.0006}$ & $0.0019\pm0.0007$ \\[0.05cm]
      $B_{2}(r)$ & $0.0015^{+0.0010}_{-0.0007}$ & $0.0028\pm0.0007$ \\[0.05cm]
      $C(k,k)$ & $0.318^{+0.003}_{-0.002}\pm0.017$ & $0.293\pm0.001$ \\[0.05cm]
      $C(n,n)$ & $0.332^{+0.002}_{-0.002}\pm0.002$ & $0.319\pm0.001$ \\[0.05cm]
      $C(r,r)$ & $0.055^{+0.009}_{-0.007}\pm0.03$ & $0.026\pm0.001$ \\[0.05cm]
      $C(k,r)$ & $-0.126^{+0.007}_{-0.015}$ & $-0.111\pm0.001$ \\[0.05cm]
      $C(r,k)$ & $-0.125^{+0.007}_{-0.015}$ & $-0.114\pm0.001$ \\[0.05cm]
      $C(n,r)$ & $0$ & $0.000\pm0.001$ \\[0.05cm]
      $C(r,n)$ & $0$ & $-0.000\pm0.001$ \\[0.05cm]
      $C(n,k)$ & $0$ & $-0.002\pm0.001$ \\[0.05cm]
      $C(k,n)$ & $0$ & $0.001\pm0.001$ \\[0.05cm]
%      \hline
      \end{tabular}
      \caption{Comparison of polarization and spin correlation values
        from theoretical predictions(~\cite{spinobspaper}) and the
        signal MC sample(see app.~\ref{app:mcsamples}).}
   \label{tab:numbers_mctheo}
    \end{center}
\end{table}
The uncertainties for the theoretical predictions in
table~\ref{tab:numbers_mctheo} are taken from~\cite{spinobspaper} and
contain the uncertainty by changing the renormalization and
factorization scale by a factor of 0.5 (2) and a second uncertainty for
differences in the expansion of the calculation where provided. There is an overall
good agreement between the Monte Carlo values and the predictions,
although the spin correlation values are slightly smaller in Powheg.

\Cref{fig:truth_dists0,fig:truth_dists1,fig:truth_dists2,fig:truth_dists3} show the comparison of the different spin
correlation observables on truth parton level for SM spin-correlation
and a sample without spin correlation, both generated with Powheg. The
polarizations are shown as well, but no difference between the spin
correlation cases is expected here.
\begin{figure}[htbp]
	\centering
        \subfloat[\helpolplus]{\includegraphics[width=0.49\textwidth]{unfolding/truth/costheta_hel_plus_ratio.pdf}}
%        \qquad
        \subfloat[\transpolplus]{\includegraphics[width=0.49\textwidth]{unfolding/truth/costheta_trans_plus_ratio.pdf}}\\
        \subfloat[\rpolplus]{\includegraphics[width=0.49\textwidth]{unfolding/truth/costheta_r_plus_ratio.pdf}}
        \caption{Comparison of polarization observables in
          different axes on Monte Carlo parton level for SM spin
          correlation and no correlation (which should not affect the
          polarization, any differences come from the generators and
          parton showers themselves).}
        \label{fig:truth_dists0}
\end{figure}
\begin{figure}[htbp]
	\centering
        \subfloat[\helcorr]{\includegraphics[width=0.49\textwidth]{unfolding/truth/helcorr_ratio.pdf}}
%        \qquad
        \subfloat[\transcorr]{\includegraphics[width=0.49\textwidth]{unfolding/truth/transcorr_ratio.pdf}}\\
        \subfloat[\rcorr]{\includegraphics[width=0.49\textwidth]{unfolding/truth/rcorr_ratio.pdf}}
        \caption{Comparison of spin correlation observables in
          different axes on Monte Carlo parton level for SM spin
          correlation and no correlation.}
        \label{fig:truth_dists1}
\end{figure}
\begin{figure}[htbp]
	\centering
        \subfloat[\transhelsum]{\includegraphics[width=0.49\textwidth]{unfolding/truth/transhelsum_ratio.pdf}}
%        \qquad
        \subfloat[\transheldiff]{\includegraphics[width=0.49\textwidth]{unfolding/truth/transheldiff_ratio.pdf}}\\
        \subfloat[\transrsum]{\includegraphics[width=0.49\textwidth]{unfolding/truth/transrsum_ratio.pdf}}
        \subfloat[\transrdiff]{\includegraphics[width=0.49\textwidth]{unfolding/truth/transrdiff_ratio.pdf}}
        \caption{Comparison of spin correlation observables in
          different axes on Monte Carlo parton level for SM spin
          correlation and no correlation.}
        \label{fig:truth_dists2}
\end{figure}
\begin{figure}[htbp]
	\centering
        \subfloat[\rhelsum]{\includegraphics[width=0.49\textwidth]{unfolding/truth/rhelsum_ratio.pdf}}
%        \qquad
        \subfloat[\rheldiff]{\includegraphics[width=0.49\textwidth]{unfolding/truth/rheldiff_ratio.pdf}}
        \caption{Comparison of spin correlation observables in
          different axes on Monte Carlo parton level for SM spin
          correlation and no correlation.}
        \label{fig:truth_dists3}
\end{figure}
A significant difference between the SM spin-correlation and no-correlation sample is seen for the spin-correlation in the helicity and
transverse axes and the R-Hel sum observable, which are the only
observables with a significant difference from 0 in the SM.

\subsection{Stable-particle level}
\label{sec:fiducial_def}
In addition to the parton-level measurement, another measurement with
stable particles is performed. It takes only stable particles for
the truth level definition, which has some advantages compared to MC
parton level. Stable particles are clearly defined and make a
comparison between different Monte Carlo generators feasible. A big
advantage is the small extrapolation from reconstructed to stable
particle level as both consist of only stable particles, i.e. only
electrons/muons and also \met from the neutrinos. A further reduction
of the extrapolation
can be applied by introducing fiducial cuts that resemble the
selection cuts. 

The stable-particle definitions follow the instructions of the TopLHC
working group~\cite{pseudotop}. 
\begin{itemize}
\item Electron: Final state electrons are taken from the truth
  record and their four-vectors clustered with photons with the
  anti-kt algorithm with a distance parameter of $ R=0.1$. No isolation condition is imposed. The parent of the electron is required not to be a hadron. 
\item Muon: Final state muons are taken from the truth record and
  their four-vectors clustered with photons with the anti-kt algorithm
  with a distance parameter of  $ R=0.1$, where leptons (electron and muons) and photons are considered for jet clustering. No isolation condition is imposed. The parent of the muon is required not to be a hadron. 
\item Missing $E_T$/Neutrinos: calculated as the 4-vector sum of
  neutrinos from $W/Z$-boson decays. Tau decays are included. A neutrino is treated as a detectable particle and is selected for consideration in the same way as electrons or muons, i.e. the parent is required not to be a hadron. 
\item Jets:  Reconstructed using the anti-kt algorithm with a distance
  parameter $R=0.4$, clustering all stable particles excluding the electrons, muons, neutrinos, and photons used in the definition of the selected leptons. 
\item $b$-jets:  A jet is ``tagged'' as ab-jet if any $B$ hadron is included in the
  jet, where only $B$ hadrons with an initial $p_{\text{T}} > 5$ GeV are
  considered. This prescription provides an unambiguous way to
  associate a single jet with a B-hadron.
% A jet is a b-jet if any rescaled B-hadron is included in the jet. A rescaled B-hadron is treated as a stable B-hadron (that does not oscillate or decay to another B-hadron) for which the 4-momentum is scaled down to the limit of floating point precision and added to the list of particles for jet-clustering as described above. Only B-hadrons with an initial $p_T > 5$ GeV are considered. This prescription provides an unambiguous way to associate a single jet with a B-hadron.

\end{itemize}

The fiducial phase-space is chosen in a way to resemble the
phase-space at reconstructed level as closely as possible. The following cuts are
applied:
\begin{itemize}
\item At least two selected leptons ($ee$, $e\mu$, $\mu\mu$) with $|\eta| < 2.5$ and $p_{\text{T}} > 25$ GeV.
\item At least two jets in the region $|\eta| < 2.5$ and $p_{\text{T}} > 25$ GeV.
\end{itemize}
These cuts are independent on the channel to be analysed in order to
have one common truth spectrum for each of the observables.

Using the stable particle objects it is possible to reconstruct a
proxy for the top quark (known as pseudo-top~\cite{pseudotop}). The following rules describe the procedure to associate  the object in order to make the pseudo-top particle:
\begin{itemize}
\item Two jets are needed to construct the top and anti-top. If there
  is any $b$-tagged jet in the event, it has priority against the non-tagged jets. In the case of no $b$-tagged jet in the event, the
  highest \pt  jets are taken for the reconstruction.

\item Exactly 2 opposite sign leptons ($e$, $\mu$)

\item Consider the 2 leading $p_T$ selected neutrinos. The two combinations $( \nu_1,\ell_+)(\nu_2,\ell_-)$ or $(\nu_1,\ell_-)( \nu_2,\ell_+)$ where the subindices 1 and 2 refers to $p_T$ ordering and $+$ ,$-$ refers to the lepton sign, are considered. The $(\nu,\ell)$ pair that gives the combined lepton-neutrino four momenta $W_1$ and $W_2$ with the minimal $| M_{W_1} - M_{W_{\text{PDG}}} | + |M_{W_2}-M_{W_{\text{PDG}}} |$, where $M_{W_{\text{PDG}}}$ is the mass of the $W$ boson from the PDG, is considered the proper combination to make the pseudo-$W_{+}$ and the pseudo-$W_{-}$ from the dilepton decay.


\item The pairs $(b\text{jet}_1, W_{+})$, $(b\text{jet}_2, W_{-})$, and $(b\text{jet}_1, W_{-})$, $(b\text{jet}_2, W_+)$ are considered and the pair where the combined $b-jet - W$ four momenta $T_1$ and $T_2$ are such that $| M_{t_1} - M_{\text{top}_{\text{PDG}}} | + |M_{t_2}-M_{\text{top}_{\text{PDG}}} |$ where $M_{\text{top}_{\text{PDG}}}$ is the mass of the top quark from the PDG, is considered the proper combination of $b$s and $W$s to determine the pseudo-top-quarks.
\end{itemize}

%For those lepton and top quark definitions, the polarization and spin
%correlation cannot be extracted directly from the truth distribution
%anymore, which is the disadvantage of this method. 
\Cref{fig:truth_fid_dists0,fig:truth_fid_dists1,fig:truth_fid_dists2,fig:truth_fid_dists3} show the stable-particle distributions for
all the different observables.

\begin{figure}[htbp]
	\centering
        \subfloat[\helpolplus]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/costheta_hel_plus_ratio.pdf}}
%        \qquad
        \subfloat[\transpolplus]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/costheta_trans_plus_ratio.pdf}}\\
        \subfloat[\rpolplus]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/costheta_r_plus_ratio.pdf}}
        \caption{Comparison of polarization observables in
          different axes on stable-particle level for SM spin
          correlation and no correlation.}
        \label{fig:truth_fid_dists0}
\end{figure}

\begin{figure}[htbp]
	\centering
        \subfloat[\helcorr]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/helcorr_ratio.pdf}}
%        \qquad
        \subfloat[\transcorr]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/transcorr_ratio.pdf}}\\
        \subfloat[\rcorr]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/rcorr_ratio.pdf}}
        \caption{Comparison of spin correlation observables in
          different axes on stable-particle level for SM spin
          correlation and no correlation.}
        \label{fig:truth_fid_dists1}
\end{figure}
\begin{figure}[htbp]
	\centering
        \subfloat[\transhelsum]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/transhelsum_ratio.pdf}}
%        \qquad
        \subfloat[\transheldiff]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/transheldiff_ratio.pdf}}\\
        \subfloat[\transrsum]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/transrsum_ratio.pdf}}
        \subfloat[\transrdiff]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/transrdiff_ratio.pdf}}
        \caption{Comparison of spin correlation observables in
          different axes on stable-particle level for SM spin
          correlation and no correlation.}
        \label{fig:truth_fid_dists2}
\end{figure}
\begin{figure}[htbp]
	\centering
        \subfloat[\rhelsum]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/rhelsum_ratio.pdf}}
%        \qquad
        \subfloat[\rheldiff]{\includegraphics[width=0.49\textwidth]{unfolding/truth/fiducial/rheldiff_ratio.pdf}}
        \caption{Comparison of spin correlation observables in
          different axes on stable-particle level for SM spin
          correlation and no correlation.}
        \label{fig:truth_fid_dists3}
\end{figure}