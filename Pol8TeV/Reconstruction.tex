\section{Reconstruction}
\label{sec:reco}

%Changed from KIN method to neutrino weighting

Reconstructing the $t\bar{t}$ kinematics in the dilepton channel is
challenging due to the two neutrinos in the final state, making
the system that relates all the particles' momenta underconstrained. Hence, assumptions are mandatory
to perform the $t\bar{t}$ reconstruction in this channel.

 Considering the process $pp \rightarrow t \bar{t} + X \rightarrow 
 W^{+}b W^{-}\bar{b} + X \rightarrow l^{+}\nu b l^{-}\bar{\nu}\bar{b}
 + X$, the following kinematic constraints can be set:\\ 

%\[
\begin{equation}
  \begin{array}{lll}
    {p}_{b} + {p}_{W^{+}} & = &{p}_{t},\\ 
    {p}_{\bar{b}} + {p}_{W^{-}} & = &{p}_{\bar{t}},\\
    {p}_{l^{+}} + {p}_{\nu} & = & {p}_{W^{+}},\\
    {p}_{l^{-}} + {p}_{\bar{\nu}} & = & {p}_{W^{-}},\\
    p_{\nu_{x}} + p_{\bar{\nu}_{x}} & = & E_{x}^{\text{miss}},\\
    p_{\nu_{y}} + p_{\bar{\nu}_{y}} & = & E_{y}^{\text{miss}}.\\
  \end{array}
  \label{eqn:ttbar_kinematics}
\end{equation}
%\]    

The above set of equations has one constraint fewer than the number of
measured variables. In order to solve this underconstrained system,
certain assumptions have to be made.  
For instance, by specifying a value for the top quark mass, this set
of equations can be solved and the four vectors of $t$ and $\bar{t}$ be determined. 

There are two types of ambiguities which still need to be resolved. First,
the set of equations is equivalent to a polynomial of the 4-th order
in one selected variable. Therefore, there can be up to four real
solutions. Second, even in the simplest case of two leptons and just
two jets in the final state, there is a twofold ambiguity to assign
the lepton and $b$-jet to the proper top quark. Furthermore, one has
to address the possibility of having more  than two jets in the final
state which increases this ambiguity even more. Finally, one also
needs to address the fact that the measured quantities entering the
equations are subject to experimental uncertainties. 

Four reconstruction methods have been tested in order to reconstruct the
top (anti-top) four-momenta: the kinematic method (KIN)~\cite{Abulencia:2006js,Aaltonen:2012tk} where the
system of equations is numerically solved, the matrix element method
(ME)~\cite{Abazov:2004cs,ATLAS-CONF-2012-057} where the probability distribution is calculated based on $gg
\rightarrow  t\bar{t}$ leading-order matrix elements, the neutrino
weighting method (NW)~\cite{Abbott:1997fv,Borroni:1536502} where a
sampling of the rapidity of the two
neutrinos is performed and a \mttwo-based reconstruction method
(MT2)~\cite{mt2reco} that uses the \mttwo~value of the event to get
the transverse momentum of the two neutrinos. The \mttwo~value is
defined as 
\begin{equation}
M_{\text{T2}} \equiv \min_{ \vec{k}_{\text{T}}^{(1)} + \vec{k}_{\text{T}}^{(2)} = \vec{E}_{\text{T}}^{miss} } \left[ \max \left\{  M_{\text{T}} \left( \vec{p}_{\text{T}}^{(1)},\vec{k}_{\text{T}}^{(1)} \right), M_{\text{T}} \left( \vec{p}_{\text{T}}^{(2)},\vec{k}_{\text{T}}^{(2)} \right)\right\}  \right],
\end{equation}
where $\vec{p}_{\text{T}}^{(i)}$ is the transverse momentum of the visible final
state particles in the detector for decay chain $i$ and
$\vec{k}_{\text{T}}^{(i)}$ the transverse momentum of the
neutrino. I.e. the neutrino $p_{x}$ and $p_{y}$ are scanned trying to
find the transverse mass value, which yields the lowest value when
taking the higher transverse mass from each decay chain.
In all the methods, we assume  $m_{W} = 80.4 \ {\rm  GeV}$ and
$m_{\nu} = 0$. 

The details of the studies are documented in the supporting
note~\cite{KinRecoNote}. According to these studies, both KIN and NW
methods show very similar performance, slightly better than the other
two methods (ME and MT2). In fact, the KIN method shows slightly
better resolutions than the NW with the maximum weight, but as there
is a huge reconstruction uncertainty linked to the KIN method (see
App.~\ref{sec:app_kin}), we choose the NW as reconstruction
method in this analysis.
 

\subsection{The neutrino weighting method}

The NW method was used in the past for top mass reconstruction at D0~\cite{Abbott:1997fv}. A brief
description of this method is given in the following.  

From the constraints in Eqns.~\ref{eqn:ttbar_kinematics}, the neutrino
weighting does not use the last two referring to the \met. Instead,
one uses information about the pseudorapidities of the two neutrinos, namely $(\eta_1,
\eta_2)$. The reconstruction method, implemented in this analysis by
the so called {\it Neutrino Weighting Tool} (NWT), consists in
making an hypothesis about a possible combination $(\eta_1, \eta_2)$, and solving the equations
to determine the full neutrino momenta, which can have up to 2
solutions. This is done for each event, scanning over a range of
different $(\eta_1, \eta_2)$ couples, and if in the event there are more
than 2 reconstructed jets, all the possible lepton-neutrino-jet
combination are tested. To each possible solution a weight, $w$, is assigned
based on the compatibility between the reconstructed neutrino momenta
and the measured \met\ in the event. The weight is defined accounting
for the \met\ resolution, namely $(\sigma_x,\sigma_y)$, as:

\begin{equation}
w = \exp{ (-\frac{(E_x^{miss} - p_{x1} -
      p_{x2})^2}{2\sigma_x^2})}  \times \exp{(- \frac{(E_y^{miss} -
     p_{y1} - p_{y2})^2}{2\sigma_y^2})}
\end{equation}

The neutrino pseudorapidity pairs $(\eta_1, \eta_2)$ are tested in a
grid configuration in $\eta$-steps of 0.05 for $5 < |\eta| < 2 $ and
0.025 for $|\eta| < 2$. When a jet combination is tested, each jet is assumed to be a $b$-jet
with the corresponding PDG mass of $m_b = 4.5$ GeV. Since assuming a
perfect jet energy knowledge will be a too tight requirement, each jet energy is
smeared 50 times around the measured energy based on resolution
functions, which are determined in a similar way as the
transfer functions used in KL fitter~\cite{Erdmann:2013rxa}. The 
resolution functions are produced as a function of the jet $p_T$ (for jets) and the
total transverse energy in the event (for $\MET$). 

The reconstruction of the \ttbar\ system in the dilepton channel is a
challenging task; the neutrino weighting method makes use of a number assumptions on the
kinematics of the \ttbar\ production and decay to guess the direction
of the two neutrinos, e.g. that the top quarks and $W$ bosons are
produced perfectly on-shell, which are not always
satisfied in real events. For this reason not all the event topologies can be
reconstructed, and considering all the \ttbar\ MC events after the
dilepton selection, the reconstruction efficiency is about 82\%.

\subsection{Comparison of Data and Simulation}
\label{subsec:reco_cmpr}

We compare the data and expectations after the full dilepton selection and
$t\bar{t}$ kinematic reconstruction performed by the KIN method.

Figures~\ref{fig:dataMC1reco} to~\ref{fig:dataMC3reco} show the
data/prediction comparisons after the final selection. The shaded area shows the
uncertainty on the signal and background normalization as for the
selection plots.
 Figure~\ref{fig:dataMC1reco} shows the top/antitop $p_T$ and $\eta$,
 figure~\ref{fig:dataMC2reco} the $t\bar{t}$ pair
 $p_T$ and $m_{t\bar{t}}$ and
 figure~\ref{fig:dataMC3reco} transverse-/r-axis $\phi$ and $\eta$
 distributions for all three individual
 dilepton channels.  
The overall agreement between Monte Carlo prediction and data is
good. 

%%%%%%%%%% same for the yields...

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{figures/reco/NuWeight/top_pt.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/NuWeight/antitop_pt.pdf} \\
    \includegraphics[width=0.45\textwidth]{figures/reco/NuWeight/top_eta.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/NuWeight/antitop_eta.pdf}
%    \includegraphics[scale=0.25]{figures/reco/partonic_tbar_pt_reco.eps}
%    \includegraphics[scale=0.25]{figures/reco/partonic_tbar_pt_reco.eps}
    \caption{Comparison of the top/antitop $p_{\text{T}}$ and $\eta$ distributions between data and predictions
      after the kinematic reconstruction in the inclusive dilepton channel. The data/expectation ratio is also
      shown. The hatched area shows the uncertainty on signal and
      background coming from MC statistics, normalization, detector
      and signal modelling systematics.} 
    \label{fig:dataMC1reco}
  \end{center}
\end{figure}

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{figures/reco/NuWeight/ttbar_pt.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/NuWeight/ttbar_m.pdf}
    \caption{Comparison of the \ttbar $p_{\text{T}}$ and mass distributions between data and predictions
      after the kinematic reconstruction in the inclusive dilepton channel. The data/expectation ratio is also
      shown. The hatched area shows the uncertainty on signal and
      background coming from MC statistics, normalization, detector
      and signal modelling systematics. } 
    \label{fig:dataMC2reco}
  \end{center}
\end{figure}

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{figures/reco/NuWeight/trans_phi.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/NuWeight/r_phi.pdf} \\
    \includegraphics[width=0.45\textwidth]{figures/reco/NuWeight/trans_eta.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/NuWeight/r_eta.pdf}
    \caption{Comparison of the transverse-/r-axis $\phi$ and $\eta$ distributions between data and predictions
      after the kinematic reconstruction in the inclusive dilepton channel. The data/expectation ratio is also
      shown. The hatched area shows the uncertainty on signal and
      background coming from MC statistics, normalization, detector
      and signal modelling systematics. } 
    \label{fig:dataMC3reco}
  \end{center}
\end{figure}


\begin{table}[htbp]
  \begin{center}
    \caption{Observed number of data events at reconstruction level in comparison to the
      expected number of signal events and different background
      contributions from MC predictions and data-driven methods (DD) as
      described in the text. The given uncertainty comes from MC
      statistics, normalization and detector systematics.} 
    \label{tab:evtnumbers_reco}
    \input{tables/yields_reco}
  \end{center}
\end{table}
%\begin{figure}[!htbp]
% \begin{center}
% \includegraphics[scale=0.25]{figures/reco/ee_top_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_top_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_top_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/ee_tbar_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_tbar_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_tbar_pt_reco.eps}
 %  \caption{Comparisons of top/antitop $p_T$, $t\bar{t}$ distributions between data and predictions
 %    after kinematic recontruction in the $ee$ (left), $e\mu$ (middle)
 %    and $\mu\mu$   channels (right). The data/expectation ratio is also
 %    shown. The shaded area shows the uncertainty on the signal and
 %    background normalization. } 
%   \label{fig:dataMC1reco}
% \end{center}
%\end{figure}

%\begin{figure}[!htbp]
% \begin{center}
% \includegraphics[scale=0.25]{figures/reco/ee_ttbar_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_ttbar_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_ttbar_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/ee_ttbar_m_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_ttbar_m_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_ttbar_m_reco.eps}

%   \caption{Comparisons $t\bar{t}$ pair $p_T$,
 %    and $m_{t\bar{t}}$ distributions between data and predictions
  %   after kinematic recontruction in the $ee$ (left), $e\mu$ (middle)
 %    and $\mu\mu$   channels (right). The data/expectation ratio is also
 %    shown. The shaded area shows the uncertainty on the signal and
 %    background normalization. } 
 %  \label{fig:dataMC2reco}
% \end{center}
%\end{figure}


%\begin{figure}[!htbp]
% \begin{center}
% \includegraphics[scale=0.25]{figures/reco/ee_top_y_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_top_y_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_top_y_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/ee_tbar_y_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_tbar_y_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_tbar_y_reco.eps}
%   \caption{Comparisons of top and antitop rapidity distribution
%     between data and predictions after event 
%     selection in the $ee$ (left), $e\mu$ (middle) and $\mu\mu$
%     channels (right). The data/expectation ratio is also shown.  The shaded
%     area shows the uncertainty on the signal and background
%     normalization. } 
%   \label{fig:dataMC3reco}
% \end{center}
%\end{figure}

% \begin{figure}[!htbp]
%  \begin{center}
%    \includegraphics[scale=0.25]{figures/reco/ee_delta_y_reco.eps}
%    \includegraphics[scale=0.25]{figures/reco/emu_delta_y_reco.eps}
%    \includegraphics[scale=0.25]{figures/reco/mumu_delta_y_reco.eps}
%    \caption{Distributions of the $\Delta |y|$ variable in data and
%      MC after event selection in the $ee$ (left), $e\mu$ (middle) and
%      $\mu\mu$ channels (right). The data/MC ratio is also shown.  The
%      shaded area shows the uncertainty on the signal and background
%      normalization. } 
%    \label{fig:dataMC3reco}
%  \end{center}
% \end{figure}
