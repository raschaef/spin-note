\section{Unfolding}
\label{sec:fbu}
% With the reconstructed distributions (see section~\ref{sec:reco}), one
% wants to retrieve observables sensitive to the different parameters from the spin density
% matrix. In principle one could use a template fit as it was done for
% polarization and spin correlation analyses on the 7 TeV data. However,
% this is not preferred in this analysis as it cannot be guaranteed that
% all of the observables are perfectly described in the Monte Carlo or
% that there are no theoretical descriptions for the distributions like
% the cross-correlations, which makes it impossible to create
% templates. In order to circumvent these issues and to minimise the MC
% dependence on the result, an unfolding procedure is applied to the
% reconstructed distributions. We use the Fully Bayesian Unfolding
% (FBU~\cite{FBU}) to unfold the data to two different levels. Once we
% unfold back to the MC parton level (in this case MC is the
% Powheg parton description), which allows us to compare the
% observables obtained from the unfolded distribution to theoretical
% predictions. For the polarization observables this would be the slope
% of the distribution and for the correlation observables the mean of
% it. As there might be some ambiguity in the definition of parton level
% between different Monte Carlo generators and compared to the theory
% prediction, we further provide unfolded distributions of all
% observables on a stable-particle level, i.e. using only particles of a
% certain life-time to recreate our top quarks. Details on the
% definitions and cuts applied to create a fiducial phase space can be
% found in section~\ref{sec:truth}.

\subsection{FBU}
The Fully Bayesian Unfolding~\cite{FBU} consists of the application of Bayes'
theorem to the problem of unfolding. This application can be stated in
the following terms: given an observed spectrum
$\Data\in\Integer^{N_r}$ and a migration matrix
$\TrasfMatrix\in\Real{}^{N_r}\times{}\Real{}^{N_t}$ with $N_{r}$ ($N_{t}$)
being the number of bins of the reconstructed (truth) distribution, the posterior
probability of the true spectrum $\Truth{}\in{}\Real{}^{N_t}$ follows
the probability density
\begin{equation}
\conditionalProb{\Truth{}}{\Data{},\TrasfMatrix{}}
\propto{}
\conditionalLhood{\Data{}}{\Truth{},\TrasfMatrix{}}
\cdot{}
\pi{}\left(\Truth{}\right)
\end{equation}
where \conditionalLhood{\Data{}}{\Truth{},\TrasfMatrix{}} is the
conditional likelihood for the data \Data{} assuming the
truth \Truth{} and the transfer matrix \TrasfMatrix{}, and \pi{} is
the prior probability density for the truth \Truth{}. The method is
implemented in a python framework~\cite{PyFBU}, originally developed
for the charge asymmetry measurement at 8 TeV.

\subsubsection{FBU Ingredients}
Under the assumption that the data events follow a poissonian distribution, the
likelihood \conditionalLhood{\Data{}}{\Truth{},\TrasfMatrix{}} can be
computed from the following two pieces of information:
\begin{itemize}
\item the transfer matrix \TrasfMatrix{}, whose element
  $\TrasfMatrix{}_{tr}$ is the probability $P(t,r)$ of an event to be
  produced in the true bin $t$ and to be observed in the reconstructed
  bin $r$;
\item the efficiency $\epsilon{}_t$ for an event produced in the
  true bin $t$ and to be reconstructed in any bin $r$.
\end{itemize}
%
While both of the above quantities can be estimated from the simulated
sample of signal events, the prior probability density \prior{} has to
be chosen according to what we know about \Truth{} before the
measurement is performed. The choice of a prior within the FBU context
is equivalent to the choice of a regularization in other well-known
unfolding techniques. After choosing a prior, and therefore making
some well-defined assumptions about the spectrum being unfolded, one
can determine the posterior probability density function
$\conditionalProb{\Truth{}}{\Data{}}$. This is done by generating a
sufficient number of uniformly-distributed points in the
$N_t$-dimensional space, and evaluating \conditionalLhood{\Data{}}{\Truth{}} and
\prior{} for each of them. The background in each bin is taken into account when computing
\conditionalLhood{\Data{}}{\Truth{}}.
Combining this set of points with the weight given by
$\conditionalLhood{\Data{}}{\Truth{},\TrasfMatrix{}}\cdot{}\pi{}$, one
can determine not only the posterior probability density distribution
for each bin of the spectrum, but also the posterior probability
density distribution for any quantity that is computed from the
spectrum, such as the polarization or the spin correlation.


\subsubsection{Prior Choice}
The simplest prior corresponds to the assumption that all points are
equally probable:
\begin{equation}
\prior{}
\propto{}
\begin{cases}
1 & \text{if }
T_{t}\in{}\left[\Tinf{}, \Tsup{}\right], \forall{}t\in{}\left[1, N_t\right] \\
0 & \text{otherwise} \\
\end{cases}
\end{equation}
where $\left[\Tinf{}, \Tsup{}\right]$ indicates, for each dimension
$t$, the finite subset of \Integer{} over which the numerical
integration is being performed.

%We use this flat prior for the unfolding of the differential \AC{}
%spectra as a function of \mtt{},\pttt{} and \betatt{}.

A more general expression can be written as follows
\begin{equation}
\prior{}
\propto{}
\begin{cases}
e^{\alpha{}S(\Truth{})} & \text{if }
T_{t}\in{}\left[\Tinf{}, \Tsup{}\right], \forall{}t\in{}\left[1, N_t\right] \\
0 & \text{otherwise} \\
\end{cases}
\end{equation}
where $\alpha{}$ is an arbitrary regularization parameter, and
$S(\Truth{})$ is the difference between the
\emph{Tikhonov} regularization functions~\cite{tikhonov_rew} $C(\Truth)$ (i.e. curvatures) 
for the given point \Truth{} and for the truth distribution in simulation.
The choice of $\alpha$ determines the impact of the prior on $\conditionalProb{\Truth{}}{\Data{}}$
, while $S(\Truth{})$ determines what additional information is being used to constrain the parameter space, thus reducing the variance of the T parameters by introducing a
small bias. In the analysis, the uniform prior is used for all
measurements.
%Ralph NOTE: Have to change in case of regularisation

\subsubsection{Marginalization}

The treatment of systematic uncertainties is naturally included in the Bayesian inference approach by
extending the likelihood \conditionalLhood{\Data{}}{\Truth{}} with nuisance parameters terms. The marginal likelihood is defined as
\begin{equation}
\conditionalLhood{\Data{}}{\Truth{}} = \int \conditionalLhood{\Data{}}{\Truth{},\boldsymbol{\theta}}\cdot \pi(\boldsymbol{\theta})d\boldsymbol{\theta},
\end{equation}

where $\boldsymbol{\theta}$ are the nuisance parameters, and
$\pi(\boldsymbol{\theta})$ their prior probability densities, which
are assumed to be Gaussian distributions G with $\mu = 0$ and $\sigma
= 1$. A nuisance parameter is associated with each of the uncertainty
sources described in sec~\ref{sec:systs}. Two categories of nuisance parameters are considered: the normalizations of the background processes ($\theta_b$), and the uncertainties associated to the objects identification, reconstruction and calibration ($\theta_s$). While the first ones only affect the background predictions, the latter, referred to as
object systematic uncertainties, affect both the reconstructed distribution for $t\bar{t}$ signal and the total background prediction, referred to as $R(T; \theta_s)$ and $B(\theta_s, \theta_b$), respectively. The marginal likelihood becomes then

\begin{equation}
\conditionalLhood{\Data{}}{\Truth{}} = \int \conditionalLhood{\Data{}}{R(T; \theta_s),B(\theta_s, \theta_b)}\cdot G(\theta_s) \cdot G(\theta_b) d\theta_s d\theta_b.
\end{equation}
\subsection{Parton level unfolding}
\label{sec:unf_parton}
The reconstructed distributions of the spin observables will be
unfolded back to the parton level description of
Powheg. \Cref{fig:truth_dists1,fig:truth_dists2,fig:truth_dists3}
showed the truth distributions of the
polarizations and spin correlations for the different bases defined in
section~\ref{sec:obs}. 
The polarization is obtained by building the
$\cos\theta$ distribution separately for positive and negative
leptons, normalizing the distribution to an integral of 1 and fitting
a linear curve to the distribution. In case of a non-equidistant
binning, the bin content was divided by the bin width beforehand. The slope of the fit represents
the polarization as discussed in section~\ref{sec:obs}. The spin correlation is obtained by taking the mean
of the corresponding $\cos\theta_{1}\cos\theta_{2}$ distribution and
multiplying it with an additional factor of $-9$. For both types of distributions only dilepton events
are considered, i.e. no leptons from semileptonic \ttbar decays for
the polarization and no spin correlation between leptons and quarks.

The goal is to combine the results of all three channels, but for a
combination after the unfolding, one has to carefully think about the
correlation of uncertainties. A better way is to combine the channels in the unfolding
procedure itself. As can be seen in app.~\ref{app:resmats_chnl}, the
different channels do not
have compatible response matrices, so the channels should not be
merged. Instead, a rectangular response matrix is
used, that combines the channels by adding more reconstructed bins to
the response matrix (the same number for each
channel), but unfolding back to the same truth distribution, i.e. the
response matrix has 3 times as many reconstructed bins as truth
bins. Figure~\ref{fig:unf_example} shows a dummy input signal
distribution and the corresponding response matrix for the helicity
spin correlation with a 6 bin configuration.
\begin{figure}[htbp]
	\centering
        \subfloat[Example \ttbar spin correlation distribution in the helicity
        axes as input for the unfolding]{\includegraphics[width=0.75\textwidth]{unfolding/input/helcorr_6BinCMS.png}}
        \\
        \subfloat[Example response matrix of the spin correlation in
        the helicity axis as it used in the unfolding]{\includegraphics[width=0.75\textwidth]{unfolding/resmats/Rect_helcorr_8Bin.png}}
        \caption{Example of a signal input distribution and its
          response matrix as used in the unfolding. The different
          channels for the response matrix are clearly visible and
          follow the same order as for the input distribution.}
        \label{fig:unf_example}
\end{figure}

The whole unfolding of the different correlations is performed on the
mean of the distribution. The factor of -9 is applied later, as well
as a calibration factor for the mean of the spin correlations (see~\ref{app:mean_calib}).

\subsubsection{Binning choice}
\label{subsubsec:unf_bin}
An optimal binning for each observable should be chosen. The choice is based on the smallest statistical
uncertainty, the linearity in each observable and the pulls of the
observables and their distributions. The linearity is
determined by reweighting the observables to introduce different
levels of polarizations
and correlations. For the polarizations and the spin correlations in
the same basis ($C(i,i)$), the double-differential
cross section (see Eqn.~\ref{eqn:double-diff}) is used. In order to eliminate
the polarization and spin correlation in the Monte Carlo and to
introduce the new values, the following weights $w_{\text{pol}}$ and
$w_{\text{corr}}$ are used:
\begin{eqnarray}
w_{\text{pol}} &=& \frac{1 \pm P_{\text{rew}}\cos\theta_1 \pm
    P_{\text{rew}}\cos\theta_2-C_{\text{theo}} \cos\theta_1\cos\theta_2}{1 \pm P_{\text{MC}}\cos\theta_1 \pm
    P_{\text{MC}}\cos\theta_2-C_{\text{MC}} \cos\theta_1\cos\theta_2}  \nonumber \\
w_{\text{corr}} &=& \frac{1  -C_{\text{rew}} \cos\theta_1\cos\theta_2}{1 \pm P_{\text{MC}}\cos\theta_1 \pm
    P_{\text{MC}}\cos\theta_2 -C_{\text{MC}} \cos\theta_1\cos\theta_2}.
\label{eqn:rew_pol}
\end{eqnarray}
In the weights, $\cos \theta_{1/2}$ refer to the angles as described
in~\ref{sec:obs}, but taken from the truth record and
not from the reconstructed observables. The factors $P_{\text{rew}}$
and $C_{\text{rew}}$ are the polarization and correlation,
respectively, to which the distribution are reweighted, $P_{\text{MC}}$
and $C_{\text{MC}}$ refer to the polarization and spin correlation
present in the used MC sample and $C_{\text{theo}}$ refers to the SM
spin correlation value. \\
For the sums and differences of the cross correlations, a different
reweighting has to be chosen as there is no analytic description for
 adding or subtracting two \coscos terms. Since there is usually a
 linear dependence of the differential cross section on the spin
 correlation and since we take the mean of the distribution for the unfolding, a simple linear
 reweighting is chosen based on the truth observable value $obs_{\text{truth}}$,
\begin{equation}
  w_{\text{crosscorr}} = 1 \pm f_{\text{rew}} obs_{\text{truth}},
\end{equation}
with $f_{\text{rew}}$ being the reweighting factor. This approach is
supported by the linear behaviour of the ratios in \Cref{fig:truth_dists0,fig:truth_dists1,fig:truth_dists2,fig:truth_dists3}.
The polarization observables are reweighted for polarizations between
$-0.1 < P_{i} < 0.1$, the spin correlations for helicity and
transverse axes between $0.20 < C(i,i) < 0.44$, in the r axis
between $-0.1 < C(r,r) < 0.1$ and for the sums and differences of
\coscos terms between $-0.15 < C(i,j) \pm C(j,i) < 0.15$. The
values are chosen in a way that they should cover the measured values
based on the expected statistical uncertainties. Each reweighted distribution is eventually
unfolded with the nominal response matrix and the result compared to the
generated value. In the ideal case, the unfolding is independent on
the polarization or spin correlation. Thus, the
calibration curve using all the results from the reweighted
distributions would yield a slope
of 1 and no offset, which is what is tested for. The widths of the bins that were chosen for the observables are based
on the resolutions and the statistics for each of them. %Furthermore
The diagonal entries of the response matrices should yield the highest values.

For each observable, around 6-8 binnings are tested. The binning definitions for each observable can be found in
appendix~\ref{app:moreplots}. An example for the binning choice is
shown for the transverse correlation in the following. Figure~\ref{fig:binning_opti_transcorr} show % 2000 ensembles for the pulls etc.
the results of slopes and offsets for the calibration curves, the
pulls of observables and their distributions and the relative
difference of the ensembles with respect to the truth distribution,
along with an average $\chi ^{2}$. The latter plots are obtained by
performing 2000 ensemble tests. Their uncertainties are taken from the
width of the resulting distributions. As can be seen from
\Cref{fig:unf_bin_transcorr_slopes,fig:unf_bin_transcorr_offsets}, the
offsets are negligible for low-Bin configurations, while the slopes
get better for higher bin configurations. The trend of the offsets
also appears in the pulls of
Fig.~\ref{fig:unf_bin_transcorr_pulls}. The pulls of the distributions
as well as the observable values are close to zero for 4- and 6-bin
configurations, while instablities can be observed for some 8-bin and
the 10-bin configurations. On the other hand, the expected statistical
uncertainty decreases with the number of bins. Therefore, a compromise
has to be found regarding the uncertainties and the instabilities,
resulting in a 6-bin configuration as the best option for the
transverse correlation. Within 6 bins, the different binnings are
quite compatible as can be seen from the uncertainties of the slopes
and offsets of the calibration curve (see Fig.~\ref{fig:binning_calib_transcorr_unc}). All other
observable binnings are chosen in the same way as for the transverse
correlation and are shown in appendix~\ref{app:moreplots}.

\begin{figure}[htbp]
	\centering
        \subfloat[Slopes]{\includegraphics[width=0.45\textwidth]{unfolding/binning/slopes_transcorr_rpZero.eps}\label{fig:unf_bin_transcorr_slopes}}
%        \qquad
        \subfloat[Offsets]{\includegraphics[width=0.45\textwidth]{unfolding/binning/offsets_transcorr_rpZero.eps}\label{fig:unf_bin_transcorr_offsets}}
        \\
        \subfloat[Pulls]{\includegraphics[width=0.45\textwidth]{unfolding/binning/pull_transcorr.eps}\label{fig:unf_bin_transcorr_pulls}}
%        \qquad
        \subfloat[Relative difference]{\includegraphics[width=0.45\textwidth]{unfolding/binning/relative_transcorr.eps}\label{fig:unf_bin_transcorr_relative}}
        \caption{Binning optimisation plots for the transverse
          correlation. Shown are the slopes and offsets of the
          calibration curves for different binnings (top), the pulls
          of the observables and its unfolded distributions (bottom
          left) as well as the relative differences of the ensembles
          with respect to the truth distribution.}
        \label{fig:binning_opti_transcorr}
\end{figure}

\begin{figure}[htbp]
	\centering
        \subfloat[Slope uncertainty]{\includegraphics[width=0.45\textwidth]{unfolding/binning/slopes_unc_transcorr_rpZero.png}\label{fig:unf_bin_transcorr_slopes_unc}}
%        \qquad
        \subfloat[Offset uncertainty]{\includegraphics[width=0.45\textwidth]{unfolding/binning/offsets_unc_transcorr_rpZero.png}\label{fig:unf_bin_transcorr_offsets_unc}}
        \caption{The slopes (left) and offsets (right) of the
          calibration curves for two different binnings of the
          transverse correlation along with the uncertainty on the
          slope/offset (grey band) for one binning.}
        \label{fig:binning_calib_transcorr_unc}
\end{figure}

%Results
%for the statistical uncertainty, the slope
%and offset of the calibration curve can be found in
%tables~\ref{tab:binning_opti_parton_helpol_plus}--\ref{tab:binning_opti_parton_rheldiff}. The offset is evaluated around the
%theoretical or expected values, which means around 0.3 for the spin
%correlation in the helicity and transverse bases, around -0.2 for the R-Hel
%sum observable and 0 elsewhere. As a result of the optimisation, the
%chosen binnings are listed in table~\ref{tbl:chosen_binnings}.

%have to add the binning tables again... were not yet part of this version

\begin{table}
  \begin{center}
    \begin{tabular}{c|c|l}
      observable & binning name & binning \\ \hline
      \helpol & 4Bin4 & [-1.,-0.4,0.0,0.5,1.] \\ %[-1.,-0.4,0.0,0.5,1.] \\
      \transpol & 4Bin1 & [-1.,-0.5,0.,0.5,1.] \\%[-1.,0.0,1.] \\
      \rpol & 6Bin & [-1.,-0.5,0.,0.5,1.] \\ %[-1.,-0.66,-0.33,0.,0.33,0.66,1.] \\
      \helcorr & 6Bin2 & [-1.,-0.6,-0.25,0.,0.25,0.6,1.] \\%[-1.,-0.55,-0.325,-0.15,0.,0.15,0.325,0.55,1.]
      \transcorr & 6Bin2 &
      [-1.,-0.6,-0.25,0.,0.25,0.6,1.] \\ %[-1.,-0.55,-0.325,-0.15,0.,0.15,0.325,0.55,1.] \\
      \rcorr & 6Bin2 &
      [-1.,-0.66,-0.33,0.,0.33,0.66,1.] \\ %[-1.,-0.65,-0.425,-0.2,0.,0.2,0.4,0.6,1.] \\
      \transhelsum & 6Bin1 &
      [-1.,-0.6,-0.3,0.,0.3,0.6,1.] \\%[-1.,-0.6,-0.3,0.,0.3,0.6,1.] \\
      \transheldiff & 6Bin1 &
      [-1.,-0.6,-0.3,0.,0.3,0.6,1.] \\ %[-1.,-0.6,-0.3,0.,0.3,0.6,1.] \\
      \transrsum & 6Bin1 &
      [-1.,-0.6,-0.3,0.,0.3,0.6,1.] \\ %[-1.,-0.6,-0.3,0.,0.3,0.6,1.] \\
      \transrdiff & 6Bin1 &
      [-1.,-0.6,-0.3,0.,0.3,0.6,1.] \\ %[-1.,-0.65,-0.3,0.,0.3,0.65,1.] \\
      \rhelsum & 6Bin1 &
      [-1.,-0.6,-0.3,0.,0.3,0.6,1.] \\ %[-1.,-0.67,-0.34,0.,0.33,0.66,1.] \\
      \rheldiff & 6Bin1 &
      [-1.,-0.6,-0.3,0.,0.3,0.6,1.] \\ %[-1.,-0.6,-0.325,-0.05,0.25,0.6,1.] \\
    \end{tabular}
    \caption{Chosen binning configurations for the partonic measurement}
    \label{tbl:chosen_binnings}
  \end{center}
\end{table}

%For the spin correlations, a 10-bin configuration often yields the best
%results for the calibration curve and the statistical
%uncertainty. However, unfolding the reconstructed distribution and
%comparing it to the truth distribution shows a small
%oscillating behaviour in the ratio for the bins (see
%Fig.~\ref{fig:unf_heltrans_10Bin}). Perfect non-closure would be
%expected, but instead alternating up- and down-fluctuations at the
%level of a few percent can be observed. The same trend can be
%seen for the cross correlations with an 8-bin
%configuration. This is an indication that the bin widths are chosen to
%small compared to the resolution for each bin. Therefore, it was decided to neglect binning options
%with that high amount of bins for the corresponding observables.

 Appendix~\ref{app:moreplots} contains the response
matrix, acceptance efficiency distribution, calibration curve and a comparison of the truth und unfolded
reconstructed distribution for each observable with the binning chosen
from the previous tests.


%\begin{figure}[htbp]
%	\centering
%        \subfloat[helicity correlation]{\includegraphics[width=0.45\textwidth]{unfolding/studies/helcorr_reco_10Bin.png}\label{fig:unf_hel_10Bin}}
%%        \qquad
%        \subfloat[transverse correlation]{\includegraphics[width=0.45\textwidth]{unfolding/studies/transcorr_reco_10Bin.png}\label{fig:unf_trans_10Bin}}
%        \caption{Comparison of the unfolded helicity and transverse
%          correlation with the corresponding MC truth distribution in
%          the 10 bin configuration.}
%        \label{fig:unf_heltrans_10Bin}
%\end{figure}

\subsection{Stable-particle level unfolding}
\label{sec:unf_fiducial}
The correction of the measured data back to stable-particle level
using a fiducial region, as it has been described in
Sec.~\ref{sec:fiducial_def}, is performed similarly to the unfolding
back to parton level (see Sec.~\ref{sec:unf_parton}). The main
differences are the truth observable distributions, which are built
using the pseudo-top information and the reconstructed signal MC
distribution. The reco distribution is now split into events which also pass the
truth selection cuts (see Sec.~\ref{sec:fiducial_def}) and those which
don't (Non-fiducial background). As the pseudo-tops are considered as
the true tops in this measurement, the reweighting as part of the
linearity test is performed using also the pseudo-top information. The
rest of the unfolding procedure stays the same with respect to the
parton level measurement.

\subsubsection{Non-fiducial background}
Defining a fiducial region on truth level leads to the situation, that
events can be reconstructed, but don't have any corresponding truth
information, because they fall out of the fiducial region. Those events cannot be filled in the response matrix,
, because the information of one of the axis is missing. Thus, the response
matrix connects only reconstructed events having its corresponding
truth information (reco) and the truth information, built from
stable-particles with fiducial cuts. However, in data it is not
possible to distinguish between reco and non-fiducial or any other
background event. Therefore, the non-fiducial part of the signal needs
to be treated as a background in the unfolding procedure. A crucial
property of this particular background is, that it needs to vary with the
normalization or underlying observable which is meant to be measured,
as it is still part of the \ttbar signal. With this requirement, the
non-fiducial background
is connected to the signal by taking a fixed fraction of the
reconstructed signal events per bin and observable. \Cref{fig:helcorr_nonfid_ratios} shows the
fraction of non-fiducial to reco events for different levels of
polarization/correlation depending on the observable. As can be seen
from the plots, the ratio is constant within the uncertainties for
all reweighted distributions. For the measurement, the fractions from
the nominal distributions are taken and applied to the
background-subtracted data to obtain the non-fiducial background. The
full distribution with all the backgrounds are then passed to the
unfolding procedure. Figures~\ref{fig:helpolcorr_nonfid_ratios} to~\ref{fig:rhel_nonfid_ratios} show the fractions for each bin and
each observables used for the measurement.

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.7\textwidth]{figures/unfolding/nonfid/helcorr_8Bin3_allratios_final}
   \caption{Fraction of nonfiducial to all reconstructed signal (nonfid.+fid.) events for the
     helicity correlation for different levels of spin
     correlation. The figure is splitted for the different channels
     \eeme (left), \mumume (middle) and \emume (right). The ratio with respect to the SM fraction is shown
     at the bottom of the figure. (KIN method plot)} 
   \label{fig:helcorr_nonfid_ratios}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_helpol_plus_fiducial.pdf}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_helcorr_fiducial.pdf}
   \caption{Fraction of non-fiducial to all reconstructed signal (nonfid.+fid.) events for the
     helicity polarization (left) and helicity correlation (right).} 
   \label{fig:helpolcorr_nonfid_ratios}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_transpol_plus_fiducial.pdf}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_transcorr_fiducial.pdf}
   \caption{Fraction of non-fiducial to all reconstructed signal (nonfid.+fid.) events for the
     transverse polarization (left) and transverse correlation (right).} 
   \label{fig:helpolcorr_nonfid_ratios}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_rpol_plus_fiducial.pdf}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_rcorr_fiducial.pdf}
   \caption{Fraction of non-fiducial to all reconstructed signal (nonfid.+fid.) events for the
     r-axis polarization (left) and r-axis correlation (right).} 
   \label{fig:rpolcorr_nonfid_ratios}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_transhelsum_fiducial.pdf}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_transheldiff_fiducial.pdf}
   \caption{Fraction of non-fiducial to all reconstructed signal (nonfid.+fid.) events for the
     \transhelsum (left) and \transheldiff (right) observables.} 
   \label{fig:transhel_nonfid_ratios}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_transrsum_fiducial.pdf}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_transrdiff_fiducial.pdf}
   \caption{Fraction of non-fiducial to all reconstructed signal (nonfid.+fid.) events for the
     \transrsum (left) and \transrdiff (right) observables.} 
   \label{fig:transr_nonfid_ratios}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_rhelsum_fiducial.pdf}
   \includegraphics[width=0.45\textwidth]{figures/unfolding/nonfid/NuWeight/non_fiducial_rheldiff_fiducial.pdf}
   \caption{Fraction of non-fiducial to all reconstructed signal (nonfid.+fid.) events for the
     \rhelsum (left) and \rheldiff (right) observables.} 
   \label{fig:rhel_nonfid_ratios}
 \end{center}
\end{figure}


%\begin{landscape}
%\begin{table}
%  \tiny
%  \begin{center}
%    \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
%      \hline 
%      Observable & Bin1 & Bin2 & Bin3 & Bin4 & Bin5 & Bin6 & Bin7 &
%      Bin8 & Bin9 & Bin10 & Bin11 & Bin12 & Bin13 & Bin14 & Bin15 & Bin16 &
%      Bin17 & Bin18 & Bin19 & Bin20 & Bin21 & Bin22 & Bin23 & Bin24
%      \\
%      \hline
%     \helpol & 0.0733 & 0.0774& 0.0800& 0.0867& 0.0528& 0.0567& 0.0588& 0.0646&
%     0.0619& 0.0666& 0.0685& 0.0719 & - & - & - & - & - & - & - & - & - &
%     - & - & -  \\
%     \transpol & 0.0801& 0.0799& 0.0579& 0.0597& 0.0676& 0.0676 & - & - & - & - & - & - & - & - & - &
%     - & - & - & - & - & - &
%     - & - & -\\
%     \rpol & 0.0842& 0.0833& 0.0788& 0.0792& 0.0771& 0.0769& 0.0607& 0.0607&
%     0.0581& 0.0568& 0.0565& 0.0596& 0.0704& 0.0689& 0.0661& 0.0664& 0.0659&
%     0.0673 & - & - & - & - & - & - \\
%     \helcorr & 0.0762& 0.0814& 0.0793& 0.0779& 0.0787& 0.0828& 0.0891& 0.0986&
%     0.0546& 0.0590& 0.0589& 0.0565& 0.0582& 0.0598& 0.0716& 0.0660& 0.0654&
%     0.0664& 0.0677& 0.0672& 0.0672& 0.0681& 0.0726& 0.0741 \\
%     \transcorr & 0.0717& 0.0751& 0.0796& 0.0841& 0.0843& 0.0794& 0.0740&
%     0.0725& 0.0529& 0.0546& 0.0581& 0.0620& 0.0616& 0.0593& 0.0548& 0.0513&
%     0.0595& 0.0632& 0.0649& 0.0702& 0.0720& 0.0692& 0.0650& 0.0674 \\
%     \rcorr & 0.0804& 0.0807& 0.0783& 0.0791& 0.0802& 0.0813& 0.0847& 0.0786&
%     0.0564& 0.0649& 0.060& 0.0582& 0.0564& 0.0566& 0.0649& 0.0664& 0.0743& 0.0686&
%     0.0682& 0.0666& 0.0663& 0.0676& 0.0719& 0.0731 \\
%     \transhelsum &0.0826& 0.0809& 0.0786& 0.0807& 0.0777& 0.0814& 0.0590&
%     0.0597& 0.0578& 0.0588& 0.0588& 0.0590& 0.0656& 0.0674& 0.0686& 0.0688&
%     0.0666& 0.0672 & - & - & - & - & - & - \\
%     \transheldiff &0.0772& 0.0817& 0.0792& 0.0817& 0.0804& 0.0791& 0.0592&
%     0.0593& 0.0571& 0.0607& 0.0591& 0.0568& 0.0672& 0.0659& 0.0673& 0.0700&
%     0.0678& 0.0670 & - & - & - & - & - & - \\
%     \transrsum &0.0790& 0.0790& 0.0811& 0.0795& 0.0804& 0.0808& 0.0572&
%     0.0601& 0.0598& 0.0608& 0.0581& 0.0551& 0.0682& 0.0679& 0.0672& 0.0691&
%     0.0666& 0.0664 & - & - & - & - & - & - \\
%     \transrdiff &0.0798& 0.0790& 0.0796& 0.0825& 0.0789& 0.0798& 0.0569& 0.0613&
%     0.0582& 0.0584& 0.0586& 0.0589& 0.0676& 0.0677& 0.0668& 0.0687& 0.0685&
%     0.0661 & - & - & - & - & - & - \\
%     \rhelsum &0.0909& 0.0847& 0.0783& 0.0777& 0.0788& 0.0770& 0.0674& 0.0614&
%     0.0564& 0.0575& 0.0583& 0.0585& 0.0739& 0.0688& 0.0673& 0.0665& 0.0666&
%     0.0672 & - & - & - & - & - & - \\
%     \rheldiff &0.0807& 0.0822& 0.0783& 0.0781& 0.0799& 0.0859& 0.0643& 0.0602&
%     0.0563& 0.0575& 0.06586& 0.0620& 0.0714& 0.0680& 0.0661& 0.0671& 0.0666&
%     0.0721 & - & - & - & - & - & - \\
%     \hline
%   \end{tabular}
%   \caption{Fraction of nonfiducial events compared to fiducial events
%   estimated from the signal MC sample for every bin of the
%   reconstructed distribution. As the observables have different
%   amounts of bins, non-existing bins are marked with a "-".}
% \label{tab:nonfid_fractions}
% \end{center}
%\end{table}
%\end{landscape}

\subsubsection{Binning choice}
Similar to the binning optimisation of the parton measurement, several
binning configurations are tested for each of the observables. The
reweighting of each observable is performed using
Eqn.~\ref{eqn:rew_pol} for the polarizations and spin correlations and
a linear reweighting for the cross correlations. The truth observables
used for the reweighting are calculated using the pseudo-top
information, not the parton tops. The difference of the reweighting procedure using
parton top information is documented in App.~\ref{app:fid_calib}. The
different binnings can be found in App.~\ref{app:moreplots}. As the
resolutions and response matrices for both parton and stable-particle
measurements are quite similar, i.e. they are dominated by detector
effects and not the top-quark definition itself, the binning choices
are also similar to the stable particle measurement, which is why the
same binning is taken for both measurements.

%\begin{table}
%  \begin{center}
 %   \begin{tabular}{c|c|l}
  %    observable & binning name & binning \\ \hline
 %     \helpol & 4Bin4 & [-1.,-0.4,0.0,0.5,1.]\\
 %     \transpol & 2Bin & [-1.,0.,1.]\\
 %     \rpol & 6Bin2 & [-1.,-0.6,-0.3,0.,0.3,0.6,1.] \\
 %     \helcorr & 8Bin3 & [-1.,-0.7,-0.45,-0.2,0.,0.2,0.45,0.7,1.]
 %     \\
 %     \transcorr & 8Bin & [-1.,-0.7,-0.45,-0.2,0.,0.2,0.45,0.7,1.] \\
 %     \rcorr & 8Bin & [-1.,-0.7,-0.45,-0.2,0.,0.2,0.45,0.7,1.] \\
 %     \transhelsum & 6Bin3 & [-1.,-0.65,-0.325,0.,0.325,0.65,1.] \\
 %     \transheldiff & 6Bin & [-1.,-0.6,-0.3,0.,0.3,0.6,1.]\\
 %     \transrsum & 6Bin &[-1.,-0.6,-0.3,0.,0.3,0.6,1.]\\
 %     \transrdiff & 6Bin &[-1.,-0.6,-0.3,0.,0.3,0.6,1.] \\
 %     \rhelsum & 6Bin4 &[-1.,-0.65,-0.3,0.,0.3,0.65,1.] \\
 %     \rheldiff & 6Bin4 &[-1.,-0.65,-0.3,0.,0.3,0.65,1.]  \\
 %   \end{tabular}
 %   \caption{Chosen binning configurations for the stable-particle measurement.}
 %   \label{tab:chosen_binnings_particle}
%  \end{center}
%\end{table}

