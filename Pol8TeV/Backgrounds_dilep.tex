\subsubsection{\ZX\ Background Estimation}
\label{sec:backgrounds_dilep_dy}

After the event selection, a disagreement between data and Monte Carlo
is observed in the Z mass window (Fig~\ref{fig:zjetsnsf1}). A first
contribution to this disagreement comes from missing transverse energy ($E_\text{T}^{\text{miss}}$)
mis-modelling, where leptons and jets that are poorly measured
contribute to the $E_\text{T}^{\text{miss}}$. A second contribution comes from the poor ability of the Monte
Carlo generator to predict the heavy flavour content of the vector boson
production in association with jets. This last contribution is enhanced when $b$-tagging is required. The effect of both contributions
propagates to our signal region at high \met (see Fig.~\ref{fig:zjets2}), thus it is necessary to apply a
correction factor by normalizing the Monte Carlo predictions to data. 

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[scale=0.3]{figures/zjets/ee_invmass_v_signal_zinv_nsf_central.eps}
   \includegraphics[scale=0.3]{figures/zjets/mumu_invmass_v_signal_zinv_nsf_central.eps} 
   \includegraphics[scale=0.3]{figures/zjets/ee_invmass_v_btag_zinv_nsf_central.eps}
   \includegraphics[scale=0.3]{figures/zjets/mumu_invmass_v_btag_zinv_nsf_central.eps}
   \caption{Dilepton invariant mass in the pretag region for $ee$
     (top-left) and $\mu\mu$ (top-right) and in the b-tag region for
     $ee$ (bottom-left) and $\mu\mu$ (bottom-right) before applying
     the scale factors. The data/MC ratio is also shown. The shaded area shows the uncertainty on the signal and background
     normalization. } 
   \label{fig:zjetsnsf1}
 \end{center}
\end{figure}

The correction factor for $E_\text{T}^{\text{miss}}$ mis-modelling has to be
separately applied to the $ee$ and $\mu\mu$ channels since electrons
and muons have a different impact in this mis-modelling. On the other
hand, $Z \rightarrow \tau\tau$ events are excluded in this correction
due to the fact that there are neutrinos in this final state
contributing to the $E_\text{T}^{\text{miss}}$ itself. The normalization correction
for heavy flavour processes are applied on top of the previously
defined scale factor. The different processes are solely distinguished
by their DSID.

The calculation has been performed inside the $Z$ mass window
requiring all other selection cuts in the following control regions:

\begin{itemize}
  \item $ee$ channel, no $b$-tagging requirement
  \item $\mu\mu$ channel, no $b$-tagging requirement
  \item Both channels together, at least one $b$-tagged jet
\end{itemize}

All the previously described constraints translate into the following system of equations:

\begin{footnotesize}
\[
\begin{cases}
{\text{SF}}_{ee} \cdot (N_{Z+\text{light}}^{ee,\text{pretag}} +
{\text{SF}}_{\text{HF}} \cdot N_{Z+\text{HF}}^{ee,\text{pretag}}) +
{\text{SF}}_{\text{HF}} \cdot
N_{Z\rightarrow\tau\tau+\text{HF}}^{ee,\text{pretag}} +N_{Z\rightarrow
  \tau\tau}^{ee,\text{pretag}} + N_{\text{others}}^{ee,\text{pretag}}
= N_{\text{data}}^{ee,\text{pretag}} \\
\\
{\text{SF}}_{\mu\mu} \cdot (N_{Z+\text{light}}^{\mu\mu,\text{pretag}}
+ {\text{SF}}_{\text{HF}} \cdot
N_{Z+\text{HF}}^{\mu\mu,\text{pretag}}) + {\text{SF}}_{\text{HF}}
\cdot N_{Z\rightarrow\tau\tau+\text{HF}}^{\mu\mu,\text{pretag}}
+N_{Z\rightarrow \tau\tau}^{\mu\mu,\text{pretag}} +
N_{\text{others}}^{\mu\mu,\text{pretag}} =
N_{\text{data}}^{\mu\mu,\text{pretag}} \\
\\
{\text{SF}}_{ee} \cdot (N_{Z+\text{light}}^{ee,\text{btag}} +
{\text{SF}}_{\text{HF}} \cdot N_{Z+\text{HF}}^{ee,\text{btag}}) +
 {\text{SF}}_{\mu\mu} \cdot (N_{Z+\text{light}}^{\mu\mu,\text{btag}} +
 {\text{SF}}_{\text{HF}} \cdot N_{Z+\text{HF}}^{\mu\mu,\text{btag}})\:+
 \\
{\text{SF}}_{\text{HF}} \cdot N_{Z\rightarrow\tau\tau+\text{HF}}^{\text{all},\text{btag}} +N_{Z\rightarrow \tau\tau}^{\text{all},\text{btag}} + N_{\text{others}}^{\text{all},\text{btag}} = N_{\text{data}}^{\text{all},\text{btag}}
\end{cases}
\]
\end{footnotesize}

with

\begin{itemize}
\item ${\text{SF}}_{ee}$ is the scale factor applied to $Z\rightarrow ee$ events.
\item ${\text{SF}}_{\mu\mu}$ is the scale factor applied to $Z\rightarrow \mu\mu$ events.
\item ${\text{SF}}_{\text{HF}}$ is the scale factor applied to $Z \rightarrow X + \text{HF}$ events.
\item $N_{Z+X}$ is the number of events for the $Z+X$ processes.
\item $N_{\text{data}}$ is number of observed events.
\item $N_{\text{others}}$ is the number of events of all the other processes.
\end{itemize}
The event yields for the different processes in the control region can
be found in \cref{tab:z_bkg_sf}.
The system of equations is solved numerically  minimizing the
log-likelihood of the system 
\begin{equation}
  N_{ee,\text{pred.}}+ N_{\mu\mu,\text{pred.}} +
  N_{\text{all},\text{pred.}} -
 N_{\text{obs},ee}\log{N_{ee,\text{pred.}}}
 -N_{\text{obs},\mu\mu}\log{N_{\mu\mu,\text{pred.}}} -
 N_{\text{obs},all}\log{N_{all,\text{pred.}}},
\end{equation}
where $N_{\text{pred}}$ describe predictions of each channel, i.e. the left-hand-sides of the system of
equation and $N_{\text{obs.}}$ the measured data, i.e. the right-hand-sides.

The result of this study shows that the
scale factor that corrects for the \met mis-modelling is $0.927 \pm
0.006$ for the $ee$ channel and $0.890 \pm 0.005$ for the
$\mu\mu$ channel, while the normalization factor for heavy flavour
processes is $1.71 \pm 0.03$. In Fig~\ref{fig:zjets1} the $Z$
mass windows after applying the derived scale factors is shown. The
scale factors are re-computed for Z-windows which are 10 (5) GeV
larger (smaller) than the nominal definition. Combining the change in
the scale factors with the statistical uncertainty gives a total
uncertainty of approximately 5\%, which is later used as normalisation
uncertainty of the Drell-Yan process in the $ee$ and $\mu\mu$
channels. The scale factors are later applied in the signal region to
the corresponding processes, which are again distinguished by their DSID.


\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[scale=0.3]{figures/zjets/ee_invmass_v_signal_zinv_central.eps}
   \includegraphics[scale=0.3]{figures/zjets/mumu_invmass_v_signal_zinv_central.eps} 
   \includegraphics[scale=0.3]{figures/zjets/ee_invmass_v_btag_zinv_central.eps}
   \includegraphics[scale=0.3]{figures/zjets/mumu_invmass_v_btag_zinv_central.eps}  
   \caption{Dilepton invariant mass in the pretag region for $ee$ (top-left) and $\mu\mu$ (top-right) and in the b-tag region for $ee$ (bottom-left) and $\mu\mu$ (bottom-right) after applying the derived scale factors. The data/MC ratio is also shown. The shaded area shows the uncertainty on the signal and background
     normalization. } 
   \label{fig:zjets1}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[scale=0.3]{figures/zjets/nosf/ee_etmiss_pretag_zinv_central.pdf}
   \includegraphics[scale=0.3]{figures/zjets/nosf/mumu_etmiss_pretag_zinv_central.pdf} 
   \includegraphics[scale=0.3]{figures/zjets/nosf/ee_etmiss_btag_zinv_central.pdf}
   \includegraphics[scale=0.3]{figures/zjets/nosf/mumu_etmiss_btag_zinv_central.pdf}
   \includegraphics[scale=0.3]{figures/zjets/ee_etmiss_pretag_zinv_central.pdf}
   \includegraphics[scale=0.3]{figures/zjets/mumu_etmiss_pretag_zinv_central.pdf} 
   \includegraphics[scale=0.3]{figures/zjets/ee_etmiss_btag_zinv_central.pdf}
   \includegraphics[scale=0.3]{figures/zjets/mumu_etmiss_btag_zinv_central.pdf}
   \caption{\met in the pretag region for $ee$
     (left) and $\mu\mu$ (right) (1st and 3rd rows) ) and in the b-tag region for
     $ee$ and $\mu\mu$ (2nd and 4th rows) before (1st and 2nd rows)
     and after (3rd and 4th rows) applying
     the scale factors. The data/MC ratio is also shown. The shaded area shows the uncertainty on the signal and background
     normalization. } 
   \label{fig:zjets2}
 \end{center}
\end{figure}


\begin{table}
  \begin{center}
    \begin{tabular}{c|c|c|c|c}
 %     \hline
     & \eeme pretag & \mumume pretag & \eeme btag & \mumume btag \\
      \hline
   $N_{Z+\text{light}}$   & $58956 \pm 208$ & $76501 \pm 248$ & $1724
   \ pm 41$ & $2286 \pm 56$\\
   $N_{Z+\text{HF}}$   & $12254 \pm 56$ & $15286 \pm 64$ & $3779 \pm 26$  & $4702 \pm 31$\\
   $N_{Z\rightarrow\tau\tau}$  & $11 \pm 3$ & $6 \pm 1$ & $1 \pm 0$ & $0 \pm 0$\\
   $N_{Z\rightarrow\tau\tau+\text{HF}}$  & $2 \pm 1$ & $3 \pm 1$ & $1 \pm 0$ & $0 \pm 0$\\
   $N_{\text{others}}$   & $3896 \pm 19$ & $4430 \pm 17$ & $2154 \pm
   11$ & $2516 \pm 13$\\
   $N_{\text{total}}$   & $75119$ & $96226$ &
   \multicolumn{2}{c}{$17163 \pm 82$}  \\
   $N_{\text{data}}$   & $77921 \pm 279$ & $95691 \pm 308$ &
  \multicolumn{2}{c}{$21379 \pm $ 146}  \\
%      \hline
      \end{tabular}
      \caption{Yields obtained for the \ZX background, other
        background and data in the pretag and btag control region. These yields are
        used to estimate the different scale factors.}
   \label{tab:z_bkg_sf}
    \end{center}
\end{table}

\subsubsection{Fake Background Estimation}
\label{sec:backgrounds_dilep_fakes}
%issues: 
%  -move to backup 

The dileptonic \ttbar\ decays are identified by high \pT\ leptons in the final state. 
%These leptons are required to have fired the trigger and passed the event selection. 
While the electroweak processes can produce the events containing the
real leptons which pass these event selections, 
there remains an additional component
from events with mis-identified or non-isolated leptons.  The dominant
sources of these fake leptons are semi-leptonic $b$ decays,  
long lived weakly decaying states (such as $\pi^{\pm}$ or $K$ mesons),
reconstruction of a $\pi^{0}$ shower as an 
electron, direct photons or reconstruction of electrons from photon
conversions. All these sources of leptons 
will be called {\it fake} leptons in the following.

We use the electrons identified by
a multi-variate likelihood (``likelihood electrons''). The top fakes
group provides the efficiencies and fake rates only for cut-based
electrons. The standard data-based matrix method (MM) used for the
 fakes estimation can in principle also be used for the likelihood electrons. 
However, for the fakes estimate based on MM, one has to define ``loose
electrons'' with some relaxed criteria. It's not easy to define such
criteria for the likelihood electrons since these are already quite loose
with a high efficiency of reconstruction ($\sim 90\%$).
This and the fact that the contribution of the fakes is
expected to be small ($\sim 1\%$) in the selected signal region, we
decided to use a MC based fakes estimate (a similar procedure was performed in
the $t\bar{t}$ spin correlations measurement using the $\Delta \Phi$ variable~\cite{SpinCorrNote}). We  
cross-check our estimate of the fakes with the MM-based estimate in
a channel where it is possible ($\mu\mu$ channel).

%procedure, selection criteria and samples used
%single-top s-channel has ~15x smaller x-section comparing to t-channel
%dibosons and single top W+t contribute each < 1% of fakes
We consider $W/Z + $ jets, $W + \gamma + $ jets, single-top t-channel, 
$t\bar{t}$ and also $t\bar{t} + W/Z$ as potential sources of fake
leptons. The samples used are listed in 
the Appendix~\ref{app:mcsamples}.  
We veto the events which have both leptons matched to truth leptons for
the processes where we expect two prompt signal leptons (e.g. $Z +$ jets).

%control regions and scale factor derivation
%\subsubsubsection{Control regions and scale factors}
%We check our model of the fakes in a control region close to the
%signal region. We require
%the presence of two charged leptons of the same electric charge
%(same-sign leptons) for all dilepton channels (as
%opposite to the signal region requiring opposite sign leptons).
%In the first control region (``pretag'' region) we apply the same criteria
%as in the signal region for the $e\mu$ channel, but with a looser
%requirement on b-tagged jets ($N_{b-tag} \ge
%0$). For the $ee$
%and \mumume channels, we apply the same criteria as to our signal
%region in the 7 TeV analysis ($\met > 60\,\rm{GeV}$, $N_{b-tag} \ge 0$). 
%In the control region (``b-tag'' region), we apply the criteria
%as for our signal region in $ee$ and $\mu\mu$ channels ($\met
%>30\,\rm{GeV}$, $N_{b-tag} \ge 1$).  In the $e\mu$ channel, we
%apply the same criteria as in the signal region ($N_{b-tag} \ge 1$).
%We use these regions also to derive the scale-factors which are then  
%applied in the signal regions.   
A control region close to the signal region is defined to check the
performance of the MC-based fake estimation and to derive scale
factors to compensate for differences in the normalisation. The control
region consists of the same cuts as the signal region, except for
requiring to leptons of the same electric charge (same-sign leptons)
as opposed to the signal region. The motivation is, that mostly
processes producing two oppositely charged leptons should survive all
the kinematic cuts, but barely any process with two same-sign leptons
(e.g. coming from diboson processes). I.e., most of the events in the
same-sign control region should originate from processes having fake
leptons and therefore serve as a good control region.


%\begin{table}[htbp]
%  \begin{center}
%    \caption{The yields in the pretag fake control region. 
%    Only statistical uncertainties are only shown.} 
%    \label{tab:yields_CR_fakes_pretag}
%    \input{tables/yields_CR_fakes_pretag}
%  \end{center}
%\end{table}

\begin{table}[htbp]
  \begin{center}
    \caption{The yields in the fake control region.
    Only statistical uncertainties are only shown.} 
    \label{tab:yields_CR_fakes_btag}
    \input{tables/yields_CR_fakes_btag}
  \end{center}
\end{table}

The yields for the control region is shown  
in Tab.~\ref{tab:yields_CR_fakes_btag}. 
The scale factors related to $Z + $ jets which are described in
Sec.~\ref{sec:backgrounds_dilep_dy} are already applied here
(although their effect is very small).  
The agreement in the control regions is reasonable for the $ee$ and $e\mu$
channels while for the $\mu\mu$ channel there is an underestimation
of the yields for MC. 
The scale factors for the fakes are derived for each dilepton
channel separately and are also shown in Tab.~\ref{tab:yields_CR_fakes_btag}. They are
calculated as 
$$
SF(\text{fakes}) = \frac{N(\text{data}) - \sum_{i= \text{all MC - fakes}}N(MC_i)}{N(MC_{\text{fakes}})},
$$ 
i.e. the ratio of the measured data subtracted by all the MC not
originating from fakes and the contribution coming from fakes.

%estimate in signal regions
The yields for the fakes in the signal regions after applying the SF
corrections derived in the control regions are shown in
Tab.~\ref{tab:yields_SR_fakes}.    

\begin{table}[htbp]
  \begin{center}
    \caption{The expected number of events for
      the signal region after applying the corrections with only
      statistical uncertainties. These yields are used in the measurement.} 
    \label{tab:yields_SR_fakes}
    \input{tables/yields_SR_fakes}
  \end{center}
\end{table}

The comparison between data and the prediction in the same-sign control region
after applying the fakes scale factors $SF \text{(fakes)}$ is shown in
Fig.~\ref{fig:CR_fakes_dil}. 

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[scale=0.35]{figures/fakes/ee_jet_n_SSCR_btag_central.eps} 
   \includegraphics[scale=0.35]{figures/fakes/mumu_jet_n_SSCR_btag_central.eps} 
   \includegraphics[scale=0.35]{figures/fakes/emu_jet_n_SSCR_btag_central.eps} 
   \caption{The jet multiplicity in the same-sign control region in the
   $ee$ (top left), $\mumu$ (top right), and $e\mu$ (bottom) channel
   after applying the corresponding fakes scale factors. 
   The grey  area in the ratio plot corresponds to the uncertainty on the
   prediction where as systematic uncertainty we consider only the
   uncertainty on the theoretical cross-sections of all processes.}  
   \label{fig:CR_fakes_dil}
 \end{center}
\end{figure}


More detailed studies and also the comparison with the data-based MM
method estimate in $\mu\mu$ channel are presented in
app.~\ref{sec:FakesEstimate}.

The systematic uncertainties in the normalization of the fake contribution are
estimated by changing the MC generator for each individual background
and propagating the change of the scale factors into the final expected number of fake events
in the signal region. 

Two different sources of the systematic uncertainty are considered:
\begin{itemize}
\item the limited statistic of data and MC in the same-sign control region which
translates into an uncertainty in the scale factors $SF(\text{fakes})$. This turns out
to be $27 \%$ and $21 \%$ for $ee$ and $\mu\mu$ channel),
respectively. In the $e\mu$ channel, this is $12 \%$.
\item the difference between various MC generators translates in
the change of both the number of expected events for various processes in
the control region (and consequently in the change of $SF(\text{fakes})$) and
also the number of fakes events in the signal region. This analysis is concentrated on the processes with the largest contributions,
i.e. $t\bar{t}$,  dibosons, $Z$ + jets in the CR and $t\bar{t}$, $W$ +
jets and  $W$ + gamma in the 
SR. The contribution of each process is shown in
tab.~\ref{tab:syst_fakes}.
%which generators???
%add samples to the note???
\begin{table}[htbp]
  \begin{center}
    \caption{The uncertainties in the normalization of the
    fakes estimates due to changes in MC generators for various processes.} 
    \label{tab:syst_fakes}
    \input{tables/syst_fakes}
  \end{center}
\end{table}

\end{itemize}

For the $\mu\mu$ channel, the difference in the normalization  
of the fakes estimate by the MM method and MC method is checked
leading to a difference
of $77 \%$. Since this  difference is larger than the
systematic uncertainty obtained by the change of MC generators, it is possible to use it conservatively as an estimate of systematic uncertainty for the
$\mu\mu$ channel. Adding all the numbers in quadrature yields the total systematic
uncertainty on the normalization of fake events:  $167 \%$ in the $ee$
channel, $77 \%$ in the $\mu\mu$ channel and $49 \% $ in the $e\mu$
channel. 

 
