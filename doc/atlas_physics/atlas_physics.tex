%-------------------------------------------------------------------------------
% This note lists the symbols defined in atlasphysics.sty.
%-------------------------------------------------------------------------------
\documentclass[koma,UKenglish]{latex/atlasdoc}

\usepackage[default,biblatex]{latex/atlaspackage}
\usepackage{xtab}
%\usepackage{multicol}
\usepackage{authblk}
% Include all style files
\usepackage[BSM,journal,math,process,other,xref]{latex/atlasphysics}

\addbibresource{atlas_physics.bib}

\graphicspath{{../../logos/}}

\newcommand{\File}[1]{\texttt{#1}\xspace}
\newcommand{\Macro}[1]{\texttt{\textbackslash #1}\xspace}
\newcommand{\Option}[1]{\textsf{#1}\xspace}
\newcommand{\Package}[1]{\texttt{#1}\xspace}

%-------------------------------------------------------------------------------
% Generic document information
%-------------------------------------------------------------------------------

% Set author and title for the PDF file
\hypersetup{pdftitle={ATLAS LaTeX guide},pdfauthor={Ian Brock}}

\AtlasTitle{Symbols defined in \File{atlasphysics.sty}}

\author{Ian C. Brock}
\affil{University of Bonn}

% \AtlasVersion{\ATPackageVersion}

\AtlasAbstract{%
  This note lists the symbols defined in \File{atlasphysics.sty}.
  These provide examples of how to define your own symbols, as well as many symbols
  that are often used in ATLAS documents.

  This document was generated using version \ATPackageVersion\ of the ATLAS \LaTeX\ package.
}

%-------------------------------------------------------------------------------
% This is where the document really begins
%-------------------------------------------------------------------------------
\begin{document}

\tableofcontents

%-------------------------------------------------------------------------------
\section{\File{atlasphysics.sty} style file}
\label{sec:atlasphysics}
%-------------------------------------------------------------------------------

The \File{atlasphysics.sty} style file implements a series of useful
shortcuts to typeset a physics paper, such as particle
symbols.

Options are parsed with the \Package{kvoptions} package, which is included by default.
The style file can included in the preamble of your paper with the usual
syntax:
%
\begin{verbatim}
  \usepackage{latex/atlasphysics}
\end{verbatim}
%
As of version 01-00-00 the file is actually split into smaller files,
which can be included or not using options.
The following options are available, where the default setting is given in parentheses
\begin{description}
\item[bsm](false) BSM and SUSY particles;
\item[journal](true) Journal abbreviations and a few other definitions for references;
\item[math](false) A few extra maths definitions;
\item[misc](true) Miscellaneous definitions that are often used;
\item[other](off) Definitions that used to be in \File{atlasphysics.sty}, 
  but are probably too specialised to be needed by most people;
\item[particle](true) Standard Model particles and some combinations;
\item[process](false) Some example processes. 
  These are not included by default as the current choice is rather arbitrary
  and certainly not complete;
\item[unit](true) Units that used to be defined -- not needed if you use \Package{siunitx} or \Package{hepunits}.
\item[xref](true) Useful abbreviations for cross-references.
\end{description}
Note that \Option{bsm} and \Option{bsm=true} are equivalent.
Use the syntax \Option{option=false} to turn off an option.

There is an additional option \Option{texmf}.
If this option is included, the subfiles are included using the command:
\verb|\RequirePackage{atlaspartcle}| etc. instead of \verb|\RequirePackage{latex/atlaspartcle}|.
This is useful if you install the ATLAS \LaTeX\ package in a central directory such as \File{\$\{HOME\}/texmf/tex/latex}.

All definitions are done in a consistent way using \verb|\newcommand*|.
All definitions use \verb|\ensuremath| where appropriate and are terminated with
\verb|\xspace|, so you can simply write ``{\verb|\ttbar production|'' instead of
``\verb|\ttbar\ production|'' or ``\verb|\ttbar{} production|'' to get \enquote{\ttbar production}.

See Appendix~\ref{sec:old} for details on changes that were introduced when
when going from version 00-04-05 of \Package{atlasnote}
to version 01-00-00 of \Package{atlaslatex}.
Let me know if you spot some other changes that are not documented here!

The following sections list the macros defined in the various files.

\twocolumn
%-------------------------------------------------------------------------------
\section{\File{atlasparticle.sty}}

Turn on including these definitions with the option \Option{particle=on} and off with the option \Option{particle=false}.

\input{atlasparticle}

A generic macro \verb|\Ups[1]| is available.
It is defined such that \verb|\Ups{3}| produces \Ups{3}.


\newpage
%-------------------------------------------------------------------------------
\section{\File{atlasjournal.sty}}

Turn on including these definitions with the option \Option{journal=true} and off with the option \Option{journal=false}.

\input{atlasjournal}


\newpage
%-------------------------------------------------------------------------------
\section{\File{atlasmisc.sty}}

Turn on including these definitions with the option \Option{misc=true} and off with the option \Option{misc=false}.

\input{atlasmisc}

\noindent A generic macro \verb|\twomass| is defined, so that for example
\verb|\twomass{\mu}{\mu}| produces \twomass{\mu}{\mu} and \verb|\twomass{\mu}{e}| produces \twomass{\mu}{e}.

A macro \verb|\dk| is also defined which makes it easier to write down decay chains.
For example
\begin{verbatim}
\[\eqalign{a \to & b+c\\
   & \dk & e+f \\
   && \dk g+h}
\]
\end{verbatim}
produces
\[\eqalign{a \to & b+c\cr
   & \dk & e+f \cr
   && \dk g+h}
\]
Note that \Macro{eqalign} is also redefined in this package so that \Macro{dk} works.


\newpage
%-------------------------------------------------------------------------------
\section{\File{atlasxref.sty}}

Turn on including these definitions with the option \Option{xref=true} and off with the option \Option{xref=false}.

\input{atlasxref}


\newpage
%-------------------------------------------------------------------------------
\section{\File{atlasbsm.sty}}

Turn on including these definitions with the option \Option{bsm} and off with the option \Option{nobsm}.

The macro \Macro{susy} simply puts a tilde ($\tilde{\ }$) over its argument,
e.g.\ \verb|\susy{q}| produces \susy{q}.

For \susy{q}, \susy{t}, \susy{b}, \slepton, \sel, \smu and
\stau, L and R states are defined; for stop, sbottom and stau also the
light (1) and heavy (2) states.
There are four neutralinos and two charginos defined, 
the index number unfortunately needs to be written out completely. 
For the charginos the last letter(s) indicate(s) the charge: 
\enquote{p} for $+$, \enquote{m} for $-$, and \enquote{pm} for $\pm$.

\input{atlasbsm}


\newpage
%-------------------------------------------------------------------------------
\section{\File{atlasmath.sty}}

Turn on including these definitions with the option \Option{math} and off with the option \Option{nomath}.

\input{atlasmath}

\noindent The macro \Macro{spinor} is also defined.
\verb|\spinor{u}| produces \spinor{u}.


\newpage
%-------------------------------------------------------------------------------
\section{\File{atlasother.sty}}

Turn on including these definitions with the option \Option{other} and off with the option \Option{noother}.

\input{atlasother}


\newpage
%-------------------------------------------------------------------------------
\section{\File{atlasprocess.sty}}

Turn on including these definitions with the option \Option{process} and off with the option \Option{noprocess}.

\input{atlasprocess}


\newpage
%-------------------------------------------------------------------------------
\section{\File{atlasunit.sty}}

Turn on including these definitions with the option \Option{unit} and off with the option \Option{nounit}.

\input{atlasunit}

\noindent Lower case versions of the units also exist, e.g.\ \verb|\tev|, \verb|\gev|, \verb|\mev|, \verb|\kev|, and
\verb|\ev|. 

As mentioned above, it is highly recommended to use a units package instead of
these definitions. \Package{siunitx} is the preferred package; a good alternative is \Package{hepunits}.
If either of these packages are used \File{atlasunit.sty} is not needed.


\onecolumn
%-------------------------------------------------------------------------------
\section{Old macros}
\label{sec:old}
%-------------------------------------------------------------------------------

With the introduction of \Package{atlaslatex} several macro names have been changed to make them more consistent.
A few have been removed. The changes include:
\begin{itemize}
\item Kaons now have a capital ``K'' in the macro name, e.g.\ \verb|\Kplus| for \Kplus;
\item \verb|\Ztau|, \verb|\Wtau|, \verb|\Htau| \verb|\Atau| have been replaced by
  \verb|\Ztautau|, \verb|\Wtautau|, \verb|\Htautau| \verb|{Atautau|;
\item \verb|\Ups| replaces \verb|\ups|;
  the use of \verb|\ups| to produce $\Upsilon$ in text mode has been removed;
\item \verb|\cm| has been removed, as it was the only length unit defined for text and math mode;
\item \verb|\mass| has been removed, as \verb|\twomass| can do the same thing and the name is more intuitive;
\item \verb|\mA| has been removed as it conflicts with \Package{siunitx} Version 1, which uses the name
  for milliamp.
\item \Macro{mathcal} rather than \Macro{mathscr} is recommended for luminosity and aplanarity.
\end{itemize}

Quite a few macros are more related to \Zboson physics than they are to LHC physics and have
been moved to the \File{atlasother.sty} file, which is not included by default.
There are also macros for various decay processes, \File{atlasprocess.sty} which are not included by default,
but may be useful for how you can define your favourite process.

It used to be the case that you had to use \verb|\met{}| rather than just \verb|\met| to get the spacing right,
as somehow \Package{xspace} dis not do a good job for \met.
However, with the latest version of the packages both forms work fine.
You can compare \MET and \MET\ and see that the spacing is correct in both cases.

\end{document}
