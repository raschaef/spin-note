\select@language {UKenglish}
\contentsline {section}{\numberline {1}To-do list}{4}{section.1}
\contentsline {section}{\numberline {2}Introduction}{5}{section.2}
\contentsline {section}{\numberline {3}Observables}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Spin density matrix and the set of observables}{6}{subsection.3.1}
\contentsline {section}{\numberline {4}Monte Carlo}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}Parton level}{9}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Stable-particle level}{12}{subsection.4.2}
\contentsline {section}{\numberline {5}Selection}{16}{section.5}
\contentsline {subsection}{\numberline {5.1}Object Selection}{16}{subsection.5.1}
\contentsline {paragraph}{Jets}{16}{section*.10}
\contentsline {paragraph}{Electrons}{16}{section*.11}
\contentsline {paragraph}{Muons}{16}{section*.12}
\contentsline {paragraph}{Missing Transverse Momentum}{16}{section*.13}
\contentsline {paragraph}{Trigger Requirements}{17}{section*.14}
\contentsline {paragraph}{Event Cleaning}{17}{section*.15}
\contentsline {subsection}{\numberline {5.2}Dilepton selection}{17}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}$Z+$jets\ Background Estimation}{18}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Fake Background Estimation}{21}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}Comparison of Data and Simulation}{22}{subsubsection.5.2.3}
\contentsline {section}{\numberline {6}Reconstruction}{26}{section.6}
\contentsline {subsection}{\numberline {6.1}The kinematic reconstruction method}{26}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Comparison of Data and Simulation}{27}{subsection.6.2}
\contentsline {section}{\numberline {7}Unfolding}{31}{section.7}
\contentsline {subsection}{\numberline {7.1}FBU}{31}{subsection.7.1}
\contentsline {subsubsection}{\numberline {7.1.1}FBU Ingredients}{31}{subsubsection.7.1.1}
\contentsline {subsubsection}{\numberline {7.1.2}Prior Choice}{31}{subsubsection.7.1.2}
\contentsline {subsubsection}{\numberline {7.1.3}Marginalization}{32}{subsubsection.7.1.3}
\contentsline {subsection}{\numberline {7.2}Parton level unfolding}{32}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}Binning choice}{34}{subsubsection.7.2.1}
\contentsline {subsection}{\numberline {7.3}Stable-particle level unfolding}{35}{subsection.7.3}
\contentsline {subsubsection}{\numberline {7.3.1}Non-fiducial background}{36}{subsubsection.7.3.1}
\contentsline {subsubsection}{\numberline {7.3.2}Binning choice}{38}{subsubsection.7.3.2}
\contentsline {section}{\numberline {8}Systematics}{39}{section.8}
\contentsline {subsection}{\numberline {8.1}Procedure used to assess systematic uncertainties}{39}{subsection.8.1}
\contentsline {subsubsection}{\numberline {8.1.1}Signal Modelling Uncertainties}{39}{subsubsection.8.1.1}
\contentsline {subsubsection}{\numberline {8.1.2}Detector Modelling Uncertainties}{39}{subsubsection.8.1.2}
\contentsline {subsection}{\numberline {8.2}List of systematics considered}{40}{subsection.8.2}
\contentsline {subsubsection}{\numberline {8.2.1}\ensuremath {t\bar {t}}\xspace \ modelling systematics}{40}{subsubsection.8.2.1}
\contentsline {subsubsection}{\numberline {8.2.2}Background uncertainties}{41}{subsubsection.8.2.2}
\contentsline {subsubsection}{\numberline {8.2.3}Detector modelling systematics}{41}{subsubsection.8.2.3}
\contentsline {section}{\numberline {9}Results}{45}{section.9}
\contentsline {subsection}{\numberline {9.1}Parton measurement}{45}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Stable-particle measurement}{47}{subsection.9.2}
\contentsline {section}{\numberline {10}Conclusion}{53}{section.10}
\contentsline {section}{\numberline {A}Response matrix comparisons}{54}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Charge comparison for polarization observables}{54}{subsection.A.1}
\contentsline {subsection}{\numberline {A.2}Channel comparison for observables}{54}{subsection.A.2}
\contentsline {section}{\numberline {B}Monte Carlo samples}{57}{appendix.B}
\contentsline {section}{\numberline {C}Further plots and tables}{60}{appendix.C}
\contentsline {subsection}{\numberline {C.1}Resolutions}{60}{subsection.C.1}
\contentsline {subsubsection}{\numberline {C.1.1}Partonic}{60}{subsubsection.C.1.1}
\contentsline {subsubsection}{\numberline {C.1.2}Stable particle}{60}{subsubsection.C.1.2}
\contentsline {subsection}{\numberline {C.2}Binnings}{60}{subsection.C.2}
\contentsline {subsection}{\numberline {C.3}Response matrices}{72}{subsection.C.3}
\contentsline {subsubsection}{\numberline {C.3.1}Partonic}{72}{subsubsection.C.3.1}
\contentsline {subsubsection}{\numberline {C.3.2}Stable-Particle}{72}{subsubsection.C.3.2}
\contentsline {subsection}{\numberline {C.4}Acceptance efficiencies}{78}{subsection.C.4}
\contentsline {subsubsection}{\numberline {C.4.1}Partonic}{78}{subsubsection.C.4.1}
\contentsline {subsubsection}{\numberline {C.4.2}Stable-Particle}{78}{subsubsection.C.4.2}
\contentsline {subsection}{\numberline {C.5}Unfolded distributions}{78}{subsection.C.5}
\contentsline {subsubsection}{\numberline {C.5.1}Partonic}{78}{subsubsection.C.5.1}
\contentsline {section}{\numberline {D}Fakes estimation}{105}{appendix.D}
\contentsline {section}{\numberline {E}Systematic Tables}{116}{appendix.E}
\contentsline {section}{\numberline {F}Mean calibration}{136}{appendix.F}
