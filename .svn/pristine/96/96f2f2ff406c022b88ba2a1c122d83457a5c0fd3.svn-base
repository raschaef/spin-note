General:
* can you refer in your ChangeLog to the version of the note that you changed it for and give the note a new version number if you make a significant change? Then it \
will be easier for us to keep track of what changed where :)
---> Sure, it will be changed form now on.
* you are using quite often text in math mode (for example in line 630 "obs" or in table 11) but in general often across the note. Can you use \mathrm{obs} etc instea\
d to make the text more readable?

Abstract:
* are the uncertainties shown stat and syst? Why does the systematic uncertainty have less precision?
---> The uncertainties are (stat.+det.) from the marginalization and the second uncertainty comes from the modelling systematics. 
     The modelling systematics of 0.20 were cut to 0.2 in the Latex macro. So it's not really less precision, but I will change it.

* l 150: mus -> must
---> Changed
* caption for table1: what is the meaning of the "=" in the caption? I guess this is simply a typo, right?
---> Yes, that was typo.
* l 160: broken reference for equation
---> Fixed.
* Table 2 or text before: could you indicate which observables have not yet been measured at all?
###### Needs to be added.
* Chapter 4: which samples are used for the estimation of the systematic uncertainties? It might be useful to already list them here. Which ttbar sample did you use f\
or the signal, the one with hdamp or the one with DSID 117050?
---> DSID 110404 was used as nominal signal sample. The samples for the systematic uncertainties are listed in Appendix B, which has a reference in the text. I add more description to the appendix.
* line 205: typo in desireable
---> Sorry, you mean 'desirable'->'desireable'?
* line 213 following: this is only true for Pythia6, what did you use in case of samples showered with Herwig?
---> For samples showered with Herwig, status code 155 was used for the top quarks and status code 123 for the leptons.
* line 217: what do you mean exactly with "errors from the calculation"?
---> This was poorly phrased and referred to an older (internal) version of the predictions. Has been changed now to the uncertainties from changing the renormalization and factorization scale.
* could you indicate in the plots Figure 1,2 and following that this is parton level and Fig 4/5 following that its particle level?
###### Needs to be added.
* Fig. 1b and 4b: could you zoom out here a bit in the ratio plot to see how large the difference is in the first bin? are the uncertainties in these distributions
shown and very small or not shown? At least in the ratio plots that might be useful to see.
---> The uncertainties are not shown here, but will be added.
* line 228 and following: could you explain this generator dependence already in the MC chapter? That would make these first few lines a bit clearer, at the moment ev\
erything reads a bit vague.
###### Needs to be added.
* line 242 and following: I do not fully understand what you mean here with "clustered",  do you mean you are using "dressed" electrons, aka added the photons within \
a dR of 0.1?
---> Yes, this should mean we are using dressed electrons with a cone of dR = 0.1
* line 248: you mean the parent of the muon, right?
---> Yes, changed to muon.
* line 254: What do you mean here with "rescaled"?
###### Ask Roger...
* line 267: do you have a reference for the pseudo-top definitions?
---> Added the pseudo-top reference.
* line 273 and 279 following: could you fix the font for the subscripts (use roman font) so this is better readable?
---> Fixed.
* line 292: please mention that the JVF cut is only applied to jets with pt < 50 GeV and |eta| < 2.4.
---> Changed
* line 293: I think its not any jet, only the closest jet if it is within DeltaR < 0.2.
---> Changed to 'closest jet'
* line 310: calorimetry -> calorimeter
---> Changed.
* line 307 following: are you not using the mini-isolation criterion?
###### Check later
* page 18 (between lines 389 and 390): could you make sure that words like "pretag, others , all" etc are written in roman font, and maybe write scale factors in capi\
tal letters? That would make it easier to read this equation system.
---> Changed.
* could you indicate on the figures in 7,8,9 etc if they are for the signal region or a special control region?
###### Will check later
* line 467: space missing before "and fakes".
---> Changed.
* line 497: MT2 is not clear here, could you add one or two sentences about the method?
###### later
* figures 12,13,14: what does "full uncertainty" include? Is this stat only, stat+norm or also all systematics?
---> Full means 'Stat+Norm+Detector' in this case. Added it to the caption of the plots.
* line 550: consists OF
---> Changed.
* line 578: do you have a reference for the Tikhonov regularization functions?
###### Need to check a reference.
* figure 15 a/b: the binning labels look a bit arbitrary here, why do they go from -1-5 for (a) and from 0 to 24 for (b)? in (b) the number only describes the bin num\
ber right?
---> Yes, in figure (b) the label describes the bin number.
* formula 13: what is P_{rew} ?
---> P_{rew} is the polarization which is introduced by the reweighting (the same holds for C_{rew}). A description is added.

* line 689: reference is broken
---> Fixed
* line 719/20: phrasing is a bit confusing, maybe better: "The bin content of the nominal distribution is varied within poissonian uncertainties to build a pseudo-dat\
aset."
###### Will change that later, need to think about it.
* line 739: what do you mean with "different levels of ISR/FSR"? Are these alpha_S variation of 0.5 and 2.0?
---> Changed to the current recommendations according to the TopGroup.
* line 747 following: what is taken as final PDF uncertainty?
---> The envelope procedure is applied to obtain the final PDF uncertainty.
* line 801 following: do you take the eigenvectors provided?
###### Have to check for which uncertainty this comment was made... I guess JER?
* I do not see the systematics for the missing ET described anywhere, could you add them?
###### Will be added.
* do you have an uncertainty for your reconstruction method, and for the limited MC statistics?
---> The uncertainty for limited MC stats has been evaluated, but is considered negligible, will be added to the uncertainty tables.
     An uncertainty for the reconstruction method based on the smearing of the objects is discussed. A different random seed for the smearing of the objects can lead to a different result. As this usually averages out on MC due to the high statistics, it can have a larger effect on data.
* did you check the impact of the JES bug?
---> Not in a detailed way. There were no results yet for the stable-particle measurement and the parton-level results were quite "preliminary". I was just checked that the results didn't change too much compared to the overall JES uncertainty, which was the case.
* What systematic uncertainty do you apply for your fake contribution?
---> Only a normalization uncertainty coming from the change of input generators in the SF calculation.

Figure 20: can you add a red line at 0 to guide the eye? It would also help if this plot were bigger, the labels are quite hard to read
###### Will add it soon (made the plot, but haven't updated the note with it yet)

Figures 24a-d and following: there seems to be a larger normalisation issues for Pk and Pn which is not yet understood, right? I can see the uncertainty on the data, \
but how large are the stat+syst uncertainties on the MC?
---> Can add the statistical unceratinty on MC, systematics go into the unfolded distribution.    

Figures 21-23 and 27-29: I assume those are plots you would want to use in the paper, right? I would change the colour and/or the size of the Powheg+Py6 label to make\
 it better visible in the plot, at the moment its quite hard to see when partly overlapping with the red filled circle.
###### Will make it bigger and find another color.
p. 51-53: how do your results compare to previous results in case the coefficients have been measured before?
---> Will ad this information.

* Which systematic uncertainties have the largest effect in this analysis?
