\section{Systematics}
\label{sec:systs}
The recommendations from the Top Working Group are followed to estimate the systematic uncertainties~\cite{top_systematics}.  
This chapter explains the methods that were used to estimate them.

\subsection{Procedure used to assess systematic uncertainties}

Different methods are used to assess the systematic uncertainties depending on
their nature. First the method used for signal modelling uncertainties is described and then those coming from detector modelling uncertainties.

\subsubsection{Signal Modelling Uncertainties}
\label{subsubsec:MCsys}
Signal modelling systematics are obtained from Monte Carlo samples for which either specific parameters or the whole event generation has changed. Therefore, a different signal modelling affects both the truth distribution as well as the reconstructed one. To assess the effect of such an uncertainty on the result, calibration curves are built by reweighting the truth and recontructed distributions as discussed in section~\ref{sec:fbu} and unfolding the reconstructed distribution with the response matrix of the nominal \ttbar sample. In order to obtain the final uncertainty, the calibration curves of the nominal and signal modelling sample are used. A measured unfolded value of an observable is calculated back to the true observable value for both calibration curves and the difference of the true value is taken as systematic uncertainty.
%Signal modelling uncertainties have an impact on the \ttbar\ samples which are used to build the response matrix and efficiency correction used in the unfolding.
%To assess the effect of these uncertainties on the result, we build calibration curves using nominal inputs 
%(i.e. nominal \ttbar\ sample reweighted to have different levels of 
%polarization or spin correlation as explained in~\ref{sec:fbu}), and unfolding with the varied samples.
%The variations of the slope and offset of the calibration curves are then compared to the statistical uncertainty.


\subsubsection{Detector Modelling Uncertainties}
\label{subsubsec:Detsys}

Detector modelling systematic uncertainties affect only the reconstructed distributions. They are evaluated within the unfolding framework through the marginalization procedure. For each systematics source there is an associated nuisance parameter. %Only the systematics that are significant with respect to Monte Carlo statistics are considered in this analysis.

As the detector modelling systematic variations are performed on the same set of events as the nominal sample, the reconstructed distributions share a large fraction of the same events, only with a slightly changed reconstruction. Each bin of the distribution is thus highly correlated between the nominal and systematic variation. In order to obtain the real uncertainty of the variation and avoid statistical fluctuations, the bootstrap method is used. In this case, the bootstrap method first defines $n$ identical histograms, each representing a pseudo-dataset, for each observable. For every event, each histogram is filled with its corresponding Poisson fluctuation. This process is applied to the nominal sample and all of the systematic samples. The correlation between the variations and the nominal distribution is kept by taking a seed for the random number, which is depending on the event number. Events, that are common between the nominal and the systematics sample and which end up in the same bin, don't provide any new information as they have the same weight. The histogram is only varied by events that migrate between the bins or jump in/out of the acceptance due to the systematics variation. The relative difference of the observable is then calculated for a given systematic with respect to the nominal sample in each of the $n$ produced histograms and plotted in a variation histogram for all bins.  An example for one bin is shown in fig.~\ref{fig:bootstrap_bin_example} for the BJES uncertainty.
 Finally, the RMS of the relative differences is taken as the statistical uncertainty on that component. Figure~\ref{fig:helpol_boot_elidsf} shows the relative variation of the \costheta in the helicity axis for the electron identification scale factor systematic. This systematics does not affect the \mumume channel as there are no signal electrons which could be systematically varied. Such a behaviour allows to constrain the systematic in the marginalization procedure that is used in the unfolding method. However, this is only valid for significant systematic uncertainties.
%Figure \ref{fig:variations} shows an example for the b-tagging and the electron reconstruction systematic, which are significant according to the followed procedure . For the b-tagging systematics the $ee$ and $\mu\mu$ channel a big variation is observed with respect to the nominal $\Delta|y|$ distribution , while for $e\mu$ is not. This behaviour is expected due to the fact the the b-tagging requirement is not used in the $e\mu$ channel, making this systematic insensitive in that channel. A similar case is observed for the electron reconstruction systematic where the $\mu\mu$ is not sensitive while for the $ee$ and $e\mu$ the sensitivity is due to the electron that is present in the process. 

\begin{figure}
  \begin{center}
 \includegraphics[width=0.6\columnwidth]{unfolding/bootstrap/BJesUnc_down_ALL_bin9_histogram.eps}\\
  \end{center}
 \caption{Example of a bin variation done by the bootstrapping method for the down variation of the BJES uncertainty and the 9th bin of the \helpolplus observable.}  
\label{fig:bootstrap_bin_example}
\end{figure}


\begin{figure}
  \begin{center}
 \includegraphics[width=0.6\columnwidth]{unfolding/bootstrap/el_idSF.png}\\
  \end{center}
 \caption{Relative variations of electron identification SF with respect to the nominal \costheta distribution in the helicity axis.}  
\label{fig:helpol_boot_elidsf}
\end{figure}



\subsection{List of systematics considered}

\subsubsection{\ttbar\ modelling systematics}

\begin{itemize}
 
 \item {\it MC generator:} the uncertainty is obtained by comparing the \powheg~\cite{powhegbox} and \mcatnlo~\cite{mcatnlo} samples, both interfaced with \herwig~\cite{herwig}. The difference is symmetrized and taken as uncertainty.

 \item {\it Parton shower:} this effect is estimated by comparing \powheg interfaced either with \pythia or \herwig.

 \item {\it ISR/FSR:} two samples generated with \powheg + \pythia are compared, one with the renormalization and factorization scale $mu$ changed by a factor of 2 and the other changed having $mu$ changed by a factor of 0.5 and additionally changing the hdamp parameter to $2m_{\text{top}}$.

 %\item {\it Renormalization and factorization scale:} \mcatnlo samples interfaced with \herwig with simultaneous variation of the renormalization and factorization scales were used. The scales are varied to 0.5 and 2 times the default generator value.

 \item {\it Colour reconnection and underlying event:} samples were produced using \powheg with \pythia. The Perugia2011 tune with the respective NoCR and mpiHi variations are used to evaluate the systematics. 

 \item {\it Mass:} \powheg+\pythia samples were generated using different values for the top mass. 
       The observed differences in the results are scaled to a variation of 0.9 GeV, according to the uncertainty on the world average.

 \item {\it PDF:} The PDF uncertainties follow the top group recommendations~\cite{toppdf}, which are based on the PDF4LHC suggestions~\cite{lhapdf}. For the intra-PDF uncertainties, the results obtained with the 52 sets of CT10 were compared. 
       For the inter-PDF uncertainties, the results obtained with the central sets from CT10, MWST2008 and NNPDF23 are compared. In the end, the envelope of of inter- and intra-PDF uncertainties is taken according to PDF4LHC.

\end{itemize}


\subsubsection{Background uncertainties}

As explained previously, the backgrounds are in majority derived from Monte-Carlo simulation.

In that case, an uncertainty is taken into account for the normalization of these backgrounds, but the shape is not varied.
These uncertainties are 7\% for single top, 5\% for diboson, and 34\% for $Z \to \tau\tau$.

%For the $Z \to ee$ and $\mu\mu$ backgrounds for which we derived scale factors from data, we apply the overall normalization uncertainty that we assessed by varying the control region and background subtractions.
%This overall uncertainty is of 5\%.



\subsubsection{Detector modelling systematics}

The following detector modelling systematics are considered in this analysis following the ATLAS Top Working Group recommendations~\cite{top_object_selection}. In table~\ref{tab:notation} the notation used for each considered systematic is presented.

\begin{itemize}


\item{ \textbf{Lepton Reconstruction, Identification and Trigger}
The reconstruction and identification efficiency of electrons and muons, as well as the efficiency of the
triggers used to record the events, differ between data and simulation. Scale factors are derived using
tag-and-probe techniques on $Z \rightarrow \ell^{+} \ell^{-} (\ell = e \mu)$ data and simulated samples to correct the simulation for these discrepancies.}
\item{ \textbf{Lepton Momentum Scale and Resolution}.The accuracy of lepton momentum scale and resolution in simulation is checked using reconstructed
distributions of the  $Z \rightarrow \ell^{+} \ell^{-}$ $J/\Psi \rightarrow  \ell^{+} \ell^{-} $ masses. In the case of electrons, $E/p$ studies using $W \rightarrow e\nu$ events are also used. Small discrepancies are observed between data and simulation, and corrections for the lepton energy scale and resolution in the latter are implemented using the tools provided by the combined performance groups. In the case of muons, momentum scale and resolution corrections are only applied to the simulation. Uncertainties on both the momentum scale and resolutions in the muon
spectrometer and the tracking systems are considered, and varied separately.}
\item{ \textbf{Jet Reconstruction Efficiency}
The jet reconstruction efficiency is found to be about $0.2\%$ lower in the simulation than in data for jets
below $30$ GeV and it is consistent with data for higher jet $p_T$. To evaluate systematic uncertainty due to
 this small inefficiency $0.2\%$ of the jets with $p_T$ below $30$ GeV are removed randomly and all jet-related
kinematic variables (including the missing transverse energy) are recomputed. The event selection is
repeated using the modified selected jet list.}
\item{ \textbf{Jet Vertex Fraction Efficiency}
The per-jet efficiency to satisfy the jet vertex fraction requirement is measured in $Z \rightarrow \ell^{+} \ell^{-}+1-jet$ events
in data and simulation, selecting separately events enriched in hard-scatter jets and events enriched in
jets from other proton interactions in the same bunch crossing (pileup). The corresponding uncertainty
is evaluated in the analysis by changing the nominal JVF cut value to the up and down cut variation and
repeating the analysis using the modified cut value.}
\item{ \textbf{Jet Energy Scale}. The jet energy scale (JES) and its uncertainty have been derived combining information from test-beam
data, LHC collision data and simulation. The jet energy scale uncertainty is split
into 22 uncorrelated sources in the 8 TeV analysis which can have different jet $p_T$ and $\eta$ dependencies
and are treated independently in this analysis. The JetUncertainties tool allows computation of
uncertainties corresponding to each of the eigenvectors.}


\item{\textbf{Energy Resolution}. The jet energy resolution has been measured separately for data and simulation using two techniques. The expected fractional $p_T$ resolution for a given jet was measured using the $JERUncertaintyProvider$ tool as a function of its $p_T$ and rapidity. A systematic uncertainty is defined as the quadratic difference between the jet energy resolutions for data and simulation. To estimate the corresponding systematic uncertainty in the analysis, the energy of jets in the simulation is smeared by this residual difference, and the changes in the normalisation and shape of the final discriminant are compared to the default prediction. In order to propagate the uncertainty in the $p_T$ resolution, for each jet in the simulation, a random number $n_{\text{rnd}}$ is drawn from a Gaussian distribution with mean $\theta$ and sigma equal to the difference in quadrature between the fractional $p_T$ resolution with the tool and the nominal one. The jet 4-momentum is then scaled by a factor $1 + n_{\text{rnd}}$. Since jets in the simulation cannot be under-smeared, by definition the resulting uncertainty on the normalisation and shape of the final discriminant is one-sided. This uncertainty is then symmetrised.}

\item{\textbf{Heavy- and Light-Flavour Tagging}. The effects of uncertainties in efficiencies for the heavy flavour identification of jets by the b-tagging
algorithm have been evaluated. These efficiencies are measured from data and depend on the jet flavour.
Efficiencies for b and c quarks in the simulation have to be corrected by $p_T$-dependent factors. The
scale factors and their uncertainties are applied to each jet in the simulation depending on its flavour and
$p_T$ using the BTaggingCalibrationDataInterface. For b-jets we are using the $t\bar{t}$ calibration. In the case of light-flavour jets, the corrections also depend on jet $\eta$.}
\end{itemize}



\begin{table}[h]
\begin{center}
\begin{tabular}{|l|l|}
\hline
$ees$ & electron energy scale \\
$eer$ & electron energy resolution\\ 
$el\_idSF$ & electron ID efficiency scale factor \\
$el\_recSF$ & electron reconstruction efficiency scale factor \\
$el\_trigSF$ & electron trigger efficiency scale factor \\ \hline
$mums$ & muon momentum resolution (MS) \\
$muid$ & muon momentum resolution (ID) \\
$musc$ & muon momentum scale \\
$mu\_idSF$ & muon ID efficiency \\
$mu\_recSF$ & muon reconstruction efficiency \\
$mu\_trigSF$ & muon trigger efficiency \\ \hline
$res\_soft$ & $E^{miss}_{\text{T}}$ soft term resolution \\
$sc\_soft$ & $E^{miss}_{\text{T}}$ soft term scale \\ \hline
$btag$ & b-tagging scale factors, 9 components \\
$mistag$ & b-tagging mis-tag rate, 7 components \\
$ctautag$ & b-tagging $c/\tau$ tag rates \\ \hline
$JesEffectiveModel$ & Jet energy scale effective model, 4 components \\
$JesEffectiveDet$ & Jet energy scale effective detector, 3 components \\
$JesEffectiveStat$ & Jet energy scale effective statistical, 4 components \\ 
$JesEffectiveMix$ & Jet energy scale effective mix, 4 components \\
$EtaIntercalibration$ & Jet energy scale $\eta$ inter-calibration \\ 
$flavor\_comp$ & Jet energy scale flavour composition \\ 
$BJesUnc$ & Jet energy scale B-JES \\
$Pileup\_Rho$ & Jet energy scale pileup $\rho$ \\ 
$flavor\_response$ & Jet energy scale flavour response \\ 
$Pileup\_OffsetNPV$ & Jet energy scale pileup offset (NPV) \\ 
$Pileup\_OffsetMu$ & Jet energy scale pileup offset (<$\mu$>) \\ 
$Pileup\_Pt$ & Jet energy scale pileup ($p_T$) \\
$SinglePart$ & Jet energy scale single-particle\\ \hline 
$jvf$ & Jet vertex fraction \\ \hline
$jer$ & Jet energy resolution \\ \hline
$jeff$ & jet reconstruction efficiency \\ \hline

\end{tabular}
\caption{Notation for all the detector systematics considered in this analysis.} 
    \label{tab:notation}
      \end{center}  
\end{table}


By using the bootstrapping method with a loose criterium of requiring one bin of the systematically varied sample to not be consistent within the uncertainties with the nominal distribution, all systematic uncertainties are considered significant and used in the marginalization procedure of the unfolding. The marginalization is tested by taking the reconstructed nominal MC predictions (referred to as Asimov data set) and running the unfolding procedure including the marginalization on it. Figure~\ref{fig:nuisance_helpol_asimov} shows the fit and the corresponding uncertainty of each systematics uncertainty. As expected, all the nuisance parameters are very close to zero and a width close to 1 except for the $el\_idSF$ uncertainty, which is constrained as explained earlier in this section.
% Using the bootstrapping method we have found that only 10 systematics are significant for the $t\bar{t}$ asymmetry while only 4 for the lepton asymmetry. These systematic are taken into account within the unfolding procedure. In Fig \ref{fig:mar_ttbar} and Fig \ref{fig:mar_lepton} the nuisance parameters  for the inclusive asymmetry only including the systematics are significant are shown for the top and lepton asymmetry respectively.These results were obtained by unfolding Asimov data to the fiducial volume. As expected all the different nuisance parameter are $~0$ since we are unfolding Asimov data. The error represent the constrain that we are able to to apply to that systematic. It is possible to note that there is a strong constrain in b-tagging and in the electron identification. The constraint in b-tagging is coming from the fact that the last 4 bins of the distribution (which correspond to the $e\mu$ channel) are not sensitive to this systematic, while for the electron identification the 4 bins that correspond to the $\mu\mu$ are not sensitive. This can be easily seen in the significance distributions in appendix \ref{sec:Significance}.  
An estimate of the detector systematics using the distributions of the bootstrapping as input to the unfolding can be found in appendix~\ref{app:syst_tables}, along with a breakdown of the signal modelling systematics.

% \clearpage

 \begin{figure}
   \begin{center}
  \includegraphics[width=0.7\columnwidth]{unfolding/nuisance_fit/nuisance_helpol_asimov.png}

   \end{center}
  \caption{Nuisance parameters for the parton level unfolding of the helicity polarization using the Asimov data set. }  
 \label{fig:nuisance_helpol_asimov}
 \end{figure}
% \clearpage
% \begin{figure}
%   \begin{center}

%  \includegraphics[width=0.7\columnwidth]{figures/marginalization_lepton.eps}
%   \end{center}
%  \caption{Nuisance parameters for inclusive lepton asymmetry in the fiducial volume. }  
% \label{fig:mar_lepton}
% \end{figure}
% \clearpage
% \paragraph{Others}
% Luminosity uncertainty of 1.8\%.

% \subsection{?}