%\newcommand{\ttbar}{$t\bar{t}$} %already defined in atlasparticle.sty
\newcommand{\gluglu}{$gg$\xspace}
\newcommand*{\costheta}{\ensuremath{\cos \theta}\xspace}
\newcommand*{\coscos}{\ensuremath{\cos \theta_{1} \cos \theta_{2}}\xspace}
\newcommand*{\eeme}{$ee$\xspace}
\newcommand*{\emume}{$e\mu$\xspace} %could also take ensuremath...
\newcommand*{\mumume}{$\mu\mu$\xspace}

%\newcommand*{\pt}{\ensuremath{p_{\text{T}}}\xspace}

\def\pythia{{\sc Pythia6}\xspace}     
\def\alpgen{{\sc Alpgen}\xspace}     
\def\powheg{{\sc Powheg}-hvq\xspace}
\def\herwig{{\sc Herwig}\xspace}  
\def\jimmy{{\sc Jimmy}\xspace}       
\def\acermc{{\sc AcerMC}\xspace}     
\def\mcatnlo{{\sc MC@NLO}\xspace}

\newcommand*{\intlumi}{\ensuremath{\mathcal{L}}\xspace}
\newcommand*{\sqrts}{\ensuremath{\sqrt{s}}\xspace}
\newcommand*{\invfb}{\ensuremath{\text{fb}^{-1}}} %bit stupid that way :D

\newcommand{\atlas}{ATLAS\xspace}
\newcommand{\tevatron}{Tevatron\xspace}
\newcommand{\cms}{CMS\xspace}
\newcommand{\lhc}{LHC\xspace}

\newcommand{\ZX}{$Z+$jets}
\def\Zee{\ensuremath{Z \rightarrow ee}}
\def\Zmm{\ensuremath{Z \rightarrow \mu\mu}}
\def\Ztau{\ensuremath{Z \rightarrow \tau\tau}}

\newcommand{\mttwo}{$M_{\text{T}2}$}

\newcommand{\helpolplus}{\ensuremath{P_{k}^{+}}\xspace}
\newcommand{\helpolminus}{\ensuremath{P_{k}^{-}}\xspace}
\newcommand{\helpol}{\ensuremath{P_{k}}\xspace}
\newcommand{\transpolplus}{\ensuremath{P_{n}^{+}}\xspace}
\newcommand{\transpolminus}{\ensuremath{P_{n}^{-}}\xspace}
\newcommand{\transpol}{\ensuremath{P_{n}}\xspace}
\newcommand{\rpolplus}{\ensuremath{P_{r}^{+}}\xspace}
\newcommand{\rpolminus}{\ensuremath{P_{r}^{-}}\xspace}
\newcommand{\rpol}{\ensuremath{P_{r}}\xspace}
\newcommand{\helcorr}{\ensuremath{C(k,k)}\xspace}
\newcommand{\transcorr}{\ensuremath{C(n,n)}\xspace}
\newcommand{\rcorr}{\ensuremath{C(r,r)}\xspace}
\newcommand{\transhelsum}{\ensuremath{C(n,k)+C(k,n)}\xspace}
\newcommand{\transheldiff}{\ensuremath{C(n,k)-C(k,n)}\xspace}
\newcommand{\transrsum}{\ensuremath{C(n,r)+C(r,n)}\xspace}
\newcommand{\transrdiff}{\ensuremath{C(n,r)-C(r,n)}\xspace}
\newcommand{\rhelsum}{\ensuremath{C(r,k)+C(k,r)}\xspace}
\newcommand{\rheldiff}{\ensuremath{C(r,k)-C(k,r)}\xspace}

%results
\FPround\ResultHelpolPlus{-0.036558358676169189}{3}
\FPround\ResultHelpolMinus{-0.051669020487710778}{3}
\FPround\ResultTranspolPlus{-0.00097340221741963823}{3}
\FPround\ResultTranspolMinus{-0.018475632939946144}{3}
\FPround\ResultRpolPlus{-0.0068393238515553519}{3}
\FPround\ResultRpolMinus{0.024633160895394135}{3}
\FPmul\ResultHelcorrHelp{-0.043125606932264103}{-8.612}
\FPmul\ResultTranscorrHelp{-0.038339560131941355}{-8.612}
\FPmul\ResultRcorrHelp{-0.022895000496791856}{-8.612}
\FPmul\ResultTranshelsumHelp{0.014787492958223816}{-9}
\FPmul\ResultTransheldiffHelp{0.0042303595117527447}{-9}
\FPmul\ResultTransrsumHelp{0.00090636031751683524}{-9}
\FPmul\ResultTransrdiffHelp{-0.00033404264551004147}{-9}
\FPmul\ResultRhelsumHelp{0.032757610184887216}{-9}
\FPmul\ResultRheldiffHelp{-0.0072382922213698594}{-9}
\FPround\ResultHelcorr{\ResultHelcorrHelp}{3}
\FPround\ResultTranscorr{\ResultTranscorrHelp}{3}
\FPround\ResultRcorr{\ResultRcorrHelp}{3}
\FPround\ResultTranshelsum{\ResultTranshelsumHelp}{3}
\FPround\ResultTransheldiff{\ResultTransheldiffHelp}{3}
\FPround\ResultTransrsum{\ResultTransrsumHelp}{3}
\FPround\ResultTransrdiff{\ResultTransrdiffHelp}{3}
\FPround\ResultRhelsum{\ResultRhelsumHelp}{3}
\FPround\ResultRheldiff{\ResultRheldiffHelp}{3}


\FPround\UncertMargeHelpolPlus{0.023598051433393399}{3}
\FPround\UncertMargeHelpolMinus{0.02379198423284113}{3}
\FPround\UncertMargeTranspolPlus{0.022961507984989134}{3}
\FPround\UncertMargeTranspolMinus{0.022610407037266536}{3}
\FPround\UncertMargeRpolPlus{0.0230746422511913}{3}
\FPround\UncertMargeRpolMinus{0.02719749786489177}{3}

\FPmul\UncertMargeHelcorrHelp{0.0053409963456708528}{8.612} %because
%                                of the mean calibration
\FPmul\UncertMargeTranscorrHelp{0.0028970802228188777}{8.612}
\FPmul\UncertMargeRcorrHelp{0.0057541772755775808}{8.612}
\FPmul\UncertMargeTranshelsumHelp{0.0067360101151341667}{9}
\FPmul\UncertMargeTransheldiffHelp{0.0051207138717197067}{9}
\FPmul\UncertMargeTransrsumHelp{0.0066949162095112282}{9}
\FPmul\UncertMargeTransrdiffHelp{0.007185848696636507}{9}
\FPmul\UncertMargeRhelsumHelp{0.00793325350806642}{9}
\FPmul\UncertMargeRheldiffHelp{0.0088912767287442334}{9}
\FPround\UncertMargeHelcorr{\UncertMargeHelcorrHelp}{3}
\FPround\UncertMargeTranscorr{\UncertMargeTranscorrHelp}{3}
\FPround\UncertMargeRcorr{\UncertMargeRcorrHelp}{3}
\FPround\UncertMargeTranshelsum{\UncertMargeTranshelsumHelp}{3}
\FPround\UncertMargeTransheldiff{\UncertMargeTransheldiffHelp}{3}
\FPround\UncertMargeTransrsum{\UncertMargeTransrsumHelp}{3}
\FPround\UncertMargeTransrdiff{\UncertMargeTransrdiffHelp}{3}
\FPround\UncertMargeRhelsum{\UncertMargeRhelsumHelp}{3}
\FPround\UncertMargeRheldiff{\UncertMargeRheldiffHelp}{3}

\newcommand{\UncertModHelpolPlus}{0.02}
\newcommand{\UncertModHelpolMinus}{0.02}
\newcommand{\UncertModTranspolPlus}{0.015}
\newcommand{\UncertModTranspolMinus}{0.015}
\newcommand{\UncertModRpolPlus}{0.024}
\newcommand{\UncertModRpolMinus}{0.024}
\newcommand{\UncertModHelcorr}{0.044}
\newcommand{\UncertModTranscorr}{0.063}
\newcommand{\UncertModRcorr}{0.055}
\newcommand{\UncertModTranshelsum}{0.098}
\newcommand{\UncertModTransheldiff}{0.035}
\newcommand{\UncertModTransrsum}{0.089}
\newcommand{\UncertModTransrdiff}{0.040}
\newcommand{\UncertModRhelsum}{0.087}
\newcommand{\UncertModRheldiff}{0.034}

%particle measurement
\FPmul\ResultFidHelpolPlusHelp{-0.036558358676169189}{3}
\FPmul\ResultFidHelpolMinusHelp{-0.051669020487710778}{3}
\FPmul\ResultFidTranspolPlusHelp{-0.00097340221741963823}{3}
\FPmul\ResultFidTranspolMinusHelp{-0.018475632939946144}{3}
\FPmul\ResultFidRpolPlusHelp{-0.0068393238515553519}{3}
\FPmul\ResultFidRpolMinusHelp{0.024633160895394135}{3}
\FPmul\ResultFidHelcorrHelp{-0.043125606932264103}{-9} % fuers erste...
\FPmul\ResultFidTranscorrHelp{-0.038339560131941355}{-9}
\FPmul\ResultFidRcorrHelp{-0.022895000496791856}{-9}
\FPmul\ResultFidTranshelsumHelp{0.014787492958223816}{-9}
\FPmul\ResultFidTransheldiffHelp{0.0042303595117527447}{-9}
\FPmul\ResultFidTransrsumHelp{0.00090636031751683524}{-9}
\FPmul\ResultFidTransrdiffHelp{-0.00033404264551004147}{-9}
\FPmul\ResultFidRhelsumHelp{0.032757610184887216}{-9}
\FPmul\ResultFidRheldiffHelp{-0.0072382922213698594}{-9}
\FPround\ResultFidHelpolPlus{\ResultFidHelpolPlusHelp}{3}
\FPround\ResultFidHelpolMinus{\ResultFidHelpolMinusHelp}{3}
\FPround\ResultFidTranspolPlus{\ResultFidTranspolPlusHelp}{3}
\FPround\ResultFidTranspolMinus{\ResultFidTranspolMinusHelp}{3}
\FPround\ResultFidRpolPlus{\ResultFidRpolPlusHelp}{3}
\FPround\ResultFidRpolMinus{\ResultFidRpolMinusHelp}{3}
\FPround\ResultFidHelcorr{\ResultFidHelcorrHelp}{3}
\FPround\ResultFidTranscorr{\ResultFidTranscorrHelp}{3}
\FPround\ResultFidRcorr{\ResultFidRcorrHelp}{3}
\FPround\ResultFidTranshelsum{\ResultFidTranshelsumHelp}{3}
\FPround\ResultFidTransheldiff{\ResultFidTransheldiffHelp}{3}
\FPround\ResultFidTransrsum{\ResultFidTransrsumHelp}{3}
\FPround\ResultFidTransrdiff{\ResultFidTransrdiffHelp}{3}
\FPround\ResultFidRhelsum{\ResultFidRhelsumHelp}{3}
\FPround\ResultFidRheldiff{\ResultFidRheldiffHelp}{3}


\FPmul\UncertMargeFidHelpolPlusHelp{0.023598051433393399}{3}
\FPmul\UncertMargeFidHelpolMinusHelp{0.02379198423284113}{3}
\FPmul\UncertMargeFidTranspolPlusHelp{0.022961507984989134}{3}
\FPmul\UncertMargeFidTranspolMinusHelp{0.022610407037266536}{3}
\FPmul\UncertMargeFidRpolPlusHelp{0.0230746422511913}{3}
\FPmul\UncertMargeFidRpolMinusHelp{0.02719749786489177}{3}
\FPmul\UncertMargeFidHelcorrHelp{0.0053409963456708528}{9} %because
%                                of the mean calibration
\FPmul\UncertMargeFidTranscorrHelp{0.0028970802228188777}{9}
\FPmul\UncertMargeFidRcorrHelp{0.0057541772755775808}{9}
\FPmul\UncertMargeFidTranshelsumHelp{0.0067360101151341667}{9}
\FPmul\UncertMargeFidTransheldiffHelp{0.0051207138717197067}{9}
\FPmul\UncertMargeFidTransrsumHelp{0.0066949162095112282}{9}
\FPmul\UncertMargeFidTransrdiffHelp{0.007185848696636507}{9}
\FPmul\UncertMargeFidRhelsumHelp{0.00793325350806642}{9}
\FPmul\UncertMargeFidRheldiffHelp{0.0088912767287442334}{9}
\FPround\UncertMargeFidHelpolPlus{\UncertMargeFidHelpolPlusHelp}{3}
\FPround\UncertMargeFidHelpolMinus{\UncertMargeFidHelpolMinusHelp}{3}
\FPround\UncertMargeFidTranspolPlus{\UncertMargeFidTranspolPlusHelp}{3}
\FPround\UncertMargeFidTranspolMinus{\UncertMargeFidTranspolMinusHelp}{3}
\FPround\UncertMargeFidRpolPlus{\UncertMargeFidRpolPlusHelp}{3}
\FPround\UncertMargeFidRpolMinus{\UncertMargeFidRpolMinusHelp}{3}
\FPround\UncertMargeFidHelcorr{\UncertMargeFidHelcorrHelp}{3}
\FPround\UncertMargeFidTranscorr{\UncertMargeFidTranscorrHelp}{3}
\FPround\UncertMargeFidRcorr{\UncertMargeFidRcorrHelp}{3}
\FPround\UncertMargeFidTranshelsum{\UncertMargeFidTranshelsumHelp}{3}
\FPround\UncertMargeFidTransheldiff{\UncertMargeFidTransheldiffHelp}{3}
\FPround\UncertMargeFidTransrsum{\UncertMargeFidTransrsumHelp}{3}
\FPround\UncertMargeFidTransrdiff{\UncertMargeFidTransrdiffHelp}{3}
\FPround\UncertMargeFidRhelsum{\UncertMargeFidRhelsumHelp}{3}
\FPround\UncertMargeFidRheldiff{\UncertMargeFidRheldiffHelp}{3}

\newcommand{\UncertModFidHelpolPlus}{0.}
\newcommand{\UncertModFidHelpolMinus}{0}
\newcommand{\UncertModFidTranspolPlus}{0.}
\newcommand{\UncertModFidTranspolMinus}{0.}
\newcommand{\UncertModFidRpolPlus}{0.}
\newcommand{\UncertModFidRpolMinus}{0.}
\newcommand{\UncertModFidHelcorr}{0.}
\newcommand{\UncertModFidTranscorr}{0.}
\newcommand{\UncertModFidRcorr}{0.}
\newcommand{\UncertModFidTranshelsum}{0.}
\newcommand{\UncertModFidTransheldiff}{0.}
\newcommand{\UncertModFidTransrsum}{0.}
\newcommand{\UncertModFidTransrdiff}{0.}
\newcommand{\UncertModFidRhelsum}{0.}
\newcommand{\UncertModFidRheldiff}{0.}


% FBU-related                                                                                                                                                                          
\providecommand{\abs}[1]{\lvert#1\rvert}
%\newcommand{\dy}{\ensuremath{\Delta{}\abs{y}}}                                                                                                                                        
\newcommand{\diffVar}{\ensuremath{\varv{}}}
\newcommand{\vect}[1]{\boldsymbol{#1}}
\newcommand{\Truth}{\ensuremath{\vect{T}}}
\newcommand{\Data}{\ensuremath{\vect{D}}}
\newcommand{\TrasfMatrix}{\ensuremath{\mathcal{M}}}
\newcommand{\Real}{\ensuremath{\mathbb{R}}}
\newcommand{\Integer}{\ensuremath{\mathbb{N}}}
\newcommand{\conditionalProb}[2]{\ensuremath{p\left(#1|#2\right)}}
\newcommand{\conditionalLhood}[2]{\ensuremath{\mathcal{L}\left(#1|#2\right)}}
\newcommand{\prior}{\ensuremath{\pi{}\left(\Truth{}\right)}}
\newcommand{\Tinf}{\ensuremath{T\ulcorner{}}}
\newcommand{\Tsup}{\ensuremath{T\urcorner{}}}
\DeclareMathOperator{\Erf}{Erf}
