\select@language {UKenglish}
\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {section}{\numberline {2}Observables}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Spin density matrix and the set of observables}{5}{subsection.2.1}
\contentsline {section}{\numberline {3}Monte Carlo}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}Parton level}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Stable-particle level}{9}{subsection.3.2}
\contentsline {section}{\numberline {4}Selection}{13}{section.4}
\contentsline {subsection}{\numberline {4.1}Object Selection}{13}{subsection.4.1}
\contentsline {paragraph}{Jets}{13}{section*.7}
\contentsline {paragraph}{Electrons}{13}{section*.8}
\contentsline {paragraph}{Muons}{13}{section*.9}
\contentsline {paragraph}{Missing Transverse Momentum}{13}{section*.10}
\contentsline {paragraph}{Trigger Requirements}{14}{section*.11}
\contentsline {paragraph}{Event Cleaning}{14}{section*.12}
\contentsline {subsection}{\numberline {4.2}Dilepton selection}{14}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}$Z+$jets\ Background Estimation}{15}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Fake Background Estimation}{18}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Comparison of Data and Simulation}{19}{subsubsection.4.2.3}
\contentsline {section}{\numberline {5}Reconstruction}{23}{section.5}
\contentsline {subsection}{\numberline {5.1}The kinematic reconstruction method}{23}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Comparison of Data and Simulation}{24}{subsection.5.2}
\contentsline {section}{\numberline {6}Unfolding}{27}{section.6}
\contentsline {subsection}{\numberline {6.1}FBU}{27}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}FBU Ingredients}{27}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Prior Choice}{27}{subsubsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.3}Marginalization}{28}{subsubsection.6.1.3}
\contentsline {subsection}{\numberline {6.2}Parton level unfolding}{28}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Binning choice}{30}{subsubsection.6.2.1}
\contentsline {subsection}{\numberline {6.3}Stable-particle level unfolding}{34}{subsection.6.3}
\contentsline {section}{\numberline {7}Systematics}{35}{section.7}
\contentsline {subsection}{\numberline {7.1}Procedure used to assess systematic uncertainties}{35}{subsection.7.1}
\contentsline {subsubsection}{\numberline {7.1.1}Generator Modelling Uncertainties}{35}{subsubsection.7.1.1}
\contentsline {subsubsection}{\numberline {7.1.2}Detector Modelling Uncertainties}{35}{subsubsection.7.1.2}
\contentsline {subsection}{\numberline {7.2}List of systematics considered}{35}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}\ensuremath {t\bar {t}}\xspace \ modelling systematics}{35}{subsubsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.2}Background uncertainties}{36}{subsubsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.3}Detector modelling systematics}{36}{subsubsection.7.2.3}
\contentsline {section}{\numberline {8}Results}{40}{section.8}
\contentsline {section}{\numberline {9}Conclusion}{41}{section.9}
\contentsline {section}{\numberline {A}Response matrix comparisons}{42}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Charge comparison for polarization observables}{42}{subsection.A.1}
\contentsline {subsection}{\numberline {A.2}Channel comparison for observables}{42}{subsection.A.2}
\contentsline {section}{\numberline {B}Further plots and tables}{47}{appendix.B}
\contentsline {subsection}{\numberline {B.1}Resolutions}{47}{subsection.B.1}
\contentsline {subsubsection}{\numberline {B.1.1}Partonic}{47}{subsubsection.B.1.1}
\contentsline {subsubsection}{\numberline {B.1.2}Stable particle}{47}{subsubsection.B.1.2}
\contentsline {subsection}{\numberline {B.2}Binnings}{47}{subsection.B.2}
\contentsline {subsection}{\numberline {B.3}Response matrices}{52}{subsection.B.3}
\contentsline {subsubsection}{\numberline {B.3.1}Partonic}{52}{subsubsection.B.3.1}
\contentsline {subsubsection}{\numberline {B.3.2}Stable-Particle}{54}{subsubsection.B.3.2}
\contentsline {subsection}{\numberline {B.4}Acceptance efficiencies}{54}{subsection.B.4}
\contentsline {subsubsection}{\numberline {B.4.1}Partonic}{54}{subsubsection.B.4.1}
\contentsline {subsubsection}{\numberline {B.4.2}Stable-Particle}{54}{subsubsection.B.4.2}
