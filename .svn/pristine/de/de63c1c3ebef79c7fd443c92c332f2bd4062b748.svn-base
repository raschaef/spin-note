@article{FBU,
      author         = "Choudalakis, Georgios",
      title          = "{Fully Bayesian Unfolding}",
      year           = "2012",
      eprint         = "1201.4612",
      archivePrefix  = "arXiv",
      primaryClass   = "physics.data-an",
      SLACcitation   = "%%CITATION = ARXIV:1201.4612;%%",
}

@misc{PyFBU,
title = {PyFBU},
howpublished = {\url{https://pypi.python.org/pypi/fbu}}
}

@Article{topdisc1,
  author =	 "CDF Collaboration",
  title =	 "Observation of Top Quark Production in $\bar{p}p$ Collisions with the Collider Detector at Fermilab",
  journal =	 "Physical Review Letters",
  volume =	 {74},
  number =       {14},
  year =	 {1995},
  pages =	 {2626-2631}
}

@Article{topdisc2,
  author =	 "D${0}$ Collaboration",
  title =	 "Search for High Mass Top Quark Production in $p \bar{p}$ Collisions at $\sqrt{s} = 1.8 \text{ TeV}$",
  journal =	 "Physical Review Letters",
  volume =	 {74},
  number =       {13},
  year =	 {1995},
  pages =	 {2422-2426}
}

@article{atlas_pol,
      title          = "{Measurement of Top Quark Polarization in Top-Antitop
                        Events from Proton-Proton Collisions at $\sqrt{s}=7$ TeV Using the ATLAS Detector}",
      collaboration  = "ATLAS Collaboration",
      journal        = "Phys.Rev.Lett.",
      number         = "23",
      volume         = "111",
      pages          = "232002",
      doi            = "10.1103/PhysRevLett.111.232002",
      year           = "2013",
      eprint         = "1307.6511",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "CERN-PH-EP-2013-101",
      SLACcitation   = "%%CITATION = ARXIV:1307.6511;%%",
}

@article{atlas_spincorr,
      title          = "{Measurements of spin correlation in top-antitop quark
                        events from proton-proton collisions at $\sqrt{s}=7$ TeV
                        using the ATLAS detector}",
      collaboration  = "ATLAS Collaboration",
      journal        = "Phys.Rev.",
      number         = "11",
      volume         = "D90",
      pages          = "112016",
      doi            = "10.1103/PhysRevD.90.112016",
      year           = "2014",
      eprint         = "1407.4314",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "CERN-PH-EP-2014-116",
      SLACcitation   = "%%CITATION = ARXIV:1407.4314;%%",
}

@article{cms_spinpol,
      title          = "{Measurements of $t\bar{t}$ spin correlations and
                        top-quark polarization using dilepton final states in $pp$
                        collisions at $\sqrt{s}$ = 7 TeV}",
      collaboration  = "CMS Collaboration",
      journal        = "Phys.Rev.Lett.",
      number         = "18",
      volume         = "112",
      pages          = "182001",
      doi            = "10.1103/PhysRevLett.112.182001",
      year           = "2014",
      eprint         = "1311.3924",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "CMS-TOP-13-003, CERN-PH-EP-2013-211",
      SLACcitation   = "%%CITATION = ARXIV:1311.3924;%%",
}

@Book{thikonov,
  author =       "A. N. Thikonov",
  title =        "On the solution of improperly posed problems and the method of regularization",
  publisher =    "Sov. Math.",
  year =         1963
}

@misc{PyFBU,
title = {PyFBU package},
howpublished = {\url{https://pypi.python.org/pypi/fbu}}
}  


@Book{thikonov,
  author =       "A. N. Thikonov",
  title =        "On the solution of improperly posed problems and the method of regularization",
  publisher =    "Sov. Math.",
  year =         1963
}

@techreport{SpinCorrNote,
      author       = "Neep, T and  Owen, M and Schwanenberger, Ch" ,
      title        = "A measurement of spin correlation in top
                  anti-top pairs and search for top squarks at
                  $\sqrt{s} = 8$~TeV using the ATLAS detector",  
      institution  = "CERN",
      address      = "Geneva",
      number       = "ATL-COM-PHYS-2014-286",
      month        = "Sep",
      year         = "2014",
}

@techreport{Fakes8TeV,
      author       = "Becker, K and Cornelissen, T and Derue, F and
                  Ferrari, A and  Henrichs, A and  Hirschbuehl, D and
                  Lei, X and Nackenhorst, O and O’Grady, F and
                  Pelikan, D and Pinamonti, M and Pires, S and
                  Sjolin, J and Soualah, R and Tepel P" ,
      title        = "Estimation of non-prompt and fake leptons
                  background in final states with top quarks produced
                  in proton-proton collision at $\sqrt{s} = 8$~TeV with the 
                  ATLAS detector",  
      institution  = "CERN",
      address      = "Geneva",
      number       = "ATL-COM-PHYS-2013-1100",
      month        = "Sep",
      year         = "2014",
}

@techreport{top_objects,
      author        = "Acharya, B and Adelman, J and Adomeit, S and Aoki, M and
                       Alvarez, B and Asquith, L and Balli, F and Bell, W and
                       Becker, K and Behr, K and Benjamin, D and Bergeaas
                       Kuutmann, E and Bernard, C and Black, K and Calvet, S and
                       Camacho, R and Coadou, Y and Cortiana, G and Compostella, G
                       and Cooper-Smith, N and Cornelissen, T and Cristinziani, M
                       and Dao, V and De Sanctis, U and Doglioni, C and Derue, F
                       and Eckardt, C and Ferrando, J and Ferreira de Lima, D and
                       Finella, K and Grahn, K and Groth-Jensen, J and Gutierrez
                       Ortiz, N and Hawkings, R and Head, S and Henrichs, A and
                       Hirschbuehl, D and Kasieczka, G and Kaushik, V and
                       Khandanyan, H and Kind, O and Krasznahorkay, A and Kuhl, T
                       and Lacker, H and Le Menedeu, E and Lee, H and Lister, A
                       and Loureiro, K and Mijovic, L and Morris, J and Moles
                       Valls, R and Nackenhorst, O and zur Nedden, M and Owen, M
                       and Pelikan, D and Pinamonti, M and Rao, K and Rosbach, K
                       and Rudolph, M and Salamanna, G and Schwindling, J and
                       Searcy, J and Shabalina, E and Shaw, K and Sjolin, J and
                       Soualah, R and Stamm, S and Ta, D and Theveneaux-Pelzer, T
                       and Thompson, E and Uchikda, K and Valery, L and Vreeswijk,
                       M and Wasicki, C and Watson, I and Yau, K and Zhong, J and
                       Zhu, H",
      title         = "{Object selection and calibration, background estimations
                       and MC samples for top quark analyses using the full 2012
                       data set}",
      institution   = "CERN",
      address       = "Geneva",
      number        = "ATL-COM-PHYS-2013-1016",
      month         = "Jul",
      year          = "2013",
}

@article{spin_anapow_lep,
      author         = "Czarnecki, Andrzej and Jezabek, Marek and Kuhn, Johann
                        H.",
      title          = "{Lepton Spectra From Decays of Polarized Top Quarks}",
      journal        = "Nucl.Phys.",
      volume         = "B351",
      pages          = "70-80",
      doi            = "10.1016/0550-3213(91)90082-9",
      year           = "1991",
      reportNumber   = "MPI-PAE-PTH-78-89",
      SLACcitation   = "%%CITATION = NUPHA,B351,70;%%",
}

@misc{TopCommonObj,
title = {Top Common Objects 2012},
howpublished = {\url{https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopCommonObjects}}
}

@techreport{Guindon:1638000,
      author        = "Guindon, S and Shabalina, E and Adelman, J and Alhroob, M
                       and Amor dos Santos, S and Basye, A and Bouffard, J and
                       Casolino, M and Connelly, I and Cortes Gonzalez, A and Dao,
                       V and D'Auria, S and Doyle, A and Ferrari, P and Filthaut,
                       F and Goncalo, R and de Groot, N and Henkelmann, S and
                       Jain, V and Juste, A and Kirby, G and Kar, D and Knue, A
                       and Kroeninger, K and Liss, T and Le Menedeu, E and Montejo
                       Berlingen, J and Moreno Llacer, M and Nackenhorst, O and
                       Neep, T and Onofre, A and Owen, M and Pinamonti, M and Qin,
                       Y and Quadt, A and Quilty, D and Schwanenberger, C and
                       Serkin, L and St Denis, R and Thomas-Wilsker, J and
                       Vazquez-Schroeder, T",
      title         = "{Search for the Standard Model Higgs boson produced in
                       association with top quarks and decaying to $b\bar{b}$ in
                       pp collisions at $\sqrt{s}$= 8 TeV with the ATLAS detector
                       at the LHC}",
      institution   = "CERN",
      address       = "Geneva",
      number        = "ATL-COM-PHYS-2013-1659",
      month         = "Dec",
      year          = "2013",
      note          = "The note contains internal documentation of the ttH(bb)
                       analysis approved as a preliminary result
                       (ATLAS-CONF-2014-011)",
}

@misc{TopSysUncertainties,
title = {Top Systematic uncertainties 2012},
howpublished = {\url{https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopSystematicUncertainties}}
}  

@Article{Abulencia:2006js,
     author    = "Abulencia, A. and others",
 collaboration = "CDF",
     title     = "{Measurement of the top quark mass using template methods
                  on dilepton events in p anti-p collisions at s**(1/2) =
                  1.96-TeV}",
     journal   = "Phys. Rev.",
     volume    = "D73",
     year      = "2006",
     pages     = "112006",
     eprint    = "hep-ex/0602008",
     archivePrefix = "arXiv",
     doi       = "10.1103/PhysRevD.73.112006",
     SLACcitation  = "%%CITATION = HEP-EX/0602008;%%"
}

@article{Aaltonen:2012tk,
      author         = "Aaltonen, T. and others",
      title          = "{$W$ boson polarization measurement in the $t\bar{t}$
                        dilepton channel using the CDF II Detector}",
      collaboration  = "CDF Collaboration",
      journal        = "Phys.Lett.",
      volume         = "B722",
      pages          = "48-54",
      doi            = "10.1016/j.physletb.2013.03.032",
      year           = "2013",
      eprint         = "1205.0354",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "FERMILAB-PUB-12-114-E",
      SLACcitation   = "%%CITATION = ARXIV:1205.0354;%%",
}

@article{Abazov:2004cs,
	author         = "Abazov, V.M. and others",
	title          = "{A precision measurement of the mass of the top quark}",
	collaboration  = "D0 Collaboration",
	journal        = "Nature",
	volume         = "429",
	pages          = "638-642",
	doi            = "10.1038/nature02589",
	year           = "2004",
	eprint         = "hep-ex/0406031",
	archivePrefix  = "arXiv",
	primaryClass   = "hep-ex",
	reportNumber   = "FERMILAB-PUB-04-083-E",
	SLACcitation   = "%%CITATION = HEP-EX/0406031;%%",
}

@article{Abbott:1997fv,
      author         = "Abbott, B. and others",
      title          = "{Measurement of the top quark mass using dilepton
                        events}",
      collaboration  = "D0 Collaboration",
      journal        = "Phys.Rev.Lett.",
      volume         = "80",
      pages          = "2063-2068",
      doi            = "10.1103/PhysRevLett.80.2063",
      year           = "1998",
      eprint         = "hep-ex/9706014",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "FERMILAB-PUB-97-172-E",
      SLACcitation   = "%%CITATION = HEP-EX/9706014;%%",                                                                                                                                        
}

@techreport{ATLAS-CONF-2012-057,
      title         = "{Measurement of the charge asymmetry in dileptonic decay
                       of top quark pairs in pp collisions at $\sqrt{s}$ = 7 TeV using
                       the ATLAS detector}",
      institution   = "CERN",
      collaboration = "ATLAS Collaboration",
      address       = "Geneva",
      number        = "ATLAS-CONF-2012-057",
      month         = "Jun",
      year          = "2012",
}

@techreport{Borroni:1536502,
      author        = "Borroni, S and Deliot, F and Deterre, C and Grohsjean, A
                       and Hamilton, S and Mijovic, L and Peters, RY and Rudolph,
                       M and Schaefer, R",
      title         = "{Measurement of top quark polarisation in $t\bar{t}$
                       events with the ATLAS detector in proton-proton collisions
                       at $\sqrt{s} = 7$ TeV}",
      institution   = "CERN",
      address       = "Geneva",
      number        = "ATL-COM-PHYS-2013-367",
      month         = "Mar",
      year          = "2013",
}

@article{mt2reco,
      author         = "Guadagnoli, Diego and Park, Chan Beom",
      title          = "{$M_{T2}$-reconstructed invisible momenta as spin
                        analizers, and an application to top polarization}",
      journal        = "JHEP",
      volume         = "1401",
      pages          = "030",
      doi            = "10.1007/JHEP01(2014)030",
      year           = "2014",
      eprint         = "1308.2226",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "LAPTH-042-13, CERN-PH-TH-2013-190",
      SLACcitation   = "%%CITATION = ARXIV:1308.2226;%%",
}

@techreport{KinRecoNote,
      author       = "Antos, J and  Borroni, S and Chapelain, A and
                  D\'eliot, F and Delong, V and  Deterre, C and
                  Howarth, J and Lysak, R and Mijovic, L and  Naranjo,
                  R and  Peters, Y and Schafer, R" ,
      title        = "Performance studies of ttbar reconstruction
                      methods in the dilepton channel at $\sqrt{s} = 8$~TeV in
                      ATLAS",  
      institution  = "CERN",
      address      = "Geneva",
      number       = "ATL-COM-PHYS-2014-226",
      month        = "Mar",
      year         = "2014",
}

@Book{NumerRecip,
  author =	"Press, W.H. and Teukolsky, S.A. and Vetterling, W.T. and Flannery, B.P.",
  title =	"Numerical Recipes: The Art of Scientific Computing (3rd ed.)",
  publisher =	"Cambridge University Press",
  year = 	"2007",
  address =	"New York",
  ISBN = 	"978-0-521-88068-8",
}

@article{ATLAS:2011qia,
      key            = "1204180",
      title          = "{Commissioning of the ATLAS high-performance b-tagging
                        algorithms in the 7 TeV collision data}",
      collaboration  = "ATLAS Collaboration",
      year           = "2011",
      reportNumber   = "ATLAS-CONF-2011-102, ATLAS-COM-CONF-2011-110",
      SLACcitation   = "%%CITATION = ATLAS-CONF-2011-102 ETC.;%%",
}

@article{Erdmann:2013rxa,
      author         = "Erdmann, Johannes and Guindon, Stefan and Kroeninger,
                        Kevin and Lemmer, Boris and Nackenhorst, Olaf and others",
      title          = "{A likelihood-based reconstruction algorithm for
                        top-quark pairs and the KLFitter framework}",
      year           = "2013",
      eprint         = "1312.5595",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      journal        = "arXiv:1312.5595",
      SLACcitation   = "%%CITATION = ARXIV:1312.5595;%%",
}

@misc{top_object_selection,
    author    = "{B. Acharya et al.}",
    title     = "Object selection and calibration, background estimations and MC samples for the Autumn 2012 Top Quark analyses with 2011 data",
    howpublished = "{ATL-COM-PHYS-2012-1197, \url{https://cds.cern.ch/record/1472525}}",
    year     = "2012",
    month    = "Aug"
}

@article{dzero_asym_early,
      author         = "Abazov, Victor Mukhamedovich and others",
      title          = "{Measurement of Leptonic Asymmetries and Top Quark
                        Polarization in $t\bar{t}$ Production}",
      collaboration  = "D0",
      journal        = "Phys.Rev.",
      number         = "1",
      volume         = "D87",
      pages          = "011103",
      doi            = "10.1103/PhysRevD.87.011103",
      year           = "2013",
      eprint         = "1207.0364",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "FERMILAB-PUB-12-319-E",
      SLACcitation   = "%%CITATION = ARXIV:1207.0364;%%",
}

@article{cdf_asym_early,
      author         = "Aaltonen, T. and others",
      title          = "{Measurement of the top quark forward-backward production
                        asymmetry and its dependence on event kinematic
                        properties}",
      collaboration  = "CDF",
      journal        = "Phys.Rev.",
      number         = "9",
      volume         = "D87",
      pages          = "092002",
      doi            = "10.1103/PhysRevD.87.092002",
      year           = "2013",
      eprint         = "1211.1003",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "FERMILAB-PUB-12-591-E",
      SLACcitation   = "%%CITATION = ARXIV:1211.1003;%%",
}

@article{theo_asym_new,
      author         = "Czakon, Michal and Fiedler, Paul and Mitov, Alexander",
      title          = "{Resolving the Tevatron top quark forward-backward
                        asymmetry puzzle}",
      year           = "2014",
      eprint         = "1411.3007",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "CAVENDISH-HEP-14-10, TTK-14-32",
      SLACcitation   = "%%CITATION = ARXIV:1411.3007;%%",
}

@article{dzero_asym_new,
      author         = "Abazov, Victor Mukhamedovich and others",
      title          = "{Measurement of the forward-backward asymmetry in top
                        quark-antiquark production in ppbar collisions using the
                        lepton+jets channel}",
      collaboration  = "D0",
      journal        = "Phys.Rev.",
      number         = "7",
      volume         = "D90",
      pages          = "072011",
      doi            = "10.1103/PhysRevD.90.072011",
      year           = "2014",
      eprint         = "1405.0421",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "FERMILAB-PUB-14-116-E",
      SLACcitation   = "%%CITATION = ARXIV:1405.0421;%%",
}

@article{cdf_asym_new,
      author         = "Aaltonen, T. and others",
      title          = "{Measurement of the top quark forward-backward production
                        asymmetry and its dependence on event kinematic
                        properties}",
      collaboration  = "CDF",
      journal        = "Phys.Rev.",
      number         = "9",
      volume         = "D87",
      pages          = "092002",
      doi            = "10.1103/PhysRevD.87.092002",
      year           = "2013",
      eprint         = "1211.1003",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "FERMILAB-PUB-12-591-E",
      SLACcitation   = "%%CITATION = ARXIV:1211.1003;%%",
}

@misc{pseudotop,
  title = {Pseudo top quark definition for \atlas and \cms},
  howpublished = {\url{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/ParticleLevelTopDefinitions}},
}

@misc{top_systematics,
  title = {Systematics description of the top working group},
  howpublished = {\url{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopSystematicUncertainties}},
}

@article{powhegbox,
      author         = "Frixione, Stefano and Nason, Paolo and Oleari, Carlo",
      title          = "{Matching NLO QCD computations with Parton Shower
                        simulations: the POWHEG method}",
      journal        = "JHEP",
      volume         = "0711",
      pages          = "070",
      doi            = "10.1088/1126-6708/2007/11/070",
      year           = "2007",
      eprint         = "0709.2092",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "BICOCCA-FT-07-9, GEF-TH-21-2007",
      SLACcitation   = "%%CITATION = ARXIV:0709.2092;%%",
}

@article{pythia6,
      author         = "Sjostrand, Torbjorn and Mrenna, Stephen and Skands, Peter
                        Z.",
      title          = "{PYTHIA 6.4 Physics and Manual}",
      journal        = "JHEP",
      volume         = "0605",
      pages          = "026",
      doi            = "10.1088/1126-6708/2006/05/026",
      year           = "2006",
      eprint         = "hep-ph/0603175",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "FERMILAB-PUB-06-052-CD-T, LU-TP-06-13",
      SLACcitation   = "%%CITATION = HEP-PH/0603175;%%",
}

@article{bernreuther_8tev,
      author         = "Bernreuther, Werner and Si, Zong-Guo",
      title          = "{Top quark spin correlations and polarization at the LHC:
                        standard model predictions and effects of anomalous top
                        chromo moments}",
      journal        = "Phys.Lett.",
      volume         = "B725",
      pages          = "115-122",
      doi            = "10.1016/j.physletb.2013.06.051,
                        10.1016/j.physletb.2015.03.035",
      year           = "2013",
      eprint         = "1305.2066",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "TTK-13-13",
      SLACcitation   = "%%CITATION = ARXIV:1305.2066;%%",
}

@article{baumgart_transpol,
      author         = "Baumgart, Matthew and Tweedie, Brock",
      title          = "{Transverse Top Quark Polarization and the ttbar
                        Forward-Backward Asymmetry}",
      journal        = "JHEP",
      volume         = "1308",
      pages          = "072",
      doi            = "10.1007/JHEP08(2013)072",
      year           = "2013",
      eprint         = "1303.1200",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      SLACcitation   = "%%CITATION = ARXIV:1303.1200;%%",
}

@article{mcatnlo,
      author         = "Frixione, Stefano and Webber, Bryan R.",
      title          = "{Matching NLO QCD computations and parton shower
                        simulations}",
      journal        = "JHEP",
      volume         = "0206",
      pages          = "029",
      doi            = "10.1088/1126-6708/2002/06/029",
      year           = "2002",
      eprint         = "hep-ph/0204244",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "CAVENDISH-HEP-02-01, LAPTH-905-02, GEF-TH-2-2002",
      SLACcitation   = "%%CITATION = HEP-PH/0204244;%%",
}

@article{alpgen,
      author         = "Mangano, Michelangelo L. and Moretti, Mauro and
                        Piccinini, Fulvio and Pittau, Roberto and Polosa, Antonio
                        D.",
      title          = "{ALPGEN, a generator for hard multiparton processes in
                        hadronic collisions}",
      journal        = "JHEP",
      volume         = "0307",
      pages          = "001",
      doi            = "10.1088/1126-6708/2003/07/001",
      year           = "2003",
      eprint         = "hep-ph/0206293",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "CERN-TH-2002-129, FTN-T-2002-06",
      SLACcitation   = "%%CITATION = HEP-PH/0206293;%%",
}

@article{herwig,
      author         = "Corcella, G. and Knowles, I.G. and Marchesini, G. and
                        Moretti, S. and Odagiri, K. and others",
      title          = "{HERWIG 6: An Event generator for hadron emission
                        reactions with interfering gluons (including
                        supersymmetric processes)}",
      journal        = "JHEP",
      volume         = "0101",
      pages          = "010",
      doi            = "10.1088/1126-6708/2001/01/010",
      year           = "2001",
      eprint         = "hep-ph/0011363",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "CAVENDISH-HEP-99-03, CERN-TH-2000-284, RAL-TR-2000-048",
      SLACcitation   = "%%CITATION = HEP-PH/0011363;%%",
}

@misc{toppdf,
  title = {Top group pdf uncertainty recommendations},
  howpublished = {\url{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopPdfUncertainty}},
}

@misc{lhapdf,
  title = {Pdf uncertainty recommendations for the \lhc experiments},
  howpublished = {\url{http://www.hep.ucl.ac.uk/pdf4lhc/}},
}

@article{spinobspaper,
      author         = "Bernreuther, Werner and Heisler, Dennis and Si, Zong-Guo",
      title          = "{A set of top quark spin correlation and polarization
                        observables for the LHC: Standard Model predictions and
                        new physics contributions}",
      year           = "2015",
      eprint         = "1508.05271",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "TTK-15-16",
}
