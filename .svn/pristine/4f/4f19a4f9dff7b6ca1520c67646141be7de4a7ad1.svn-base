\BOOKMARK [1][-]{section.1}{Introduction}{}% 1
\BOOKMARK [1][-]{section.2}{Observables}{}% 2
\BOOKMARK [2][-]{subsection.2.1}{Spin density matrix and the set of observables}{section.2}% 3
\BOOKMARK [1][-]{section.3}{Monte Carlo}{}% 4
\BOOKMARK [2][-]{subsection.3.1}{Parton level}{section.3}% 5
\BOOKMARK [2][-]{subsection.3.2}{Stable-particle level}{section.3}% 6
\BOOKMARK [1][-]{section.4}{Selection}{}% 7
\BOOKMARK [2][-]{subsection.4.1}{Object Selection}{section.4}% 8
\BOOKMARK [2][-]{subsection.4.2}{Dilepton selection}{section.4}% 9
\BOOKMARK [3][-]{subsubsection.4.2.1}{Z+jets Background Estimation}{subsection.4.2}% 10
\BOOKMARK [3][-]{subsubsection.4.2.2}{Fake Background Estimation}{subsection.4.2}% 11
\BOOKMARK [3][-]{subsubsection.4.2.3}{Comparison of Data and Simulation}{subsection.4.2}% 12
\BOOKMARK [1][-]{section.5}{Reconstruction}{}% 13
\BOOKMARK [2][-]{subsection.5.1}{The kinematic reconstruction method}{section.5}% 14
\BOOKMARK [2][-]{subsection.5.2}{Comparison of Data and Simulation}{section.5}% 15
\BOOKMARK [1][-]{section.6}{Unfolding}{}% 16
\BOOKMARK [2][-]{subsection.6.1}{FBU}{section.6}% 17
\BOOKMARK [3][-]{subsubsection.6.1.1}{FBU Ingredients}{subsection.6.1}% 18
\BOOKMARK [3][-]{subsubsection.6.1.2}{Prior Choice}{subsection.6.1}% 19
\BOOKMARK [3][-]{subsubsection.6.1.3}{Marginalization}{subsection.6.1}% 20
\BOOKMARK [2][-]{subsection.6.2}{Parton level unfolding}{section.6}% 21
\BOOKMARK [3][-]{subsubsection.6.2.1}{Binning choice}{subsection.6.2}% 22
\BOOKMARK [2][-]{subsection.6.3}{Stable-particle level unfolding}{section.6}% 23
\BOOKMARK [1][-]{section.7}{Systematics}{}% 24
\BOOKMARK [2][-]{subsection.7.1}{Procedure used to assess systematic uncertainties}{section.7}% 25
\BOOKMARK [3][-]{subsubsection.7.1.1}{Signal Modelling Uncertainties}{subsection.7.1}% 26
\BOOKMARK [3][-]{subsubsection.7.1.2}{Detector Modelling Uncertainties}{subsection.7.1}% 27
\BOOKMARK [2][-]{subsection.7.2}{List of systematics considered}{section.7}% 28
\BOOKMARK [3][-]{subsubsection.7.2.1}{tbart modelling systematics}{subsection.7.2}% 29
\BOOKMARK [3][-]{subsubsection.7.2.2}{Background uncertainties}{subsection.7.2}% 30
\BOOKMARK [3][-]{subsubsection.7.2.3}{Detector modelling systematics}{subsection.7.2}% 31
\BOOKMARK [1][-]{section.8}{Results}{}% 32
\BOOKMARK [1][-]{section.9}{Conclusion}{}% 33
\BOOKMARK [1][-]{appendix.A}{Response matrix comparisons}{}% 34
\BOOKMARK [2][-]{subsection.A.1}{Charge comparison for polarization observables}{appendix.A}% 35
\BOOKMARK [2][-]{subsection.A.2}{Channel comparison for observables}{appendix.A}% 36
\BOOKMARK [1][-]{appendix.B}{Monte Carlo samples}{}% 37
\BOOKMARK [1][-]{appendix.C}{Further plots and tables}{}% 38
\BOOKMARK [2][-]{subsection.C.1}{Resolutions}{appendix.C}% 39
\BOOKMARK [3][-]{subsubsection.C.1.1}{Partonic}{subsection.C.1}% 40
\BOOKMARK [3][-]{subsubsection.C.1.2}{Stable particle}{subsection.C.1}% 41
\BOOKMARK [2][-]{subsection.C.2}{Binnings}{appendix.C}% 42
\BOOKMARK [2][-]{subsection.C.3}{Response matrices}{appendix.C}% 43
\BOOKMARK [3][-]{subsubsection.C.3.1}{Partonic}{subsection.C.3}% 44
\BOOKMARK [3][-]{subsubsection.C.3.2}{Stable-Particle}{subsection.C.3}% 45
\BOOKMARK [2][-]{subsection.C.4}{Acceptance efficiencies}{appendix.C}% 46
\BOOKMARK [3][-]{subsubsection.C.4.1}{Partonic}{subsection.C.4}% 47
\BOOKMARK [3][-]{subsubsection.C.4.2}{Stable-Particle}{subsection.C.4}% 48
\BOOKMARK [2][-]{subsection.C.5}{Unfolded distributions}{appendix.C}% 49
\BOOKMARK [3][-]{subsubsection.C.5.1}{Partonic}{subsection.C.5}% 50
\BOOKMARK [1][-]{appendix.D}{Fakes estimation}{}% 51
