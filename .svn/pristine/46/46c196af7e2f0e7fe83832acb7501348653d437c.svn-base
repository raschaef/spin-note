Major comments

    Demonstrate that the data/MC disagreement in the ee channel doesn’t show a significant shape difference compared to the other channels. For example, could plot for the normalized distributions data/MC and compare the 3 channels.
 (x)   Understand why unfolded particle level data is ~10% higher than prediction, even though at reco level the difference is smaller and the electron ID fit parameter is pulled up (which should reduce the measured cross-section).
    Is the MC still normalised to the NNLO+NNLL prediction in the results plots?
    Consider evaluating the correlations (perhaps statistical only) between the different observables. If possible could have a single p-value to quantify agreement with SM, but at least try to provide some information on the correlations.
    Add modelling uncertainties to particle level results
 (x)   Add pull tests to the note

Minor comments

    Write convincing argument why you are not sensitive to the ttbar normalisation uncertainty.
    Calibration curves:
 (x)   Add uncertainties to the plots / fits.
    Clarify what is done for non-closure (added as systematic?).
    Can we have plots summarising the offset and slope for each measured variable in the final binning? (i.e two plots, one for offset, one for slope, with a point per observable in each plot)
 x   Clarify in note that hadronization / parton shower uncertainty is included (Powheg+Herwig compared to Powheg+Pythia).
    Check the fraction of the selected particle-level events that have ==0 b-jets (where b-jet is identified with the standard TopFiducial b-jet ghost matching). These events will have a very low efficiency to be reconstructed, so we should check that the fraction is small.
    Z background normalisation:
    Are all the factors calculated with the MET > 30 GeV cut?
    How is the splitting between ‘+light flavour’ and ‘+HF’ done - just via the different MC samples?
    Add the MET distribution before and after the Z+jets scaling in the control region
    Check why in Figure 10 for ee and mumu there is no Z+bb contribution at low m(ll).
    Add DR/DS uncertainty for the Wt single top events.
    Table 4: The ttbar line only includes ttbar from real leptons (i.e. charge flips)?
    Fake uncertainties: Is it understood why changing the ttbar generator changes the ee fake yield so much? Since you have the data correction, shouldn’t you only be sensitive to the difference between OS/SS fakes between generators? i.e. if changing the MC generator increases the fakes in both OS and SS, then the normalisation to the SS data should cancel that effect.
    Top mass uncertainty: Usually we try to quote the impact on the measurement separately and not include it in the uncertainties. Any reason not to do that here?
    Section 7.2.1 explain better where you see an oscillation behaviour for the 10-bin configuration
    Fig 17: add ratio between non fiducial-fiducial events

Issues for the paper presentation

    For particle level results, should present either dsigma/dX or 1/sigma dsigma/dX.
    Consider whether to add the single number spin / polarisation observables at particle level.
    Consider whether to add full distributions at parton level.

Text fixes

    Table 5: Says both pre-tag and b-tag are calculated, but only one number is there. Please fix either caption or the table.
    ---> Fixed the caption.
    For kinematic reconstruction, make it clear you either take unsmeared if that works, or take first smeared result.
    ---> Added a short description at the end of "the kinematic reconstruction method" section.
    Equation 13 (reweighting): C and P terms not defined.
    ---> Defined them now.
    Fig 17 (non-fiducial): Does this show the 3 channels (ee, mumu & emu)? Make more clear in caption.
    ##### Have to look at that later
    Systematics bootstrapping (8.1.2): Nicely explained, but at the end of this section please mention the criterium used to decide if a systematic is considered or not.
    ---> No/A loose criterium is used for the consideration of a systematic.
    MC generator systematic: Alpgen is mentioned, but is not used (which is fine) - please fix the text.
    ---> Fixed by removing Alpgen.
    ISR/FSR: Note says Alpgen is used, but in approval talk you said Powheg was used.
    ---> Fixed by updating the prescription. The powheg samples with the changes in the scale and hdamp parameter are used.
    Presumably these are particle level results (caption says parton)
    ###### need to have a look where this is
    156: typo ‘mus’ -> ‘must'
    ---> Fixed
    166: Broken equation reference
    ---> Fixed
    436: 'signal region consists of the same cuts’ should be ‘control region consists of the same cuts’?
    ---> That's correct. Fixed it to 'control region'
    Table 8 should be referenced from section 7.2.1
    ---> The same label was taken for the parton and particle tables. Has been fixed now.
    676: ‘on axis’ -> ‘one axis'
    ---> Fixed.
    688: Broken table reference
    ---> Fixed.
    700: ‘prescriptions’ -> ‘recommendations'
    ---> Changed.
    802: theta is not defined
    ##### Will check that later
