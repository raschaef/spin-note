\section{Results}
\label{sec:results}

The results after correcting the reconstructed distributions of
the observables back to parton and stable-particle level are
presented. On parton level, values for the underlying spin parameter
of each observable is presented while on stable-particle, the unfolded
distributions of each observable are presented. The uncertainties
include the statistical and systematic components after applying the
marginalization procedure within the unfolding. 

\subsection{Parton measurement}
Unfolding a data set corresponding to 20.3 fb$^{-1}$, the values
obtained for the unfolding of each observable back to parton level are
shown in table~\ref{tab:finalpartonresults}. The first column of values
shows the predicted values taken from section~\ref{sec:parton_def}. The last column
shows the unfolded values with uncertainties coming from the
marginalisation procedure and from signal modelling. All polarization
observables are in agreement with the standard model, whereas the spin
correlation in the helicity (C(k,k)) and transverse (C(n,n)) axis  are
a bit higher than
the calculations for the SM, but still well within one standard
deviations. A different case can be seen for the spin correlation in
the r-axis (C(r,r)), which unfolded value of \ResultRcorr $\pm$
\UncertMargeRcorr $\pm$ \UncertModRcorr~is significantly higher than
the predicted value of $0.055 \pm 0.009 \pm 0.03$. The difference is about
two sigma. The cross-correlation observables are
also well in agreement with the SM, esppecially the R-Hel sum observable,
which is the only observable significantly different from zero. The
only exception for a slight disagreement with the predicted values for
the cross correlations is the Trans-Hel sum
observable, which differs by a bit more than one standard
deviations. Plots of the corresponding unfolded distributions %, the posterior distribution
as well as the fit of the nuisance parameters
can be found in appendix~\ref{app:moreplots}. A visual summary of all
results is given in \Cref{fig:results_parton_pol,fig:results_parton_corr,fig:results_parton_crosscorr}.
\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.7\textwidth]{figures/unfolding/results/pol_result_summary.pdf}
   \caption{Results of the polarization observables for the unfolding
     back to Powheg parton level. The statistical + detector-related uncertainties are shown
   in red, signal modelling systematics in blue. The results are
   compared to predictions taken from~\cite{spinobspaper}.} 
   \label{fig:results_parton_pol}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.7\textwidth]{figures/unfolding/results/corr_result_summary.pdf}
   \caption{Results of the spin correlation observables for the unfolding
     back to Powheg parton level. The statistical + detector-related uncertainties are shown
   in red, signal modelling systematics in blue. The results are
   compared to predictions taken from~\cite{spinobspaper}.} 
   \label{fig:results_parton_corr}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.7\textwidth]{figures/unfolding/results/crosscorr_result_summary.pdf}
   \caption{Results of the cross correlation observables for the unfolding
     back to Powheg parton level. The statistical + detector-related uncertainties are shown
   in red, signal modelling systematics in blue. The results are
   compared to predictions taken from~\cite{spinobspaper}.} 
   \label{fig:results_parton_crosscorr}
 \end{center}
\end{figure}


%\begin{eqnarray}
% P^{+}_{\text{hel}} = xxx \pm 0.024, \quad P^{-}_{\text{hel}} = xxx \pm 0.024, \nonumber \\
% P^{+}_{\text{trans}} = xxx \pm 0.020, \quad P^{-}_{\text{trans}} = xxx \pm 0.019, \nonumber \\
% P^{+}_{\text{r}} = xxx \pm 0.023, \quad P^{-}_{\text{r}} = xxx \pm 0.023, \nonumber \\
%C_{\text{hel}} = xxx \pm 0.046, \quad C_{\text{trans}} = xxx \pm 0.025, \quad C_{\text{r}}
%  = xxx \pm 0.050 \nonumber \\
%C(n,k) + C(k,n) = xxx \pm 0.060, \quad C(n,k) - C(k,n) = xxx \pm 0.045
 % \nonumber \\
%C(n,r) + C(r,n) = xxx \pm 0.059, \quad C(n,r) - C(r,n) = xxx \pm 0.063
%  \nonumber \\ 
%C(r,k) + C(k,r) = xxx \pm 0.071, \quad C(n,k) - C(k,n) = xxx \pm 0.077
%\end{eqnarray}


\begin{table}
  \begin{center}
    \begin{tabular}{r|c|c}
 %     \hline
      Observable & val$_{\text{th.}}$ & val$_{\text{meas}} \pm$
      (marg.) $\pm$
      (mod. syst) \\
      \hline
      helpol (+) & $0.0030 \pm 0.001$ &\ResultHelpolPlus
      $\pm$ \UncertMargeHelpolPlus $\pm$ \UncertModHelpolPlus \\\
      helpol (-) & $0.0034 \pm 0.001$ &  \ResultHelpolMinus
      $\pm$ \UncertMargeHelpolMinus $\pm$ \UncertModHelpolMinus \\
      transpol (+) & $0.0035 \pm0.0004$ & \ResultTranspolPlus
      $\pm$ \UncertMargeTranspolPlus $\pm$ \UncertModTranspolPlus \\
      transpol (-) & $0.0035 \pm0.0004$ & \ResultTranspolMinus
      $\pm$ \UncertMargeTranspolMinus $\pm$ \UncertModTranspolMinus \\
      rpol (+) & $0.0013 \pm 0.001 $ & \ResultRpolPlus
      $\pm$ \UncertMargeRpolPlus $\pm$ \UncertModRpolPlus \\
      rpol (-) & $0.0015 \pm 0.001$ & \ResultRpolMinus
      $\pm$ \UncertMargeRpolMinus $\pm$ \UncertModRpolMinus \\
      HelCorr & $0.318 \pm 0.005 \pm0.017$ & \ResultHelcorr
      $\pm$ \UncertMargeHelcorr $\pm$ \UncertModHelcorr \\
      TransCorr & $0.332 \pm 0.004 \pm0.002$ &  \ResultTranscorr
      $\pm$ \UncertMargeTranscorr $\pm$ \UncertModTranscorr\\
      RCorr & $0.055 \pm 0.009 \pm0.03$ & \ResultRcorr
      $\pm$ \UncertMargeRcorr $\pm$ \UncertModRcorr \\
      RHelSum & $-0.251 \pm -0.015 $ & \ResultRhelsum
      $\pm$ \UncertMargeRhelsum $\pm$ \UncertModRhelsum \\
      RHelDiff & $ 0.001 \pm 0.015$ & \ResultRheldiff
      $\pm$ \UncertMargeRheldiff $\pm$ \UncertModRheldiff \\
      TransRSum & $0$ & \ResultTransrsum
      $\pm$ \UncertMargeTransrsum $\pm$ \UncertModTransrsum \\
      TransRDiff & $0$ &  \ResultTransrdiff
      $\pm$ \UncertMargeTransrdiff $\pm$ \UncertModTransrdiff\\
      TransHelSum & $0$ & \ResultTranshelsum
      $\pm$ \UncertMargeTranshelsum $\pm$ \UncertModTranshelsum \\
      TransHelDiff & $0$ & \ResultTransheldiff
      $\pm$ \UncertMargeTransheldiff $\pm$ \UncertModTransheldiff \\
%      \hline
      \end{tabular}
      \caption{Results of the observables after the unfolding back to
        Monte Carlo parton level. The uncertainties are broken down
        into the uncertainty coming from the marginalisation procedure
        and signal modelling systematics. Each value is compared to the predicted values
        shown in sec.~\ref{sec:parton_def}. }
   \label{tab:finalpartonresults}
    \end{center}
\end{table}

\subsection{Stable-particle measurement}

With the same data set as for the partonic measurement, the unfolding
is performed back to stable-particle level. The unfolded distributions
are presented in
\Cref{fig:unfolded_data_particle1,fig:unfolded_data_particle2,fig:unfolded_data_particle3}. On
the contrary to the parton level measurement, the normalisation
difference between data and expectation is not resolved by the
marginalization procedure. All unfolded data distribution are
approximately 10 percent above the truth stable particle distributions
created with the Powheg signal sample. Most of the distributions don't
show a significantly different shape than the expectation from
MC. However, some like the helicity correlation show a strong
fluctuating behaviour between neighbouring bins. In order to compare
the results to the partonic measurement, summary plots of all
observables are shown in \cref{fig:results_particle_pol,fig:results_particle_corr,fig:results_particle_crosscorr}. As no polarization or spin
correlation is defined for the pseudo-tops on stable-particle level,
the mean of the correlation distributions is multiplied with a factor
of $-9$ and the mean of the polarization distributions with a factor
of $3$, which corresponds to the spin correlation and polarization,
respectively, on parton level (without modelling systematics at the moment).

\begin{figure}[htbp]
	\centering
        \subfloat[\helpolplus]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/helpol_plus_data_4Bin4_particle_nuisancefit_note.png}}
%        \qquad
        \subfloat[\helpolminus]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/helpol_minus_data_4Bin4_particle_nuisancefit_note.png}}\\
        \subfloat[\transpolplus]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/transpol_plus_data_2Bin_particle_nuisancefit_note.png}}
%        \qquad
        \subfloat[\transpolminus]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/transpol_minus_data_2Bin_particle_nuisancefit_note.png}}\\
        \subfloat[\rpolplus]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/rpol_plus_data_6Bin2_particle_nuisancefit_note.png}}
%        \qquad
        \subfloat[\rpolminus]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/rpol_minus_data_6Bin2_particle_nuisancefit_note.png}}\\
        \caption{Polarization distributions corrected back to stable particle
        level and compared to the truth distribution from the signal
        MC.}
        \label{fig:unfolded_data_particle1}
\end{figure}

\begin{figure}[htbp]
	\centering
        \subfloat[\helcorr]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/helcorr_data_8Bin3_particle_nuisancefit_note.png}}
%        \qquad
        \subfloat[\transcorr]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/transcorr_data_8Bin_particle_nuisancefit_note.png}}\\
        \subfloat[\rcorr]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/rcorr_data_8Bin_particle_nuisancefit_note.png}}
        \caption{Correlation distributions corrected back to stable particle
        level and compared to the truth distribution from the signal
        MC.}
        \label{fig:unfolded_data_particle2}
\end{figure}

\begin{figure}[htbp]
	\centering
        \subfloat[\transhelsum]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/transhelsum_data_6Bin3_particle_nuisancefit_note.png}}
%        \qquad
        \subfloat[\transheldiff]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/transheldiff_data_6Bin_particle_nuisancefit_note.png}}\\
        \subfloat[\transrsum]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/transrsum_data_6Bin_particle_nuisancefit_note.png}}
%        \qquad
        \subfloat[\transrdiff]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/transrdiff_data_6Bin_particle_nuisancefit_note.png}}\\
        \subfloat[\rhelsum]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/rhelsum_data_6Bin4_particle_nuisancefit_note.png}}
%        \qquad
        \subfloat[\rheldiff]{\includegraphics[width=0.45\textwidth]{unfolding/unfolded/rheldiff_data_6Bin4_particle_nuisancefit_note.png}}\\
        \caption{Cross correlation distributions corrected back to stable particle
        level and compared to the truth distribution from the signal
        MC.}
        \label{fig:unfolded_data_particle3}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.7\textwidth]{figures/unfolding/results/pol_particle_result_summary.pdf}
   \caption{Results of the polarization observables for the unfolding
     back to Powheg parton level. The statistical + detector-related uncertainties are shown
   in red, signal modelling systematics in blue. The results are
   compared to predictions from Powheg+Pythia.} 
   \label{fig:results_particle_pol}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.7\textwidth]{figures/unfolding/results/corr_particle_result_summary.pdf}
   \caption{Results of the spin correlation observables for the unfolding
     back to Powheg parton level. The statistical + detector-related uncertainties are shown
   in red, signal modelling systematics in blue. The results are
   compared to predictions from Powheg+Pythia.} 
   \label{fig:results_particle_corr}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.7\textwidth]{figures/unfolding/results/crosscorr_particle_result_summary.pdf}
   \caption{Results of the cross correlation observables for the unfolding
     back to Powheg parton level. The statistical + detector-related uncertainties are shown
   in red, signal modelling systematics in blue. The results are
   compared to predictions from Powheg+Pythia.} 
   \label{fig:results_particle_crosscorr}
 \end{center}
\end{figure}