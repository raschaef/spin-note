General:
* can you refer in your ChangeLog to the version of the note that you changed it for and give the note a new version number if you make a significant change? Then it \
will be easier for us to keep track of what changed where :)
* you are using quite often text in math mode (for example in line 630 "obs" or in table 11) but in general often across the note. Can you use \mathrm{obs} etc instea\
d to make the text more readable?

Abstract:
* are the uncertainties shown stat and syst? Why does the systematic uncertainty have less precision?

* l 150: mus -> must
* caption for table1: what is the meaning of the "=" in the caption? I guess this is simply a typo, right?
* l 160: broken reference for equation
* Table 2 or text before: could you indicate which observables have not yet been measured at all?
* Chapter 4: which samples are used for the estimation of the systematic uncertainties? It might be useful to already list them here. Which ttbar sample did you use f\
or the signal, the one with hdamp or the one with DSID 117050?
* line 205: typo in desireable
* line 213 following: this is only true for Pythia6, what did you use in case of samples showered with Herwig?
* line 217: what do you mean exactly with "errors from the calculation"?
* could you indicate in the plots Figure 1,2 and following that this is parton level and Fig 4/5 following that its particle level?
* Fig. 1b and 4b: could you zoom out here a bit in the ratio plot to see how large the difference is in the first bin? are the uncertainties in these distributions
shown and very small or not shown? At least in the ratio plots that might be useful to see.
* line 228 and following: could you explain this generator dependence already in the MC chapter? That would make these first few lines a bit clearer, at the moment ev\
erything reads a bit vague.
* line 242 and following: I do not fully understand what you mean here with "clustered",  do you mean you are using "dressed" electrons, aka added the photons within \
a dR of 0.1?
* line 248: you mean the parent of the muon, right?
* line 254: What do you mean here with "rescaled"?
* line 267: do you have a reference for the pseudo-top definitions?
* line 273 and 279 following: could you fix the font for the subscripts (use roman font) so this is better readable?
* line 292: please mention that the JVF cut is only applied to jets with pt < 50 GeV and |eta| < 2.4.
* line 293: I think its not any jet, only the closest jet if it is within DeltaR < 0.2.
* line 310: calorimetry -> calorimeter
* line 307 following: are you not using the mini-isolation criterion?
* page 18 (between lines 389 and 390): could you make sure that words like "pretag, others , all" etc are written in roman font, and maybe write scale factors in capi\
tal letters? That would make it easier to read this equation system.
* could you indicate on the figures in 7,8,9 etc if they are for the signal region or a special control region?
* line 467: space missing before "and fakes".
* line 497: MT2 is not clear here, could you add one or two sentences about the method?
* figures 12,13,14: what does "full uncertainty" include? Is this stat only, stat+norm or also all systematics?
* line 550: consists OF
* line 578: do you have a reference for the Tikhonov regularization functions?
* figure 15 a/b: the binning labels look a bit arbitrary here, why do they go from -1-5 for (a) and from 0 to 24 for (b)? in (b) the number only describes the bin num\
ber right?
* formula 13: what is P_{rew} ?

* line 689: reference is broken
* line 719/20: phrasing is a bit confusing, maybe better: "The bin content of the nominal distribution is varied within poissonian uncertainties to build a pseudo-dat\
aset."
* line 739: what do you mean with "different levels of ISR/FSR"? Are these alpha_S variation of 0.5 and 2.0?
* line 747 following: what is taken as final PDF uncertainty?
* line 801 following: do you take the eigenvectors provided?
* I do not see the systematics for the missing ET described anywhere, could you add them?
* do you have an uncertainty for your reconstruction method, and for the limited MC statistics?
* did you check the impact of the JES bug?
* What systematic uncertainty do you apply for your fake contribution?

Figure 20: can you add a red line at 0 to guide the eye? It would also help if this plot were bigger, the labels are quite hard to read

Figures 24a-d and following: there seems to be a larger normalisation issues for Pk and Pn which is not yet understood, right? I can see the uncertainty on the data, \
but how large are the stat+syst uncertainties on the MC?

Figures 21-23 and 27-29: I assume those are plots you would want to use in the paper, right? I would change the colour and/or the size of the Powheg+Py6 label to make\
 it better visible in the plot, at the moment its quite hard to see when partly overlapping with the red filled circle.
p. 51-53: how do your results compare to previous results in case the coefficients have been measured before?

* Which systematic uncertainties have the largest effect in this analysis?
