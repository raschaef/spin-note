\section{Reconstruction}
\label{sec:reco}

Reconstructing the $t\bar{t}$ kinematics in the dilepton channel is
challenging due to the two neutrinos in the final state, making
the system that relates all the particles' momenta underconstrained. Hence, assumptions are mandatory
to perform the $t\bar{t}$ reconstruction in this channel.

 Considering the process $pp \rightarrow t \bar{t} + X \rightarrow 
 W^{+}b W^{-}\bar{b} + X \rightarrow l^{+}\nu b l^{-}\bar{\nu}\bar{b}
 + X$, the following kinematic constraints can be set:\\ 

\[
\begin{array}{lll}
{p}_{b} + {p}_{W^{+}} & = &{p}_{t},\\ 
{p}_{\bar{b}} + {p}_{W^{-}} & = &{p}_{\bar{t}},\\
{p}_{l^{+}} + {p}_{\nu} & = & {p}_{W^{+}},\\
{p}_{l^{-}} + {p}_{\bar{\nu}} & = & {p}_{W^{-}},\\
 p_{\nu_{x}} + p_{\bar{\nu}_{x}} & = & E_{x}^{miss},\\
 p_{\nu_{y}} + p_{\bar{\nu}_{y}} & = & E_{y}^{miss}.\\
\end{array}
\]    

   
The above set of equations has one constraint fewer than the number of
measured variables. In order to solve this underconstrained system,
certain assumptions have to be made.  
For instance, by specifying a value for the top quark mass, this set
of equations can be solved and the four vectors of $t$ and $\bar{t}$ be determined. 

There are two types of ambiguities which still need to be resolved. First,
the set of equations is equivalent to a polynomial of the 4-th order
in one selected variable. Therefore, there can be up to four real
solutions. Second, even in the simplest case of two leptons and just
two jets in the final state, there is a twofold ambiguity to assign
the lepton and $b$-jet to the proper top quark. Furthermore, one has
to address the possibility of having more  than two jets in the final
state which increases this ambiguity even more. Finally, one also
needs to address the fact that the measured quantities entering the
equations are subject to experimental uncertainties. 

Four reconstruction methods have been tested in order to reconstruct the
top (anti-top) four-momenta: the kinematic method (KIN)~\cite{Abulencia:2006js,Aaltonen:2012tk} where the
system of equations is numerically solved, the matrix element method
(ME)~\cite{Abazov:2004cs,ATLAS-CONF-2012-057} where the probability distribution is calculated based on $gg
\rightarrow  t\bar{t}$ leading-order matrix elements, the neutrino
weighting method (NW)~\cite{Abbott:1997fv,Borroni:1536502} where a
sampling of the rapidity of the two
neutrinos is performed and a \mttwo-based reconstruction method
(MT2)~\cite{mt2reco} that uses the \mttwo~value of the event to get
the transverse momentum of the two neutrinos. In all the methods, we assume  $m_{W} = 80.4 \ {\rm  GeV}$ and
$m_{\nu} = 0$. 

The details of the studies are documented in the supporting
note~\cite{KinRecoNote}. According to these studies, both KIN and NW
methods show very similar performance, slightly better than the other
two methods (ME and MT2).
However, since there seems to be a bias in reconstructed asymmetry
for the NW method (not present for the KIN method) and NW shows also
some instability  with respect to changing its internal parameters,
we decided to use  the KIN method for the kinematic reconstruction of
$t\bar{t}$ events in this analysis. Below, we
summarize the basic 
assumptions of reconstruction methods, give a more detailed
description of KIN method, and present the data/MC comparison for
 variables related to $t\bar{t}$ kinematic reconstruction.



 

\subsection{The kinematic reconstruction method}

The KIN  method was used  in the past for top mass reconstruction and
other studies at CDF~\cite{Abulencia:2006js,Aaltonen:2012tk} . A brief
description  of this method is given in the following.  

Assuming a value for the top quark mass ($m_t = 172.5 $ GeV), the system 
is constrained and can be solved. In order to find the solution of the
kinematic equations the numerical Newton-Raphson
method~\cite{NumerRecip} is used. 
 
In case there are more than two reconstructed jets in a given event,
the two jets  with the highest value of b-tagging weight (as determined by the
MV1 $b$-tagging algorithm~\cite{ATLAS:2011qia}) are used. This improves
the efficiency of choosing the correct jets, in comparison to just
choosing the two jets with the highest transverse energy ($E_{T})$,
from about 54\% to about 69\%. 

The experimental uncertainties on the measured objects are taken into
account by sampling the phase-space of the measured jets and $\MET$
($N_{smear}$ trials) according to their resolution. For each point in
this space the procedure described below is applied to find a solution
 for $t$ and $\bar{t}$ four-vectors.  
The resolution functions are determined in a similar way as the
transfer functions used in KL fitter~\cite{Erdmann:2013rxa}, i.e. the 
resolution functions as a function of the jet $p_T$ (for jets) and the
total transverse energy in the event (for $\MET$) are produced. 

From the four kinematically possible solutions, the KIN method takes the
solution which leads to the lowest effective mass of the $t\bar{t}$
system.   
The reason for this is that the partonic $t\bar{t}$ cross-section  is a
decreasing function of the partonic energy $\hat{s}\simeq
m(t\bar{t})$ (except very close to the $m(t\bar{t})$ boundary),  therefore,
it is more likely to have events with smaller $m_{t\bar{t}}$.    
The numerical algorithm to find the solution actually stops after
finding two solutions. This is mostly due to efficiency reasons, since
it is known that most of the time there are two solutions and the probability
that there exist four real solutions is very small (a few percent). In
consequence, we would gain very little and the effectiveness of the
method would be decreased (longer processing time). 

The twofold ambiguity in the lepton and $b$-jet assignment is resolved
in the following way. 
For a given lepton-jet pairing (lep1-jet1, lep2-jet2), the
 kinematic solution with the lowest $m(t\bar{t})$ is taken, as
 described above.  The smearing is performed and the reconstruction of
 the $t\bar{t}$ kinematics  for a given pairing is tried $N_{\text{smear}}$
 times. There could be no solution  found given the ``smeared'' jets
 and $\MET$. Therefore, for a given event, it is possible to end up
 with  less reconstructed trials ($N_{\text{smear}}^{\text{reco}}$) than what was
 originally tried  ($N_{\text{smear}}$). For the second lepton-jet pairing
 possibility (lep1-jet2, lep2-jet1), the previously described
 procedure is repeated and a different number of reconstructed trials
 is obtained. The correct pairing is chosen to be the one that has
 more reconstructed trials, i.e. the one that maximizes
 $N_{\text{smear}}^{\text{reco}}/N_{\text{smear}}$.  


\subsection{Comparison of Data and Simulation}
We compare the data and expectations after the full dilepton selection and
$t\bar{t}$ kinematic reconstruction performed by the KIN method.

Figures~\ref{fig:dataMC1reco} to~\ref{fig:dataMC3reco} show the
data/prediction comparisons after the final selection. The shaded area shows the
uncertainty on the signal and background normalization as for the
selection plots.
 Figure~\ref{fig:dataMC1reco} shows the top/antitop $p_T$ and $\eta$,
 figure~\ref{fig:dataMC2reco} the $t\bar{t}$ pair
 $p_T$ and $m_{t\bar{t}}$ and
 figure~\ref{fig:dataMC3reco} transverse-/r-axis $\phi$ and $\eta$
 distributions for all three individual
 dilepton channels.  
The overall agreement between Monte Carlo prediction and data is
good. 

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_top_pt_ratio.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_antitop_pt_ratio.pdf} \\
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_top_eta_ratio.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_antitop_eta_ratio.pdf}
%    \includegraphics[scale=0.25]{figures/reco/partonic_tbar_pt_reco.eps}
%    \includegraphics[scale=0.25]{figures/reco/partonic_tbar_pt_reco.eps}
    \caption{Comparison of the top/antitop $p_{\text{T}}$ and $\eta$ distributions between data and predictions
      after the kinematic reconstruction in the inclusive dilepton channel. The data/expectation ratio is also
      shown. The shaded area shows the full uncertainty on signal and
      background. } 
    \label{fig:dataMC1reco}
  \end{center}
\end{figure}

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_ttbar_pt_ratio.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_ttbar_m_ratio.pdf}
    \caption{Comparison of the \ttbar $p_{\text{T}}$ and mass distributions between data and predictions
      after the kinematic reconstruction in the inclusive dilepton channel. The data/expectation ratio is also
      shown. The shaded area shows the full uncertainty on signal and
      background. } 
    \label{fig:dataMC2reco}
  \end{center}
\end{figure}

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_trans_phi_ratio.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_r_phi_ratio.pdf} \\
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_trans_eta_ratio.pdf}
    \includegraphics[width=0.45\textwidth]{figures/reco/partonic_r_eta_ratio.pdf}
    \caption{Comparison of the transverse-/r-axis $\phi$ and $\eta$ distributions between data and predictions
      after the kinematic reconstruction in the inclusive dilepton channel. The data/expectation ratio is also
      shown. The shaded area shows the full uncertainty on signal and
      background. } 
    \label{fig:dataMC3reco}
  \end{center}
\end{figure}

%\begin{figure}[!htbp]
% \begin{center}
% \includegraphics[scale=0.25]{figures/reco/ee_top_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_top_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_top_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/ee_tbar_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_tbar_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_tbar_pt_reco.eps}
 %  \caption{Comparisons of top/antitop $p_T$, $t\bar{t}$ distributions between data and predictions
 %    after kinematic recontruction in the $ee$ (left), $e\mu$ (middle)
 %    and $\mu\mu$   channels (right). The data/expectation ratio is also
 %    shown. The shaded area shows the uncertainty on the signal and
 %    background normalization. } 
%   \label{fig:dataMC1reco}
% \end{center}
%\end{figure}

%\begin{figure}[!htbp]
% \begin{center}
% \includegraphics[scale=0.25]{figures/reco/ee_ttbar_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_ttbar_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_ttbar_pt_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/ee_ttbar_m_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_ttbar_m_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_ttbar_m_reco.eps}

%   \caption{Comparisons $t\bar{t}$ pair $p_T$,
 %    and $m_{t\bar{t}}$ distributions between data and predictions
  %   after kinematic recontruction in the $ee$ (left), $e\mu$ (middle)
 %    and $\mu\mu$   channels (right). The data/expectation ratio is also
 %    shown. The shaded area shows the uncertainty on the signal and
 %    background normalization. } 
 %  \label{fig:dataMC2reco}
% \end{center}
%\end{figure}


%\begin{figure}[!htbp]
% \begin{center}
% \includegraphics[scale=0.25]{figures/reco/ee_top_y_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_top_y_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_top_y_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/ee_tbar_y_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/emu_tbar_y_reco.eps}
% \includegraphics[scale=0.25]{figures/reco/mumu_tbar_y_reco.eps}
%   \caption{Comparisons of top and antitop rapidity distribution
%     between data and predictions after event 
%     selection in the $ee$ (left), $e\mu$ (middle) and $\mu\mu$
%     channels (right). The data/expectation ratio is also shown.  The shaded
%     area shows the uncertainty on the signal and background
%     normalization. } 
%   \label{fig:dataMC3reco}
% \end{center}
%\end{figure}

% \begin{figure}[!htbp]
%  \begin{center}
%    \includegraphics[scale=0.25]{figures/reco/ee_delta_y_reco.eps}
%    \includegraphics[scale=0.25]{figures/reco/emu_delta_y_reco.eps}
%    \includegraphics[scale=0.25]{figures/reco/mumu_delta_y_reco.eps}
%    \caption{Distributions of the $\Delta |y|$ variable in data and
%      MC after event selection in the $ee$ (left), $e\mu$ (middle) and
%      $\mu\mu$ channels (right). The data/MC ratio is also shown.  The
%      shaded area shows the uncertainty on the signal and background
%      normalization. } 
%    \label{fig:dataMC3reco}
%  \end{center}
% \end{figure}
