\providecommand{\href}[2]{#2}\begingroup\raggedright\begin{thebibliography}{10}

\bibitem{topdisc1}
C.~Collaboration, {\em Observation of Top Quark Production in $\bar{p}p$
  Collisions with the Collider Detector at Fermilab}, Physical Review Letters
  {\bfseries 74} no.~14, (1995) 2626--2631.

\bibitem{topdisc2}
D.~Collaboration, {\em Search for High Mass Top Quark Production in $p \bar{p}$
  Collisions at $\sqrt{s} = 1.8 \text{ TeV}$}, Physical Review Letters
  {\bfseries 74} no.~13, (1995) 2422--2426.

\bibitem{cdf_asym_early}
{CDF} Collaboration, T.~Aaltonen {et~al.}, {\em {Measurement of the top quark
  forward-backward production asymmetry and its dependence on event kinematic
  properties}}, \href{http://dx.doi.org/10.1103/PhysRevD.87.092002}{Phys.Rev.
  {\bfseries D87} no.~9, (2013) 092002},
\href{http://arxiv.org/abs/1211.1003}{{\ttfamily arXiv:1211.1003 [hep-ex]}}.
%%CITATION = ARXIV:1211.1003;%%.

\bibitem{dzero_asym_early}
{D0} Collaboration, V.~M. Abazov {et~al.}, {\em {Measurement of Leptonic
  Asymmetries and Top Quark Polarization in $t\bar{t}$ Production}},
  \href{http://dx.doi.org/10.1103/PhysRevD.87.011103}{Phys.Rev. {\bfseries D87}
  no.~1, (2013) 011103},
\href{http://arxiv.org/abs/1207.0364}{{\ttfamily arXiv:1207.0364 [hep-ex]}}.
%%CITATION = ARXIV:1207.0364;%%.

\bibitem{cdf_asym_new}
{CDF} Collaboration, T.~Aaltonen {et~al.}, {\em {Measurement of the top quark
  forward-backward production asymmetry and its dependence on event kinematic
  properties}}, \href{http://dx.doi.org/10.1103/PhysRevD.87.092002}{Phys.Rev.
  {\bfseries D87} no.~9, (2013) 092002},
\href{http://arxiv.org/abs/1211.1003}{{\ttfamily arXiv:1211.1003 [hep-ex]}}.
%%CITATION = ARXIV:1211.1003;%%.

\bibitem{dzero_asym_new}
{D0} Collaboration, V.~M. Abazov {et~al.}, {\em {Measurement of the
  forward-backward asymmetry in top quark-antiquark production in ppbar
  collisions using the lepton+jets channel}},
  \href{http://dx.doi.org/10.1103/PhysRevD.90.072011}{Phys.Rev. {\bfseries D90}
  no.~7, (2014) 072011},
\href{http://arxiv.org/abs/1405.0421}{{\ttfamily arXiv:1405.0421 [hep-ex]}}.
%%CITATION = ARXIV:1405.0421;%%.

\bibitem{theo_asym_new}
M.~Czakon, P.~Fiedler,  and A.~Mitov, {\em {Resolving the Tevatron top quark
  forward-backward asymmetry puzzle}},
\href{http://arxiv.org/abs/1411.3007}{{\ttfamily arXiv:1411.3007 [hep-ph]}}.
%%CITATION = ARXIV:1411.3007;%%.

\bibitem{pseudotop}
``Pseudo top quark definition for \atlas and \cms.''
  \url{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/ParticleLevelTopDefinitions}.

\bibitem{spin_anapow_lep}
A.~Czarnecki, M.~Jezabek,  and J.~H. Kuhn, {\em {Lepton Spectra From Decays of
  Polarized Top Quarks}},
\href{http://dx.doi.org/10.1016/0550-3213(91)90082-9}{Nucl.Phys. {\bfseries
  B351} (1991) 70--80}.
%%CITATION = NUPHA,B351,70;%%.

\bibitem{atlas_pol}
{ATLAS Collaboration} Collaboration,, {\em {Measurement of Top Quark
  Polarization in Top-Antitop Events from Proton-Proton Collisions at
  $\sqrt{s}=7$ TeV Using the ATLAS Detector}},
  \href{http://dx.doi.org/10.1103/PhysRevLett.111.232002}{Phys.Rev.Lett.
  {\bfseries 111} no.~23, (2013) 232002},
\href{http://arxiv.org/abs/1307.6511}{{\ttfamily arXiv:1307.6511 [hep-ex]}}.
%%CITATION = ARXIV:1307.6511;%%.

\bibitem{atlas_spincorr}
{ATLAS Collaboration} Collaboration,, {\em {Measurements of spin correlation in
  top-antitop quark events from proton-proton collisions at $\sqrt{s}=7$ TeV
  using the ATLAS detector}},
  \href{http://dx.doi.org/10.1103/PhysRevD.90.112016}{Phys.Rev. {\bfseries D90}
  no.~11, (2014) 112016},
\href{http://arxiv.org/abs/1407.4314}{{\ttfamily arXiv:1407.4314 [hep-ex]}}.
%%CITATION = ARXIV:1407.4314;%%.

\bibitem{cms_spinpol}
{CMS Collaboration} Collaboration,, {\em {Measurements of $t\bar{t}$ spin
  correlations and top-quark polarization using dilepton final states in $pp$
  collisions at $\sqrt{s}$ = 7 TeV}},
  \href{http://dx.doi.org/10.1103/PhysRevLett.112.182001}{Phys.Rev.Lett.
  {\bfseries 112} no.~18, (2014) 182001},
\href{http://arxiv.org/abs/1311.3924}{{\ttfamily arXiv:1311.3924 [hep-ex]}}.
%%CITATION = ARXIV:1311.3924;%%.

\bibitem{bernreuther_8tev}
W.~Bernreuther and Z.-G. Si, {\em {Top quark spin correlations and polarization
  at the LHC: standard model predictions and effects of anomalous top chromo
  moments}}, \href{http://dx.doi.org/10.1016/j.physletb.2013.06.051,
  10.1016/j.physletb.2015.03.035}{Phys.Lett. {\bfseries B725} (2013) 115--122},
\href{http://arxiv.org/abs/1305.2066}{{\ttfamily arXiv:1305.2066 [hep-ph]}}.
%%CITATION = ARXIV:1305.2066;%%.

\bibitem{baumgart_transpol}
M.~Baumgart and B.~Tweedie, {\em {Transverse Top Quark Polarization and the
  ttbar Forward-Backward Asymmetry}},
  \href{http://dx.doi.org/10.1007/JHEP08(2013)072}{JHEP {\bfseries 1308} (2013)
  072},
\href{http://arxiv.org/abs/1303.1200}{{\ttfamily arXiv:1303.1200 [hep-ph]}}.
%%CITATION = ARXIV:1303.1200;%%.

\bibitem{powhegbox}
S.~Frixione, P.~Nason,  and C.~Oleari, {\em {Matching NLO QCD computations with
  Parton Shower simulations: the POWHEG method}},
  \href{http://dx.doi.org/10.1088/1126-6708/2007/11/070}{JHEP {\bfseries 0711}
  (2007) 070},
\href{http://arxiv.org/abs/0709.2092}{{\ttfamily arXiv:0709.2092 [hep-ph]}}.
%%CITATION = ARXIV:0709.2092;%%.

\bibitem{pythia6}
T.~Sjostrand, S.~Mrenna,  and P.~Z. Skands, {\em {PYTHIA 6.4 Physics and
  Manual}}, \href{http://dx.doi.org/10.1088/1126-6708/2006/05/026}{JHEP
  {\bfseries 0605} (2006) 026},
\href{http://arxiv.org/abs/hep-ph/0603175}{{\ttfamily arXiv:hep-ph/0603175
  [hep-ph]}}.
%%CITATION = HEP-PH/0603175;%%.

\bibitem{alpgen}
M.~L. Mangano, M.~Moretti, F.~Piccinini, R.~Pittau,  and A.~D. Polosa, {\em
  {ALPGEN, a generator for hard multiparton processes in hadronic collisions}},
  \href{http://dx.doi.org/10.1088/1126-6708/2003/07/001}{JHEP {\bfseries 0307}
  (2003) 001},
\href{http://arxiv.org/abs/hep-ph/0206293}{{\ttfamily arXiv:hep-ph/0206293
  [hep-ph]}}.
%%CITATION = HEP-PH/0206293;%%.

\bibitem{herwig}
G.~Corcella, I.~Knowles, G.~Marchesini, S.~Moretti, K.~Odagiri,  {et~al.}, {\em
  {HERWIG 6: An Event generator for hadron emission reactions with interfering
  gluons (including supersymmetric processes)}},
  \href{http://dx.doi.org/10.1088/1126-6708/2001/01/010}{JHEP {\bfseries 0101}
  (2001) 010},
\href{http://arxiv.org/abs/hep-ph/0011363}{{\ttfamily arXiv:hep-ph/0011363
  [hep-ph]}}.
%%CITATION = HEP-PH/0011363;%%.

\bibitem{spinobspaper}
W.~Bernreuther, D.~Heisler,  and Z.-G. Si, {\em {A set of top quark spin
  correlation and polarization observables for the LHC: Standard Model
  predictions and new physics contributions}},
  \href{http://arxiv.org/abs/1508.05271}{{\ttfamily arXiv:1508.05271
  [hep-ph]}}.

\bibitem{TopCommonObj}
``Top common objects 2012.''
  \url{https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopCommonObjects}.

\bibitem{top_objects}
B.~Acharya, J.~Adelman, S.~Adomeit, M.~Aoki, B.~Alvarez, L.~Asquith, F.~Balli,
  W.~Bell, K.~Becker, K.~Behr, D.~Benjamin, E.~Bergeaas~Kuutmann, C.~Bernard,
  K.~Black, S.~Calvet, R.~Camacho, Y.~Coadou, G.~Cortiana, G.~Compostella,
  N.~Cooper-Smith, T.~Cornelissen, M.~Cristinziani, V.~Dao, U.~De~Sanctis,
  C.~Doglioni, F.~Derue, C.~Eckardt, J.~Ferrando, D.~Ferreira~de Lima,
  K.~Finella, K.~Grahn, J.~Groth-Jensen, N.~Gutierrez~Ortiz, R.~Hawkings,
  S.~Head, A.~Henrichs, D.~Hirschbuehl, G.~Kasieczka, V.~Kaushik,
  H.~Khandanyan, O.~Kind, A.~Krasznahorkay, T.~Kuhl, H.~Lacker, E.~Le~Menedeu,
  H.~Lee, A.~Lister, K.~Loureiro, L.~Mijovic, J.~Morris, R.~Moles~Valls,
  O.~Nackenhorst, M.~zur Nedden, M.~Owen, D.~Pelikan, M.~Pinamonti, K.~Rao,
  K.~Rosbach, M.~Rudolph, G.~Salamanna, J.~Schwindling, J.~Searcy,
  E.~Shabalina, K.~Shaw, J.~Sjolin, R.~Soualah, S.~Stamm, D.~Ta,
  T.~Theveneaux-Pelzer, E.~Thompson, K.~Uchikda, L.~Valery, M.~Vreeswijk,
  C.~Wasicki, I.~Watson, K.~Yau, J.~Zhong,  and H.~Zhu, {\em {Object selection
  and calibration, background estimations and MC samples for top quark analyses
  using the full 2012 data set}}, Tech. Rep. ATL-COM-PHYS-2013-1016, CERN,
  Geneva, Jul, 2013.

\bibitem{Guindon:1638000}
S.~Guindon, E.~Shabalina, J.~Adelman, M.~Alhroob, S.~Amor~dos Santos, A.~Basye,
  J.~Bouffard, M.~Casolino, I.~Connelly, A.~Cortes~Gonzalez, V.~Dao,
  S.~D'Auria, A.~Doyle, P.~Ferrari, F.~Filthaut, R.~Goncalo, N.~de~Groot,
  S.~Henkelmann, V.~Jain, A.~Juste, G.~Kirby, D.~Kar, A.~Knue, K.~Kroeninger,
  T.~Liss, E.~Le~Menedeu, J.~Montejo~Berlingen, M.~Moreno~Llacer,
  O.~Nackenhorst, T.~Neep, A.~Onofre, M.~Owen, M.~Pinamonti, Y.~Qin, A.~Quadt,
  D.~Quilty, C.~Schwanenberger, L.~Serkin, R.~St~Denis, J.~Thomas-Wilsker,  and
  T.~Vazquez-Schroeder, {\em {Search for the Standard Model Higgs boson
  produced in association with top quarks and decaying to $b\bar{b}$ in pp
  collisions at $\sqrt{s}$= 8 TeV with the ATLAS detector at the LHC}}, Tech.
  Rep. ATL-COM-PHYS-2013-1659, CERN, Geneva, Dec, 2013.
\newblock The note contains internal documentation of the ttH(bb) analysis
  approved as a preliminary result (ATLAS-CONF-2014-011).

\bibitem{SpinCorrNote}
T.~Neep, M.~Owen,  and C.~Schwanenberger, {\em A measurement of spin
  correlation in top anti-top pairs and search for top squarks at $\sqrt{s} =
  8$~TeV using the ATLAS detector}, Tech. Rep. ATL-COM-PHYS-2014-286, CERN,
  Geneva, Sep, 2014.

\bibitem{TopSysUncertainties}
``Top systematic uncertainties 2012.''
  \url{https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopSystematicUncertainties}.

\bibitem{ttV_norm_unc}
``Normalization uncertainty for $\ttbar v$.''
  \url{http://indico.cern.ch/event/351006/contributions/823677/attachments/1154597/1659014/ttX.pdf}.

\bibitem{Abulencia:2006js}
{CDF} Collaboration, A.~Abulencia {et~al.}, {\em {Measurement of the top quark
  mass using template methods on dilepton events in p anti-p collisions at
  s**(1/2) = 1.96-TeV}},
  \href{http://dx.doi.org/10.1103/PhysRevD.73.112006}{Phys. Rev. {\bfseries
  D73} (2006) 112006},
\href{http://arxiv.org/abs/hep-ex/0602008}{{\ttfamily arXiv:hep-ex/0602008}}.
%%CITATION = HEP-EX/0602008;%%.

\bibitem{Aaltonen:2012tk}
{CDF Collaboration} Collaboration, T.~Aaltonen {et~al.}, {\em {$W$ boson
  polarization measurement in the $t\bar{t}$ dilepton channel using the CDF II
  Detector}},
  \href{http://dx.doi.org/10.1016/j.physletb.2013.03.032}{Phys.Lett. {\bfseries
  B722} (2013) 48--54},
\href{http://arxiv.org/abs/1205.0354}{{\ttfamily arXiv:1205.0354 [hep-ex]}}.
%%CITATION = ARXIV:1205.0354;%%.

\bibitem{Abazov:2004cs}
{D0 Collaboration} Collaboration, V.~Abazov {et~al.}, {\em {A precision
  measurement of the mass of the top quark}},
  \href{http://dx.doi.org/10.1038/nature02589}{Nature {\bfseries 429} (2004)
  638--642},
\href{http://arxiv.org/abs/hep-ex/0406031}{{\ttfamily arXiv:hep-ex/0406031
  [hep-ex]}}.
%%CITATION = HEP-EX/0406031;%%.

\bibitem{ATLAS-CONF-2012-057}
{ATLAS Collaboration} Collaboration,, {\em {Measurement of the charge asymmetry
  in dileptonic decay of top quark pairs in pp collisions at $\sqrt{s}$ = 7 TeV
  using the ATLAS detector}}, Tech. Rep. ATLAS-CONF-2012-057, CERN, Geneva,
  Jun, 2012.

\bibitem{Abbott:1997fv}
{D0 Collaboration} Collaboration, B.~Abbott {et~al.}, {\em {Measurement of the
  top quark mass using dilepton events}},
  \href{http://dx.doi.org/10.1103/PhysRevLett.80.2063}{Phys.Rev.Lett.
  {\bfseries 80} (1998) 2063--2068},
\href{http://arxiv.org/abs/hep-ex/9706014}{{\ttfamily arXiv:hep-ex/9706014
  [hep-ex]}}.
%%CITATION = HEP-EX/9706014;%%.

\bibitem{Borroni:1536502}
S.~Borroni, F.~Deliot, C.~Deterre, A.~Grohsjean, S.~Hamilton, L.~Mijovic,
  R.~Peters, M.~Rudolph,  and R.~Schaefer, {\em {Measurement of top quark
  polarisation in $t\bar{t}$ events with the ATLAS detector in proton-proton
  collisions at $\sqrt{s} = 7$ TeV}}, Tech. Rep. ATL-COM-PHYS-2013-367, CERN,
  Geneva, Mar, 2013.

\bibitem{mt2reco}
D.~Guadagnoli and C.~B. Park, {\em {$M_{T2}$-reconstructed invisible momenta as
  spin analizers, and an application to top polarization}},
  \href{http://dx.doi.org/10.1007/JHEP01(2014)030}{JHEP {\bfseries 1401} (2014)
  030},
\href{http://arxiv.org/abs/1308.2226}{{\ttfamily arXiv:1308.2226 [hep-ph]}}.
%%CITATION = ARXIV:1308.2226;%%.

\bibitem{KinRecoNote}
J.~Antos, S.~Borroni, A.~Chapelain, F.~D\'eliot, V.~Delong, C.~Deterre,
  J.~Howarth, R.~Lysak, L.~Mijovic, R.~Naranjo, Y.~Peters,  and R.~Schafer,
  {\em Performance studies of ttbar reconstruction methods in the dilepton
  channel at $\sqrt{s} = 8$~TeV in ATLAS}, Tech. Rep. ATL-COM-PHYS-2014-226,
  CERN, Geneva, Mar, 2014.

\bibitem{Erdmann:2013rxa}
J.~Erdmann, S.~Guindon, K.~Kroeninger, B.~Lemmer, O.~Nackenhorst,  {et~al.},
  {\em {A likelihood-based reconstruction algorithm for top-quark pairs and the
  KLFitter framework}}, arXiv:1312.5595 (2013),
\href{http://arxiv.org/abs/1312.5595}{{\ttfamily arXiv:1312.5595 [hep-ex]}}.
%%CITATION = ARXIV:1312.5595;%%.

\bibitem{FBU}
G.~Choudalakis, {\em {Fully Bayesian Unfolding}},
\href{http://arxiv.org/abs/1201.4612}{{\ttfamily arXiv:1201.4612
  [physics.data-an]}}.
%%CITATION = ARXIV:1201.4612;%%.

\bibitem{PyFBU}
``Pyfbu.'' \url{https://pypi.python.org/pypi/fbu}.

\bibitem{top_systematics}
``Systematics description of the top working group.''
  \url{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopSystematicUncertainties}.

\bibitem{mcatnlo}
S.~Frixione and B.~R. Webber, {\em {Matching NLO QCD computations and parton
  shower simulations}},
  \href{http://dx.doi.org/10.1088/1126-6708/2002/06/029}{JHEP {\bfseries 0206}
  (2002) 029},
\href{http://arxiv.org/abs/hep-ph/0204244}{{\ttfamily arXiv:hep-ph/0204244
  [hep-ph]}}.
%%CITATION = HEP-PH/0204244;%%.

\bibitem{toppdf}
``Top group pdf uncertainty recommendations.''
  \url{https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopPdfUncertainty}.

\bibitem{lhapdf}
``Pdf uncertainty recommendations for the \lhc experiments.''
  \url{http://www.hep.ucl.ac.uk/pdf4lhc/}.

\bibitem{top_object_selection}
{B. Acharya et al.}, ``Object selection and calibration, background estimations
  and mc samples for the autumn 2012 top quark analyses with 2011 data.''
  {ATL-COM-PHYS-2012-1197, \url{https://cds.cern.ch/record/1472525}}, Aug,
  2012.

\bibitem{Fakes8TeV}
K.~Becker, T.~Cornelissen, F.~Derue, A.~Ferrari, A.~Henrichs, D.~Hirschbuehl,
  X.~Lei, O.~Nackenhorst, F.~O’Grady, D.~Pelikan, M.~Pinamonti, S.~Pires,
  J.~Sjolin, R.~Soualah,  and T.~P, {\em Estimation of non-prompt and fake
  leptons background in final states with top quarks produced in proton-proton
  collision at $\sqrt{s} = 8$~TeV with the ATLAS detector}, Tech. Rep.
  ATL-COM-PHYS-2013-1100, CERN, Geneva, Sep, 2014.

\bibitem{NumerRecip}
W.~Press, S.~Teukolsky, W.~Vetterling,  and B.~Flannery, {\em Numerical
  Recipes: The Art of Scientific Computing (3rd ed.)}.
\newblock Cambridge University Press, New York, 2007.

\bibitem{ATLAS:2011qia}
{ATLAS Collaboration} Collaboration,,
{\em {Commissioning of the ATLAS high-performance b-tagging algorithms in the 7
  TeV collision data}},.
%%CITATION = ATLAS-CONF-2011-102 ETC.;%%.

\end{thebibliography}\endgroup
